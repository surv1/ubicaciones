<?php 	$url = base_url();	

$form=array('accept-charset'=>'utf8');	

echo form_open_multipart('gestion_paso/validar_registro',$form);	

$procesos=$this->paso_model->comboProcesos();	

$pasos=$this->paso_model->comboPasos();	

$idfacultades=$this->paso_model->comboFacultades();

$secciones=$this->paso_model->comboSecciones();

$nombImg=$this->paso_model->nombImg($this->session->userdata('idpaso'));	

// header ('Content-type: ' . 'image/jpeg');	
// echo base64_decode($this->session->userdata('userfile'));		
//$this->session->set_userdata('nombImg',$url.'/recursos/pasos/'.$nombImg);		
$urlImg=$url.'/recursos/pasos/'.$nombImg;?>
<center>			
	<div id="title-page">				
		Editar Paso</br></br>			
	</div>							
	
	<p>Los campos marcados con * son requeridos</p></br>				
	
	<table>		
					
		<tr>						
			<td align="right"><label>Id: *</label></td>						
			<td id="row1"><?php echo form_input('idpaso',$this->session->userdata('idpaso'),'readonly=readonly maxlength="50" style="height:25px; width:150px" onkeypress="return numeros(event,1)" '); ?>														
			</td>			
		</tr>					
		<tr>						
			<td align="right"><label>Proceso: *</label></td>						
			<td><?php echo form_dropdown('idproc',$procesos,$this->session->userdata('idproc'),' id="idproc" style="width: 300px;" class="dropdown" ');?>						
			</td>					
		</tr>					
		<tr>						
			<td align="right"><label>Paso: *</label></td>						
			<td ><?php echo form_input('nombPaso',$this->session->userdata('nombPaso'),'readonly=readonly maxlength="50" style="height:25px; width:300px" '); ?>														
			</td>					
		</tr>					
		<tr>						
			<td align="right"><label>Descripci&oacute;n:&nbsp; &nbsp; </label></td>						
			<td ><?php echo form_textarea('descripPaso',$this->session->userdata('descripPaso'),'id="descripPaso" style="width: 300px; height:70px"  ');?>						
			</td>					
		</tr>	
		<tr>						
			<td align="right"><label>Facultad:&nbsp; &nbsp; </label></td>						
			<td><?php echo form_dropdown('idfacultad',$idfacultades,$this->session->userdata('idfacultad'),'id="idfacultad" style="width: 300px;" class="dropdown" ');?>						
			</td>					
		</tr>

		<tr>
			<td align="right"><label>Secci&oacute;n Fin: *</label></td>						
			<td><?php echo form_dropdown('idsecc',$secciones,$this->session->userdata('idsecc'),'id="idsecc" style="width: 300px;" class="dropdown" ');?>						
			</td>
		</tr>
		
		<tr>						
			<td align="right"><label>N&uacute;mero: *</label></td>						
			<td id="row2"><?php echo form_input('numPosiPaso',$this->session->userdata('numPosiPaso'),'maxlength="50" 															  style="height:25px; width:150px" onkeypress="return numeros(event,2)" '); ?>						
			</td>					
		</tr>
		
		<tr>						
			<td align="right"><label>Frase de paso cumplido: *</label></td>						
			<td ><?php echo form_textarea('logroPaso',$this->session->userdata('logroPaso'),'id="logroPaso" style="width: 200px; height:100px"  ');?>					
			</td>					}
		</tr>
					
		<tr>						
			<td align="right"><label>Subir Documento:&nbsp; &nbsp; </label></td>						
			<td><?php echo form_upload('userfile','',$this->session->userdata('userfile'),'id="userfile" name="userfile" style="width: 300px;"  ');?>						
			</td>											
		</tr>					
		<tr>						
			<td align="right"><label>Anterior :&nbsp; &nbsp; </label></td>						
			<td><?php  if($nombImg!=null) { echo "<img src='".$url."recursos/pasos/".$nombImg."' alt='logo' height='200' width='300'></td> \n"; } ?>
			</td>																			
		</tr>					
		<tr>						
			<td align="right"><label>Antecesor:&nbsp; &nbsp; </label></td>						
			<td><?php echo form_dropdown('antecesor',$pasos,$this->session->userdata('antecesor'),'id="antecesor" style="width: 200px;" class="dropdown" ');?>						
			</td>					
		</tr>									
	</table>
</center>				
<center>
	<table>					
		<tr>						
			<td><input type="submit" name="Guardar_Cambios" value="Actualizar" class="button-submit"/></td>						
			<td><input type="submit" name="Cancelar" value="Cancelar" class="button-submit"/></td>					
		</tr>
	</table>
</center>					
			
<script language="Javascript" type="text/javascript">	
	function numeros(e,div1)
	{
		tecla = e.keyCode || e.which;
		//alert(div1);
		var iddiv=div1;
		patron=/[0-9-+()\b]/;
		if(tecla!=9){
			if(tecla!=37){
				if(tecla!=39){
					codigo_letra=String.fromCharCode(tecla);
					if(!patron.test(codigo_letra)){
						var iDiv = document.createElement('div');
						iDiv.id = 'errorId'+div1;
						
						var s = document.getElementById('row'+div1);
						s.appendChild(iDiv);
						document.getElementById ("errorId"+div1).innerHTML="Debe ingresar solo numeros";
						document.getElementById ("errorId"+div1).style.display = 'block';
						document.getElementById ("errorId"+div1).style.color = 'red';
						//alert("Debe ingresar solo numeros");
						return false;
					}
					document.getElementById ("errorId"+div1).style.display = 'none';
					return true;
				}
			document.getElementById ("errorId"+div1).style.display = 'none';
			return true;	
			}		
		document.getElementById ("errorId"+div1).style.display = 'none';
		return true;	
		}	
		document.getElementById ("errorId"+div1).style.display = 'none';
		return true;	
	}
</script>					
<?php echo form_close();?>		