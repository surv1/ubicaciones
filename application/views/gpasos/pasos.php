<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');
	echo form_open('gestion_paso/mostrar_datos',$form);
	$pasos=$this->paso_model->pasos();
?>	
<center>
	<div id="title-page">
		Pasos
	</div>
	<br>
	<p>Seleccione el paso que desea Editar.</p>
	<input type="hidden" name="idpaso" id="idpaso" readonly="readonly" value="prueba">
	<table id="lista_paso" height="100px">
		<thead>
			<tr>
				<th>Id</th>
				<th>Proceso</th>
				<th>Nombre</th>
				<th>Decripci&oacute;n</th>
				<th>Antecesor</th>
				<th>Facultad</th>

				<th></th>								

				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($pasos);$i++){
					echo $pasos[$i];
				}
			?>
		</tbody>
	</table>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('#lista_paso').dataTable({
		"sPaginationType":"full_numbers",
		"oLanguage": {
		"oPaginate": {
        "sFirst": "Primera pagina",
		"sLast": "Ultima pagina",
		"sNext": "Siguiente",
		"sPrevious": "Anterior"},
		"sInfo": "Mostrando _END_ de _TOTAL_ pasos",
		"sInfoEmpty": "No se encontraron datos de pasos para mostrar",
		"sLengthMenu": "Mostrar _MENU_  pasos por  p&aacute;gina",
		"sProcessing": "Se estan buscando todos las datos de los pasos registrados",
		"sZeroRecords":    "No se encontro datos de pasos",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sSearch": "Buscar:"
		}	
		});
		
		}
	)
</script>
<?php echo form_close();?>