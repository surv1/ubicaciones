<!DOCTYPE HTML>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Gesti&oacute;n de UbicacionES</title>
	<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/jquery.dataTables.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/jquery-ui-1.8.4.custom.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>recursos/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>recursos/css/mensajes.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/message.css" type="text/css">	
	<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/estilo_forms.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/bootstrap-responsive.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/jquery.confirm.css" type="text/css"/>		
	<link rel="shortcut icon" href="<?php echo base_url();?>recursos/css/images/ico.png">
	
	
	<script src="<?php echo base_url();?>recursos/js/jquery.js"></script>
	<script src="<?php echo base_url();?>recursos/js/core.js"></script>
	<script src="<?php echo base_url();?>recursos/js/position.js"></script>
	<script src="<?php echo base_url();?>recursos/js/widget.js"></script>
	<script src="<?php echo base_url();?>recursos/js/button.js"></script>	
	<script src="<?php echo base_url();?>recursos/js/dialog.js"></script>
	<script src="<?php echo base_url();?>recursos/js/jquery.dataTables.js"></script>
	<script src="<?php echo base_url();?>recursos/js/jquery.maskedinput.js"></script>

	<script>
	$(function() {		
		$("#dialog:ui-dialog").dialog("destroy");	
		$("#dialog-message").dialog({
			modal: true,
			buttons: {
				Ok: function() {
					$(this).dialog("close");
				}}				
			
		});
	});
	
	
	</script>
  </head>
  <body>
  	<div class="pagina">
	  	<header id="principal" align="center">
	  		<div class="aux-space" id="fixed-space">
		  		<div id="logo">
		  			<a href="<?php echo base_url();?>index.php"><img src="<?php echo base_url()?>recursos/imagenes/logo1.jpg" alt="logo" id="logo-img"></a>
		  		</div>
		  		<div id="body-header">
		        	<h1>Gesti&oacute;n de UbicacionES</h1>
		      	</div>
		      <div class="div-table">
		      	<div class="div-cell">
				      <div id="titulo-pie-cont">
						<ul>
							<li class="li-clear"><a href="#"><?php //echo $this->session->userdata('nomb_usuario');?></a></li>
							<li class="li-float"><a href="<?php echo base_url();?>index.php/inicio/cerrarSesion">Cerrar Sesi&oacute;n</a></li>
							<li class="li-float"><a href="#">
								<script>
									var f = new Date();
									document.write(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
								</script></a>
							</li>
						</ul>
			        </div>
			      </div>
			    </div>
	      </div>
	  	</header>
	  		<div id="mn">
		  		<div class="aux-space"><center>
						<ul id="nav">
							<li><a href="<?php echo base_url();?>index.php">Inicio</a></li>
							
							<!--<li><a href="<?php echo base_url();?>index.php/gestion_empleados/gEmpleados">Gesti�n de Empleados</a></li>-->
							<li><a href=""> Mantenimientos</a>
								<ul>
									<li><a href="<?php echo base_url();?>index.php/gestion_segmentos/gSegmentos">Segmentos</a></li>
									<li><a href="<?php echo base_url();?>index.php/gestion_secciones/gSecciones">Secciones</a></li>
									<li><a href="<?php echo base_url();?>index.php/gestion_conexiones/gConexiones">Conexiones</a></li>
									<li><a href="<?php echo base_url();?>index.php/gestion_info/gInfo">Informaci&oacute;n</a></li>
									<li><a href="<?php echo base_url();?>index.php/gestion_tipopersonaje/gTipoPer">Tipos de personajes</a></li>
									<li><a href="<?php echo base_url();?>index.php/gestion_personajes/gPersonaje"> Personajes</a></li>
									<li><a href="<?php echo base_url();?>index.php/gestion_frases/gFrase"> Frases</a></li>
								</ul>
							</li>
							<li><a href=""> Nuevo Ingreso</a>
								<ul>
									<li><a href="<?php echo base_url();?>index.php/gestion_procesos/gProceso">Procesos</a></li>
									<li><a href="<?php echo base_url();?>index.php/gestion_paso/gPaso">Pasos</a></li>
								</ul>
							</li>
							
							
							<li><a href=""> Actualizaciones</a>
								<ul>
									<li ><a  onclick="confirmAccion(1)">Actualizar Versi&oacute;n</a></li>
									<li id="actua_bd"><a  onclick="confirmAccion(2)">Actualizar BD</a></li>
								</ul>
							</li>
							<li><a href=""> Reportes</a>
								<ul>
									<li>
										<a href="<?php echo base_url();?>index.php/gestion_reportes/gReporteUso">Reporte de UbicacionES</a>
										<a href="<?php echo base_url();?>index.php/gestion_reportes/gReporteFrecUso">Reporte de Frecuencia de Uso</a>
									</li>
								</ul>
								
							</li>
						</ul>
					</center></div>
				</div>
			<div class="aux-space-body">
				<div class="content-page">
					<div class="side-bar" style="float:left;">
						<aside>
							<?php echo $sidebar;?>
						</aside>
					</div>
					<div class="work-space" style="float:left; border-left: 2px double;">
						<?php echo $workspace;?>
					</div>
				</div>
	  	</div>
	  	<footer>
	  		<div class="aux-space"><center>
	  			Desarrollado por  - 2014 - Universidad de El Salvador Copyright &copy; todos los Derechos Reservados
	  		</center></div>
	  	</footer>
	</div>
	<!-- javascript
    ================================================== -->
    <!-- Situado hasta el final de la pagina para hacerla mas rapida -->
    <script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>    		  
	<script src="<?php echo base_url();?>recursos/js/alert.js"></script>	
	<script src="<?php echo base_url();?>recursos/js/dropdown.js"></script> 
	<script src="<?php echo base_url();?>recursos/js/application.js"></script>
	<script src="<?php echo base_url();?>recursos/js/tooltip.js"></script>   
	<script src="<?php echo base_url();?>recursos/js/popover.js"></script>  
	<script src="<?php echo base_url();?>recursos/js/affix.js"></script>
	<script src="<?php echo base_url();?>recursos/js/jquery.confirm.js"></script>
	<script language="Javascript" type="text/javascript">	
		function confirmAccion(opcion){
			if(opcion==1){
				$(document).ready(function(){
					var elem = $(this).closest('.item');

				        $.confirm({
				            'title'		: 'Actualizaci&oacute;n',
				            'message'	: '&iquest;Desea actualizar a una nueva versi&oacute;n?',
				            'buttons'	: {
				                'Si'	: {
				                    'class'	: 'blue',
				                    'action': function(){
				                    	window.location.href ='<?php echo base_url().'index.php/gestion_actualizaciones/gUpdateVer';?>';
				                    }
				                },
				                'No'	: {
				                    'class'	: 'gray',
				                    'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
				                }
				            }
				        });
				});
					
			}
			else{
				$(document).ready(function(){
					var elem = $(this).closest('.item');

				        $.confirm({
				            'title'		: 'Actualizaci&oacute;n',
				            'message'	: '&iquest;Desea actualizar la base de datos?',
				            'buttons'	: {
				                'Si'	: {
				                    'class'	: 'blue',
				                    'action': function(){
				                    	window.location.href ='<?php echo base_url().'index.php/gestion_actualizaciones/gETL_FIA';?>';
				                    }
				                },
				                'No'	: {
				                    'class'	: 'gray',
				                    'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
				                }
				            }
				        });
				});
			}
			
		}
	</script>					
  </body>
</html>
