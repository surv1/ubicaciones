<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');
	echo form_open('gestion_procesos/mostrar_datos',$form);
	$procesos=$this->proceso_model->procesos();
?>	
<center>
	<div id="title-page">
		Procesos
	</div>
	<br>
	<p>Seleccione el proceso que desea Editar.</p>
	<input type="hidden" name="idproceso" id="idproceso" readonly="readonly" value="prueba">
	<table id="lista_proceso" height="100px">
		<thead>
			<tr>
				<th>Id</th>
				<th>Proceso</th>
				<th>Decripci&oacute;n</th>
				<th>Antecesor</th>
				<th></th>								<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($procesos);$i++){
					echo $procesos[$i];
				}
			?>
		</tbody>
	</table>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('#lista_proceso').dataTable({
		"sPaginationType":"full_numbers",
		"oLanguage": {
		"oPaginate": {
        "sFirst": "Primera pagina",
		"sLast": "Ultima pagina",
		"sNext": "Siguiente",
		"sPrevious": "Anterior"},
		"sInfo": "Mostrando _END_ de _TOTAL_ procesos",
		"sInfoEmpty": "No se encontraron datos de procesos para mostrar",
		"sLengthMenu": "Mostrar _MENU_  procesos por  p&aacute;gina",
		"sProcessing": "Se estan buscando todos las datos de los procesos registrados",
		"sZeroRecords":    "No se encontro datos de procesos",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sSearch": "Buscar:"
		}	
		});
		
		}
	)
</script>
<?php echo form_close();?>