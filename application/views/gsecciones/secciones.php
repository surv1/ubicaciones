<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');
	echo form_open('gestion_secciones/mostrar_datos',$form);
	$secciones=$this->seccion_model->secciones();
?>	
<center>
	<div id="title-page">
		Secciones
	</div>
	<br>
	<p>Seleccione la Secci&oacute;n que desea Editar.</p>
	<input type="hidden" name="idseccion" id="idseccion" readonly="readonly" value="prueba">
	<table id="lista_secciones" height="100px">
		<thead>
			<tr>
				<th>Id</th>
				<th>Segmento</th>
				<th>Secci&oacute;n</th>
				<th>Descripci&oacute;n</th>
				<th>Tipo de instalaci&oacute;n</th>
				<th>Br&uacute;jula</th>								<th>Mapa</th>
				<th></th>								<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($secciones);$i++){
					echo $secciones[$i];
				}
			?>
		</tbody>
	</table>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('#lista_secciones').dataTable({
		"sPaginationType":"full_numbers",
		"oLanguage": {
		"oPaginate": {
        "sFirst": "Primera pagina",
		"sLast": "Ultima pagina",
		"sNext": "Siguiente",
		"sPrevious": "Anterior"},
		"sInfo": "Mostrando _END_ de _TOTAL_ secciones",
		"sInfoEmpty": "No se encontraron secciones para mostrar",
		"sLengthMenu": "Mostrar _MENU_  Secciones por  p&aacute;gina",
		"sProcessing": "Se estan buscando todos los secciones registrados",
		"sZeroRecords":    "No se encontraron secciones",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sSearch": "Buscar:"
		}	
		});
		
		}
	)
</script>
<?php echo form_close();?>