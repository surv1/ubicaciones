<?php 
	$url = base_url();
	$form=array('accept-charset'=>'utf-8');
	echo form_open('gestion_secciones/validar_registro',$form);
	$nombSegmento=$this->seccion_model->comboSegmentos();
	$nombAula=$this->seccion_model->comboAulas();
	//maxlength="50" style="height:25px; width:150px" onkeypress="return numeros(event,2)"  onblur="return validar(2)"
?>
<center>
			<div id="title-page">
				Registro de Nueva Secci&oacute;n</br></br>
			</div>
			
				<p>Los campos marcados con * son requeridos</p></br>
				<table>
					<tr>
						<td align="right"><label>Id: *</label></td>
						<td id="row1"><?php echo form_input('idSeccion',$this->session->userdata('idSeccion'),'maxlength="50" style="height:25px; width:150px" onkeypress="return numeros(event,1)" '); ?>
								
						</td>
					</tr>
					<tr>
						<td align="right"><label>Segmento: *</label></td>
						<td ><?php echo form_dropdown('idSegmento',$nombSegmento,$this->session->userdata('idSegmento'),'id="idSegmento" style="width: 150px;" class="dropdown" ');?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>Secci&oacute;n: *</label></td>
						<td ><?php echo form_input('nombSeccion',$this->session->userdata('nombSeccion'),'maxlength="50" style="height:25px; width:250px" '); ?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>Descripci&oacute;n:* </label></td>
						<td><?php echo form_textarea('descripSeccion',$this->session->userdata('descripSeccion'),'rows="1" cols="1"  style="height:60px; width:250px"  '); ?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>Tipo de instalaci&oacute;n: *</label></td>
						<td ><?php $tipInstal=array(
												'0'=>'Seleccione...',
												'1'=>'Externa',
												'2'=>'Interna'
											);
						echo form_dropdown('tipoInstalac',$tipInstal,$this->session->userdata('tipoInstalac'),'id="tipoInstalac" style="width: 150px;" class="dropdown" ');?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>Aula: &nbsp;&nbsp;</label></td>
						<td ><?php echo form_dropdown('seccAula',$nombAula,$this->session->userdata('seccAula'),'id="seccAula" style="width: 150px;" class="dropdown" ');?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>Br&uacute;jula: *</label></td>
						<td id="row2" ><?php $tipBrujula=array(												
														
														'0'=>'Seleccione...',												
														
														'1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12'											
														
														);						
														
														echo form_dropdown('brujula',$tipBrujula,$this->session->userdata('brujula'),' id="brujula"  style="width: 150px;" class="dropdown" '); ?>
						</td>
					</tr>
					<tr>						
						<td align="right"><label>Mapa: *</label></td>												
						<td id="row3">							
							<img src="<?php echo base_url()?>recursos/imagenes/mapita.jpg" alt="logo" >														
									<?php $tipMap=array(												
														'0'=>'Seleccione...',												
														
														'1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15'											
														
														);							
														
														echo form_dropdown('mapa',$tipMap,$this->session->userdata('mapa'),' id="mapa" style="width: 150px;" class="dropdown"  '); ?>						
						</td>					
					</tr>
					<tr>
						<td align="right"><label>Puntos GPS (Latitud,Longitud): &nbsp;&nbsp;</label></td>	
						<td>
							<table>
								<tr >
									<td>
										<table>
											<td>
												<label>1(</label>
											</td>
											<td>
												<?php echo form_input('ptoGrupLat1',$this->session->userdata('ptoGrupLat1'),' style="height:25px; width:100px" '); ?>
											</td>
											<td>
												<label>,</label>
											</td>
											<td>
												<?php echo form_input('ptoGrupLong1',$this->session->userdata('ptoGrupLong1'),' style="height:25px; width:100px" '); ?>
											</td>
											<td>
												<label>)</label>
											</td>
										</table>
										
									</td>
									<td rowspan="2">
										<img src="<?php echo base_url()?>recursos/imagenes/MapaOriginal2.png" alt="logo" >	
									</td>
									<td>
										<table>
											<td>
												<label>2(</label>
											</td>
											<td>
												<?php echo form_input('ptoGrupLat2',$this->session->userdata('ptoGrupLat2'),' style="height:25px; width:100px" '); ?>
											</td>
											<td>
												<label>,</label>
											</td>
											<td>
												<?php echo form_input('ptoGrupLong2',$this->session->userdata('ptoGrupLong2'),' style="height:25px; width:100px" '); ?>
											</td>
											<td>
												<label>)</label>
											</td>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table>
											<td>
												<label>3(</label>
											</td>
											<td>
												<?php echo form_input('ptoGrupLat3',$this->session->userdata('ptoGrupLat3'),' style="height:25px; width:100px" '); ?>
											</td>
											<td>
												<label>,</label>
											</td>
											<td>
												<?php echo form_input('ptoGrupLong3',$this->session->userdata('ptoGrupLong3'),' style="height:25px; width:100px" '); ?>
											</td>
											<td>
												<label>)</label>
											</td>
										</table>
									</td>
									
									<td>
										<table>
											<td>
												<label>4(</label>
											</td>
											<td>
												<?php echo form_input('ptoGrupLat4',$this->session->userdata('ptoGrupLat4'),' style="height:25px; width:100px" '); ?>
											</td>
											<td>
												<label>,</label>
											</td>
											<td>
												<?php echo form_input('ptoGrupLong4',$this->session->userdata('ptoGrupLong4'),' style="height:25px; width:100px" '); ?>
											</td>
											<td>
												<label>)</label>
											</td>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>	
				</table></center>
				<center><table>
					<tr>
						<td><input type="submit" name="Guardar_Seccion" value="Guardar Secci&oacute;n" class="button-submit"/></td>
						<td><input type="submit" name="Cancelar" value="Cancelar" class="button-submit"/></td>
					</tr></table></center>
					
<script language="Javascript" type="text/javascript">
	
	
	function letras(e)
	{
		tecla = e.keyCode || e.which;
		if(tecla!=9){
			if(tecla!=37){
				if(tecla!=39){
					if((tecla>40 && tecla<64))
						{
						alert("Debe ingresar solo letras");
						return false;
						}
					else{
						if((tecla>90 && tecla<97)){
							alert("Debe ingresar solo letras");
							return false;
							}
						else{
							if((tecla>122 && tecla<126)){
								alert("Debe ingresar solo letras");
								return false;
								}
							else{
								patron=/[�!"#$%&\/\()=?�]/;
								codigo_letra=String.fromCharCode(tecla);
								if(patron.test(codigo_letra)){
									alert("Debe ingresar solo letras");
									return false;
									}
								}
							}
						}
					return true;	
					}	
				return true;
				}	
			return true;
			}
	}	
	
	function numeros(e,div1)
	{
		tecla = e.keyCode || e.which;
		//alert(div1);
		var iddiv=div1;
		patron=/[0-9-+()\b]/;
		if(tecla!=9){
			if(tecla!=37){
				if(tecla!=39){
					codigo_letra=String.fromCharCode(tecla);
					if(!patron.test(codigo_letra)){
						var iDiv = document.createElement('div');
						iDiv.id = 'errorId'+div1;
						
						var s = document.getElementById('row'+div1);
						s.appendChild(iDiv);
						document.getElementById ("errorId"+div1).innerHTML="Debe ingresar solo n&uacute;meros";
						document.getElementById ("errorId"+div1).style.display = 'block';
						document.getElementById ("errorId"+div1).style.color = 'red';
						//alert("Debe ingresar solo numeros");
						return false;
					}
					document.getElementById ("errorId"+div1).style.display = 'none';
					return true;
				}
			document.getElementById ("errorId"+div1).style.display = 'none';
			return true;	
			}		
		document.getElementById ("errorId"+div1).style.display = 'none';
		return true;	
		}	
		document.getElementById ("errorId"+div1).style.display = 'none';
		return true;	
	}
	
	function validar(div1){
		var iddiv=div1;
		var valor = parseInt(document.getElementById('brujula').value);
		if(valor<0 || valor>12){
			
			var iDiv = document.createElement('div');
			iDiv.id = 'errorMax'+div1;
			
			var s = document.getElementById('row'+div1);
			s.appendChild(iDiv);
			document.getElementById ("errorMax"+div1).innerHTML="Debe estar entre 1 y 12";
			document.getElementById ("errorMax"+div1).style.display = 'block';
			document.getElementById ("errorMax"+div1).style.color = 'red';
			document.getElementById('brujula').value="";
			//alert("Debe ingresar solo numeros");
			return false;
		}
		else{
			document.getElementById ("errorMax"+div1).style.display = 'none';
			return true;
		}
	}
	
</script>				
<?php echo form_close();?>		