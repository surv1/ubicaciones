<?php 
	$url = base_url();
	$form=array('accept-charset'=>'utf8');
	echo form_open('gestion_info/mostrar_datos',$form);
	$informaciones=$this->info_model->informaciones();
?>	
<center>
	<div id="title-page">
		Informaciones
	</div>
	<br>
	<p>Seleccione la Informaci&oacute;n que desea Editar.</p>
	<input type="hidden" name="idinfo" id="idinfo" readonly="readonly" value="prueba">
	<table id="lista_info" height="100px">
		<thead>
			<tr>
				<th>Id</th>
				<th>Rotulo</th>
				<th>Secci&oacute;n</th>
				<th>Contenido</th>
				<th>Tema</th>
				<th>Tipo Instalaci&oacute;n</th>
				<th></th>								<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($informaciones);$i++){
					echo $informaciones[$i];
				}
			?>
		</tbody>
	</table>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('#lista_info').dataTable({
		"sPaginationType":"full_numbers",
		"oLanguage": {
		"oPaginate": {
        "sFirst": "Primera pagina",
		"sLast": "Ultima pagina",
		"sNext": "Siguiente",
		"sPrevious": "Anterior"},
		"sInfo": "Mostrando _END_ de _TOTAL_ informaci&oacute;n",
		"sInfoEmpty": "No se encontraron datos de informaci&oacute;n para mostrar",
		"sLengthMenu": "Mostrar _MENU_  informaci&oacute;n por  p&aacute;gina",
		"sProcessing": "Se estan buscando todos las datos de informaci&oacute;n registrados",
		"sZeroRecords":    "No se encontro datos de informaci&oacute;n",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sSearch": "Buscar:"
		}	
		});
		
		}
	)
</script>
<?php echo form_close();?>