<?php 

	$url = base_url();

	$form=array('accept-charset'=>'utf8');

	echo form_open('gestion_info/validar_registro',$form);

	$nombSecciones=$this->info_model->comboSecciones();

?>

<center>

			<div id="title-page">

				Editar Informaci&oacute;n</br></br>

			</div>

			

				<p>Los campos marcados con * son requeridos</p></br>

				<table>

					<tr>

						<td align="right"><label>Id: *</label></td>

						<td id="row1"><?php echo form_input('idInfo',$this->session->userdata('idInfo'),'maxlength="50" style="height:25px; width:150px" readonly=readonly onkeypress="return numeros(event,1)" '); ?>

								

						</td>

					</tr>

					<tr>

						<td align="right"><label>N&uacute;mero de R&oacute;tulo: *</label></td>

						<td id="row2"><?php 

							$num=array(

								'1'=>'1',

								'2'=>'2',

								'3'=>'3',

							);

							echo form_input('infonum',$this->session->userdata('infonum'),'maxlength="50" style="height:25px; width:150px" onkeypress="return numeros(event,2)"  ');?>

						</td>

					</tr>

					<tr>

						<td align="right"><label>Secci&oacute;n: *</label></td>

						<td ><?php echo form_dropdown('idSecc',$nombSecciones,$this->session->userdata('idSecc'),'id="idSecc" style="width: 200px;" class="dropdown"  ');?>

						</td>

					</tr>

					<tr>

						<td align="right"><label>Tipo informaci&oacute;n:* </label></td>

						<td id="row3"><?php $tipo=array(

															'0'=>'Mapa',

															'1'=>'Rotulo',

															'2'=>'Procesos administrativos',

															'3'=>'Procesos academicos',

															'4'=>'Informaci&oacute;n de instalaci&oacute;n',

															'5'=>'Autoridades'

														);

										echo form_dropdown('infotipo',$tipo,$this->session->userdata('infotipo'),' style="width:200px" class="dropdown" onChange="cambioSize(this)" '); ?>

						</td>

					</tr>


					<tr>

						<td align="right"><label>Contenido: *</label></td>

						<td><?php echo form_textarea('contenido',$this->session->userdata('contenido'),'rows="15" cols="15"  style="height:150px; width:200px"  '); ?>

						</td>

					</tr>

					<tr>

						<td align="right"><label>Tema: *</label></td>

						<td id="row2"><?php echo form_input('tema',$this->session->userdata('tema'),' maxlength="50" style="height:25px; width:250px"  '); ?>

						</td>

					</tr>

					

					

					<tr>

				</table></center>

				<center><table>

					<tr>

						<td><input type="submit" name="Guardar_Cambios" value="Actualizar" class="button-submit"/></td>

						<td><input type="submit" name="Cancelar" value="Cancelar" class="button-submit"/></td>

					</tr></table></center>

					

<script language="Javascript" type="text/javascript">

	

	function letras(e)

	{

		tecla = e.keyCode || e.which;

		if(tecla!=9){

			if(tecla!=37){

				if(tecla!=39){

					if((tecla>40 && tecla<64))

						{

						alert("Debe ingresar solo letras");

						return false;

						}

					else{

						if((tecla>90 && tecla<97)){

							alert("Debe ingresar solo letras");

							return false;

							}

						else{

							if((tecla>122 && tecla<126)){

								alert("Debe ingresar solo letras");

								return false;

								}

							else{

								patron=/[?!"#$%&\/\()=??]/;

								codigo_letra=String.fromCharCode(tecla);

								if(patron.test(codigo_letra)){

									alert("Debe ingresar solo letras");

									return false;

									}

								}

							}

						}

					return true;	

					}	

				return true;

				}	

			return true;

			}

	}	

	

	function numeros(e,div1)

	{

		tecla = e.keyCode || e.which;

		//alert(div1);

		var iddiv=div1;

		patron=/[0-9-+()\b]/;

		if(tecla!=9){

			if(tecla!=37){

				if(tecla!=39){

					codigo_letra=String.fromCharCode(tecla);

					if(!patron.test(codigo_letra)){

						var iDiv = document.createElement('div');

						iDiv.id = 'errorId'+div1;

						

						var s = document.getElementById('row'+div1);

						s.appendChild(iDiv);

						document.getElementById ("errorId"+div1).innerHTML="Debe ingresar solo numeros";

						document.getElementById ("errorId"+div1).style.display = 'block';

						document.getElementById ("errorId"+div1).style.color = 'red';

						//alert("Debe ingresar solo numeros");

						return false;

					}

					document.getElementById ("errorId"+div1).style.display = 'none';

					return true;

				}

			document.getElementById ("errorId"+div1).style.display = 'none';

			return true;	

			}		

		document.getElementById ("errorId"+div1).style.display = 'none';

		return true;	

		}	

		document.getElementById ("errorId"+div1).style.display = 'none';

		return true;	

	}

	

	function cambiarDestino(combo){

		// alert('entra');

		// cod=combo.options[combo.selectedIndex].value;		

		// alert(cod);

		var numbers = [combo.length];

		$('#idSeccD').html('');

		for(i = 0;  i < combo.length;  i++) {

			numbers[combo[i].value] = combo[i].text;

        }

		var option = '';

			$.each(numbers, function(val, text) {

            $('#idSeccD').append( $('<option></option>').val(val).html(text) )

            });

			$('#idSeccD').append(option);

			$("#idSeccD").find("option[value='"+combo.options[combo.selectedIndex].value+"']").remove();  	

	}

	function cambioSize(comboTipInstal){

		cod=comboTipInstal.options[comboTipInstal.selectedIndex].value;
		//alert(cod);
		
		if(cod==5){
		
			document.getElementById ("contenido").style.width = '400px';
		
			document.getElementById ("contenido").style.height = '400px';
		
		}
		
		else{
		
			document.getElementById ("contenido").style.width = '200px';
		
			document.getElementById ("contenido").style.height = '150px';
		
		}
	}


</script>					

<?php echo form_close();?>		