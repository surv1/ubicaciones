<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');
	echo form_open('gestion_tipopersonaje/mostrar_datos',$form);
	$tiposper=$this->tipopersonaje_model->tiposperson();
?>	
<center>
	<div id="title-page">
		Tipos de personajes
	</div>
	<br>
	<p>Seleccione el tipo de personaje que desea Editar.</p>
	<input type="hidden" name="idtipoper" id="idtipoper" readonly="readonly" value="prueba">
	<table id="lista_tipoper" height="100px">
		<thead>
			<tr>
				<th>Id</th>
				<th>Tipo personaje</th>
				<th>Decripci&oacute;n</th>
				<th></th>								<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($tiposper);$i++){
					echo $tiposper[$i];
				}
			?>
		</tbody>
	</table>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('#lista_tipoper').dataTable({
		"sPaginationType":"full_numbers",
		"oLanguage": {
		"oPaginate": {
        "sFirst": "Primera pagina",
		"sLast": "Ultima pagina",
		"sNext": "Siguiente",
		"sPrevious": "Anterior"},
		"sInfo": "Mostrando _END_ de _TOTAL_ Tipo personajes",
		"sInfoEmpty": "No se encontraron datos de tipos de personajes para mostrar",
		"sLengthMenu": "Mostrar _MENU_  Tipos de personajes por  p&aacute;gina",
		"sProcessing": "Se estan buscando todos las datos de los tipos de personajes registrados",
		"sZeroRecords":    "No se encontro datos de los tipos de personajes",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sSearch": "Buscar:"
		}	
		});
		
		}
	)
</script>
<?php echo form_close();?>