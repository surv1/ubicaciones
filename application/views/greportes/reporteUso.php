<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');

	$reporte=$this->reporte_model->uso();
?>	
<center>
	<div id="title-page">
		Reporte del uso de la aplicaci&oacute;n m&oacute;vil UbicacionES
	</div>
	<br>
	<p></p>
	<table id="lista_uso" height="100px">
		<thead>
			<tr>
				<th>Plataforma</th>
				<th>Cantidad</th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($reporte);$i++){
					echo $reporte[$i];
				}
			?>
		</tbody>
	</table>
	
</center>
<script type="text/javascript">
	$(document).ready( function {
	       $('#lista_uso').dataTable( {
	         "bPaginate": false,
	         "bSort": false 
	       } );
	     } );
	
	
</script>
<?php echo form_close();?>