<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');

	$reporte_frec=$this->reporte_model->frecuenciaUso();
?>	
<center>
	<div id="title-page">
		Reporte de Frecuencia de Uso de la aplicaci&oacute;n m&oacute;vil UbicacionES
	</div>
	<br>
	<p></p>
	<table id="lista_frec_uso" height="100px">
		<thead>
			<tr>
				<th>Tipo de Navegaci&oacute;n</th>
				<th>Cantidad</th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($reporte_frec);$i++){
					echo $reporte_frec[$i];
				}
			?>
		</tbody>
	</table>
	
</center>
<script type="text/javascript">
	$(document).ready( function {
	       $('#lista_frec_uso').dataTable( {
	         "bPaginate": false,
	         "bSort": false 
	       } );
	     } );
	
	
</script>
<?php echo form_close();?>