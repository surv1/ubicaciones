<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Bienvenido al Sistema de Gestión de  UbicacionES</title>
    <link rel="stylesheet" href="<?php echo base_url();?>recursos/css/login.css" type="text/css">
	<!--<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/style.css" type="text/css">
    --><link rel="stylesheet" href="<?php echo base_url();?>recursos/css/mensajes.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/message.css" type="text/css">	
	<!--<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/estilo_forms.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>recursos/css/bootstrap-responsive.css" type="text/css">		
	<link rel="shortcut icon" href="<?php echo base_url();?>recursos/css/images/ico.png">
	--><script src="<?php echo base_url();?>recursos/js/jquery.js"></script>
	<script src="<?php echo base_url();?>recursos/js/core.js"></script>
	<script src="<?php echo base_url();?>recursos/js/position.js"></script>
	<script src="<?php echo base_url();?>recursos/js/widget.js"></script>
	<script src="<?php echo base_url();?>recursos/js/button.js"></script>	
	<script src="<?php echo base_url();?>recursos/js/dialog.js"></script>
	<script>
	$(function() {		
		$("#dialog:ui-dialog").dialog("destroy");	
		$("#dialog-message").dialog({
			modal: true,
			buttons: {
				Ok: function() {
					$(this).dialog("close");
				}}				
			
		});
	});
	</script>
  </head>
  <body>
	<div>
		<header id="principal">
		  <div class="aux-space">
			<div id="logo">
			  <a href="#"><img src="<?php echo base_url();?>recursos/imagenes/logo.jpg" alt="logo" id="logo-img"></a>
			</div>
		  </div>
		</header>
	</div>
    <div id="container">
          <div id="content">
            <div id="sing-in">
              <div id="login-box">
                <?php echo form_open();?>
				<table>
					<tr>
						<td>
							<h2>Iniciar sesi&oacute;n<strong>UbicacionES</strong></h2>
						</td>
					</tr>
					<tr class="email-div">
						<td >
							<strong class="email-label">Nombre de usuario</strong>
						</td>
					</tr>
					<tr class="email-div">
						<td>
							<input type="text" spellcheck="false" name="nick" value="">
						</td>
					</tr>
					<tr class="passwd-div">
						<td>
							<strong class="passwd-label">Contrase&ntilde;a</strong>
						</td>
					</tr>
					<tr class="passwd-div">
						<td>
							<input type="password" name="password" value="">
						</td>
					</tr>
					<tr style="text-align:center">
						<td>
							<input type="submit" name="iniciar_sesion" class="button-submit" value="Iniciar sesi&oacute;n">
						</td>
					</tr>
				</table>
              <?php form_close();?>
              </div>
            </div>
          </div>
      <footer>
        <div class="aux-space">
            <script>
              var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
              var diasSemana = new Array("Domingo","Lunes","Martes","Mir&eacute;coles","Jueves","Viernes","S&aacute;bado");
              var f=new Date();
              document.write(diasSemana[f.getDay()] + " " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
            </script>
        </div>
      </footer>
    </div>
	<!-- javascript
    ================================================== -->
    <!-- Situado hasta el final de la pagina para hacerla mas rapida -->
    <script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>    		  
	<script src="<?php echo base_url();?>recursos/js/alert.js"></script>	
	<script src="<?php echo base_url();?>recursos/js/dropdown.js"></script> 
	<script src="<?php echo base_url();?>recursos/js/application.js"></script>
	<script src="<?php echo base_url();?>recursos/js/tooltip.js"></script>   
	<script src="<?php echo base_url();?>recursos/js/popover.js"></script>  
	<script src="<?php echo base_url();?>recursos/js/affix.js"></script>
  </body>
 </html>
