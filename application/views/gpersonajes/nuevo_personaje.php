<?php 
	$url = base_url();
	$form=array('accept-charset'=>'utf8');
	echo form_open('gestion_personajes/validar_registro',$form);
	$tipoper=$this->personaje_model->comboTipoPer();
	$secciones=$this->personaje_model->comboSecciones();
	//<td align="right"><label>Id: *</label></td>
?>
<center>
			<div id="title-page">
				Registro de Personaje</br></br>
			</div>
			
				<p>Los campos marcados con * son requeridos</p></br>
				<table>
					<tr>
						
						<td id="row1"><?php echo form_hidden('idPerson',$this->session->userdata('idPerson'),'maxlength="50" style="height:25px; width:125px" onkeypress="return numeros(event,1)" '); ?>
								
						</td>
					</tr>
					<tr>
						<td align="right"><label>N&uacute;mero: *</label></td>
						<td id="row2"><?php echo form_input('numPerson',$this->session->userdata('numPerson'),'maxlength="50" style="height:25px; width:125px" onkeypress="return numeros(event,2)" '); ?>
								
						</td>
					</tr>
					<tr>
						<td align="right"><label>Tipo de personaje: *</label></td>
						<td><?php echo form_dropdown('tipPerson',$tipoper,$this->session->userdata('tipPerson'),'id="tipPerson" style="width: 175px;" class="dropdown" ');?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>Secci&oacute;n: *</label></td>
						<td><?php echo form_dropdown('idSeccion',$secciones,$this->session->userdata('idSeccion'),'id="idSeccion" style="width: 175px;" class="dropdown" ');?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>Nombre: *</label></td>
						<td ><?php echo form_input('nombre',$this->session->userdata('nombre'),'maxlength="50" style="height:25px; width:230px" '); ?>
								
						</td>
					</tr>
					<tr>
						<td align="right"><label>Apellido: *</label></td>
						<td ><?php echo form_input('apellido',$this->session->userdata('apellido'),'maxlength="50" style="height:25px; width:230px" '); ?>
								
						</td>
					</tr>
					<tr>
						<td align="right"><label>Correo:&nbsp &nbsp </label></td>
						<td ><?php echo form_input('correo',$this->session->userdata('correo'),'maxlength="50" style="height:25px; width:230px" '); ?>
								
						</td>
					</tr>
					<tr>
						<td align="right"><label>Informaci&oacute;n extra:&nbsp &nbsp </label></td>
						<td ><?php echo form_textarea('infoExtra',$this->session->userdata('infoExtra'),'id="infoExtra" style="width: 230px; height:90px"  ');?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>T&eacute;lefono:&nbsp &nbsp </label></td>
						<td ><?php echo form_input('telefono',$this->session->userdata('telefono'),'maxlength="10" style="height:25px; width:125px" '); ?>
								
						</td>
					</tr>
					
					
				</table></center>
				<center><table>
					<tr>
						<td><input type="submit" name="Guardar_Per" value="Guardar personaje" class="button-submit"/></td>
						<td><input type="submit" name="Cancelar" value="Cancelar" class="button-submit"/></td>
					</tr></table></center>
					
<script language="Javascript" type="text/javascript">
	
	function letras(e)
	{
		tecla = e.keyCode || e.which;
		if(tecla!=9){
			if(tecla!=37){
				if(tecla!=39){
					if((tecla>40 && tecla<64))
						{
						alert("Debe ingresar solo letras");
						return false;
						}
					else{
						if((tecla>90 && tecla<97)){
							alert("Debe ingresar solo letras");
							return false;
							}
						else{
							if((tecla>122 && tecla<126)){
								alert("Debe ingresar solo letras");
								return false;
								}
							else{
								patron=/[�!"#$%&\/\()=?�]/;
								codigo_letra=String.fromCharCode(tecla);
								if(patron.test(codigo_letra)){
									alert("Debe ingresar solo letras");
									return false;
									}
								}
							}
						}
					return true;	
					}	
				return true;
				}	
			return true;
			}
	}	
	
	function numeros(e,div1)
	{
		tecla = e.keyCode || e.which;
		//alert(div1);
		var iddiv=div1;
		patron=/[0-9-+()\b]/;
		if(tecla!=9){
			if(tecla!=37){
				if(tecla!=39){
					codigo_letra=String.fromCharCode(tecla);
					if(!patron.test(codigo_letra)){
						var iDiv = document.createElement('div');
						iDiv.id = 'errorId'+div1;
						
						var s = document.getElementById('row'+div1);
						s.appendChild(iDiv);
						document.getElementById ("errorId"+div1).innerHTML="Debe ingresar solo numeros";
						document.getElementById ("errorId"+div1).style.display = 'block';
						document.getElementById ("errorId"+div1).style.color = 'red';
						//alert("Debe ingresar solo numeros");
						return false;
					}
					document.getElementById ("errorId"+div1).style.display = 'none';
					return true;
				}
			document.getElementById ("errorId"+div1).style.display = 'none';
			return true;	
			}		
		document.getElementById ("errorId"+div1).style.display = 'none';
		return true;	
		}	
		document.getElementById ("errorId"+div1).style.display = 'none';
		return true;	
	}
	
	function cambiarDestino(combo){
		// alert('entra');
		// cod=combo.options[combo.selectedIndex].value;		
		// alert(cod);
		var numbers = [combo.length];
		$('#idSeccD').html('');
		for(i = 0;  i < combo.length;  i++) {
			numbers[combo[i].value] = combo[i].text;
        }
		var option = '';
			$.each(numbers, function(val, text) {
            $('#idSeccD').append( $('<option></option>').val(val).html(text) )
            });
			$('#idSeccD').append(option);
			$("#idSeccD").find("option[value='"+combo.options[combo.selectedIndex].value+"']").remove();  	
	}
	
	// function prueba(combo){
		// alert('entra');
		// cod=combo.options[combo.selectedIndex].value;		
		// alert(cod);
		// var dropdown = $('#idSeccD');
	// }
</script>				
<?php echo form_close();?>		