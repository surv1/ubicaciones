<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');
	echo form_open('gestion_personajes/mostrar_datos',$form);
	$personajes=$this->personaje_model->personajes();
?>	
<center>
	<div id="title-page">
		Personajes
	</div>
	<br>
	<p>Seleccione el Personaje que desea Editar.</p>
	<input type="hidden" name="idper" id="idper" readonly="readonly" value="prueba">
	<table id="lista_personajes" height="100px">
		<thead>
			<tr>
				<th>Id</th>
				<th>N&uacute;mero</th>
				<th>Tipo personaje</th>
				<th>Secci&oacute;n</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th></th>								<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($personajes);$i++){
					echo $personajes[$i];
				}
			?>
		</tbody>
	</table>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('#lista_personajes').dataTable({
		"sPaginationType":"full_numbers",
		"oLanguage": {
		"oPaginate": {
        "sFirst": "Primera pagina",
		"sLast": "Ultima pagina",
		"sNext": "Siguiente",
		"sPrevious": "Anterior"},
		"sInfo": "Mostrando _END_ de _TOTAL_ personajes",
		"sInfoEmpty": "No se encontraron personajes para mostrar",
		"sLengthMenu": "Mostrar _MENU_  Personajes por  p&aacute;gina",
		"sProcessing": "Se estan buscando todos los personajes registrados",
		"sZeroRecords":    "No se encontraron personajes",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sSearch": "Buscar:"
		}	
		});
		
		}
	)
</script>
<?php echo form_close();?>