<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');
	echo form_open('gestion_segmentos/mostrar_datos',$form);
	$segmentos=$this->segmento_model->segmentos();
?>	
<center>
	<div id="title-page">
		Segmentos
	</div>
	<br>
	<p>Seleccione el segmento que desea Editar.</p>
	<input type="hidden" name="idsegmento" id="idsegmento" readonly="readonly" value="prueba">
	<table id="lista_segmentos" height="100px">
		<thead>
			<tr>
				<th>Id</th>
				<th>Segmento</th>
				<th>Descripci&oacute;n</th>
				<th>M&iacute;nimo</th>
				<th>M&aacute;ximo</th>
				<th>Empresa</th>
				<th></th>								<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($segmentos);$i++){
					echo $segmentos[$i];
				}
			?>
		</tbody>
	</table>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('#lista_segmentos').dataTable({
		"sPaginationType":"full_numbers",
		"oLanguage": {
		"oPaginate": {
        "sFirst": "Primera pagina",
		"sLast": "Ultima pagina",
		"sNext": "Siguiente",
		"sPrevious": "Anterior"},
		"sInfo": "Mostrando _END_ de _TOTAL_ segmentos",
		"sInfoEmpty": "No se encontraron segmentos para mostrar",
		"sLengthMenu": "Mostrar _MENU_  Segmentos por  p&aacute;gina",
		"sProcessing": "Se estan buscando todos los segmentos registrados",
		"sZeroRecords":    "No se encontraron segmentos",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sSearch": "Buscar:"
		}	
		});
		
		}
	)
	
	function showDialog(id) {
		if(confirm(" �Esta seguro de eliminar el segmento?")) {
			str = id;

			//alert(str);

			$.ajax({

				output_string:'',

				url: '<?php echo base_url().'index.php/gestion_segmentos/deleteSeg';?>',

				type:'POST',

				data: 'id_seg='+str,

				dataType: 'json',

				success: function(output_string){

						$("#mostrarContenido").empty();

						$('#mostrarContenido').append(output_string);

						return false;

					} // End of success function of ajax form

            });  // End of ajax call 
		}
	}
</script>
<?php echo form_close();?>