<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');
	echo form_open('gestion_conexiones/validar_registro',$form);
	$nombSecciones=$this->conexion_model->comboSecciones();
	$destino=$this->session->userdata('idSeccD');
?>
<center>
			<div id="title-page">
				Editar Conexi&oacute;n</br></br>
			</div>
			
				<p>Los campos marcados con * son requeridos</p></br>
				<table>
					<tr>
						<td align="right"><label>Id: *</label></td>
						<td id="row1"><?php echo form_input('idConexion',$this->session->userdata('idConexion'),' readonly=readonly maxlength="50" style="height:25px; width:150px" onkeypress="return numeros(event,1)" '); ?>
								
						</td>
					</tr>
					<tr>
						<td align="right"><label>Secci&oacute;n origen: *</label></td>
						<td ><?php echo form_dropdown('idSeccO',$nombSecciones,$this->session->userdata('idSeccO'),'id="idSeccO" style="width: 200px;" class="dropdown" onChange="cambiarDestino(this)" load="cambiarDestino(this)" ');?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>Secci&oacute;n destino: *</label></td>
						<td ><?php echo form_dropdown('idSeccD',$destino,$this->session->userdata('idSeccD'),'id="idSeccD" style="width: 200px; " class="dropdown" ');?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>Entrada: *</label></td>
						<td id="row2"><?php echo form_input('entrada',$this->session->userdata('entrada'),'" style="height:25px; width:150px" onkeypress="return numeros(event,2)" '); ?>
						</td>
					</tr>
					<tr>
						<td align="right"><label>Salida:* </label></td>
						<td id="row3"><?php echo form_input('salida',$this->session->userdata('salida'),' style="height:25px; width:150px" onkeypress="return numeros(event,3)"  '); ?>
						</td>
					</tr>
				</table></center>
				<center><table>
					<tr>
						<td><input type="submit" name="Guardar_Cambios" value="Actualizar" class="button-submit"/></td>
						<td><input type="submit" name="Cancelar" value="Cancelar" class="button-submit"/></td>
					</tr></table></center>
					
<script language="Javascript" type="text/javascript">
	
	function letras(e)
	{
		tecla = e.keyCode || e.which;
		if(tecla!=9){
			if(tecla!=37){
				if(tecla!=39){
					if((tecla>40 && tecla<64))
						{
						alert("Debe ingresar solo letras");
						return false;
						}
					else{
						if((tecla>90 && tecla<97)){
							alert("Debe ingresar solo letras");
							return false;
							}
						else{
							if((tecla>122 && tecla<126)){
								alert("Debe ingresar solo letras");
								return false;
								}
							else{
								patron=/[�!"#$%&\/\()=?�]/;
								codigo_letra=String.fromCharCode(tecla);
								if(patron.test(codigo_letra)){
									alert("Debe ingresar solo letras");
									return false;
									}
								}
							}
						}
					return true;	
					}	
				return true;
				}	
			return true;
			}
	}	
	
	function numeros(e,div1)
	{
		tecla = e.keyCode || e.which;
		//alert(div1);
		var iddiv=div1;
		patron=/[0-9-+()\b]/;
		if(tecla!=9){
			if(tecla!=37){
				if(tecla!=39){
					codigo_letra=String.fromCharCode(tecla);
					if(!patron.test(codigo_letra)){
						var iDiv = document.createElement('div');
						iDiv.id = 'errorId'+div1;
						
						var s = document.getElementById('row'+div1);
						s.appendChild(iDiv);
						document.getElementById ("errorId"+div1).innerHTML="Debe ingresar solo numeros";
						document.getElementById ("errorId"+div1).style.display = 'block';
						document.getElementById ("errorId"+div1).style.color = 'red';
						//alert("Debe ingresar solo numeros");
						return false;
					}
					document.getElementById ("errorId"+div1).style.display = 'none';
					return true;
				}
			document.getElementById ("errorId"+div1).style.display = 'none';
			return true;	
			}		
		document.getElementById ("errorId"+div1).style.display = 'none';
		return true;	
		}	
		document.getElementById ("errorId"+div1).style.display = 'none';
		return true;	
	}
	
	function cambiarDestino(combo){
		// alert('entra');
		// cod=combo.options[combo.selectedIndex].value;		
		// alert(cod);
		var numbers = [combo.length];
		$('#idSeccD').html('');
		for(i = 0;  i < combo.length;  i++) {
			numbers[combo[i].value] = combo[i].text;
        }
		var option = '';
			$.each(numbers, function(val, text) {
				if(text!=undefined){
					$('#idSeccD').append( $('<option></option>').val(val).html(text) )
				}
            });
			$('#idSeccD').append(option);
			
			$("#idSeccD").find("option[value='"+combo.options[combo.selectedIndex].value+"']").remove();  	
	}
</script>					
<?php echo form_close();?>		