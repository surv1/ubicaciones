<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');
	echo form_open('gestion_conexiones/mostrar_datos',$form);
	$conexiones=$this->conexion_model->conexiones();
?>	
<center>
	<div id="title-page">
		Conexiones
	</div>
	<br>
	<p>Seleccione la Secci&oacute;n que desea Editar.</p>
	<input type="hidden" name="idconexion" id="idconexion" readonly="readonly" value="prueba">
	<table id="lista_conexiones" height="100px">
		<thead>
			<tr>
				<th>Id</th>
				<th>Secci&oacute;n origen</th>
				<th>Secci&oacute;n destino</th>
				<th>Entrada</th>
				<th>Salida</th>
				<th></th>								<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($conexiones);$i++){
					echo $conexiones[$i];
				}
			?>
		</tbody>
	</table>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('#lista_conexiones').dataTable({
		"sPaginationType":"full_numbers",
		"oLanguage": {
		"oPaginate": {
        "sFirst": "Primera pagina",
		"sLast": "Ultima pagina",
		"sNext": "Siguiente",
		"sPrevious": "Anterior"},
		"sInfo": "Mostrando _END_ de _TOTAL_ conexiones",
		"sInfoEmpty": "No se encontraron conexiones para mostrar",
		"sLengthMenu": "Mostrar _MENU_  Conexiones por  p&aacute;gina",
		"sProcessing": "Se estan buscando todos las conexiones registrados",
		"sZeroRecords":    "No se encontraron conexiones",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sSearch": "Buscar:"
		}	
		});
		
		}
	)
</script>
<?php echo form_close();?>