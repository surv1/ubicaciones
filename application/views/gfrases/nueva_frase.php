<?php 

	$url = base_url();

	$form=array('accept-charset'=>'utf8');

	echo form_open('gestion_frases/validar_registro',$form);

	//$personajes=$this->frase_model->comboPersonajes(1);

	$secciones=$this->frase_model->comboSecciones();

	//$pasos=$this->frase_model->comboPasos();

	$facultades=$this->frase_model->comboFacultades();

	$procesos=$this->frase_model->comboProcesos('');
	//<td align="right"><label>Id: *</label></td>

?>

<center>

			<div id="title-page">

				Registro de Nueva Frase</br></br>

			</div>

			

				<p>Los campos marcados con * son requeridos</p></br>

				<table>

					<tr>

						

						<td id="row1"><?php echo form_hidden('idFrase',$this->session->userdata('idFrase'),'maxlength="50" style="height:25px; width:125px" onkeypress="return numeros(event,1)" '); ?>

								

						</td>

					</tr>

					<tr>

						<td align="right"><label>Secci&oacute;n: *</label></td>

						<td ><?php echo form_dropdown('idSecc',$secciones,$this->session->userdata('idSecc'),'id="idSecc" name="idSecc" style="width: 230px;" class="dropdown" onChange="cargarPersonajes(this)" ');?>

						</td>

					</tr>

					<tr>

						<td align="right"><label>Personaje: *</label></td>

						<td ><div id="mostrarContenido">

										<?php $person=$this->session->userdata('idPer');

										if($person>=-1){

											

												$secc=$this->session->userdata('idSecc');

												$personajes=$this->frase_model->comboPersonajes($secc);

												// if($person==-1)

													// echo form_dropdown('idPer',$personajes,'Seleccione...','id="idPer" name="idPer" style="width: 230px;" class="dropdown"  ');

												// else

													echo form_dropdown('idPer',$personajes,$this->session->userdata('idPer'),'id="idPer" name="idPer" style="width: 230px;" class="dropdown"  ');	

												

										}

										else{

											//if($person)

												echo form_dropdown('idPer',array(),$this->session->userdata('idPer'),'id="idPer" name="idPer" style="width: 230px;" class="dropdown"  ');

											}?>

								</div>

						</td>

					</tr>

					<tr>

						<td align="right"><label>Facultad: *</label></td>

						<td ><?php echo form_dropdown('idfac',$facultades,$this->session->userdata('idfac'),'id="idfac" name="idfac" style="width: 230px;" class="dropdown"  onChange="cargarPasos(idProces,this)" ');?>

					</tr>

					<tr>

						<td align="right"><label>Proceso: *</label></td>

						<td ><?php echo form_dropdown('idProces',$procesos,$this->session->userdata('idProces'),'id="idProces" name="idProces" style="width: 230px;" class="dropdown" onChange="cargarPasos(this,idfac)" '); ?>
						</td>

					</tr>

					<tr>

						<td align="right"><label>Paso: *</label></td>
						<td ><div id="mostrarContenidoPas">

										<?php $pas=$this->session->userdata('idPaso');

										if($pas>=-1){

											

												$proce=$this->session->userdata('idProces');

												$fac=$this->session->userdata('idfac');

												$pasosxProc=$this->frase_model->comboPasos($proce,$fac);

												// if($person==-1)

													// echo form_dropdown('idPer',$personajes,'Seleccione...','id="idPer" name="idPer" style="width: 230px;" class="dropdown"  ');

												// else

													echo form_dropdown('idPaso',$pasosxProc,$this->session->userdata('idPaso'),'id="idPaso" name="idPaso" style="width: 230px;" class="dropdown"  ');

												

										}

										else{

											//if($person)

												echo form_dropdown('idPaso',array(),$this->session->userdata('idPaso'),'id="idPaso" name="idPaso" style="width: 230px;" class="dropdown"  ');

											}?>

								</div>

						</td>

						

					</tr>

					<tr>

						<td align="right"><label>Frase:* </label></td>

						<td><?php echo form_textarea('descripFrase',$this->session->userdata('descripFrase'),'rows="1" cols="1"  style="height:90px; width:230px"  '); ?>

						</td>

					</tr>
					

					<tr>

				</table>

				

				</center>

				<center><table>

					<tr>

						<td><input type="submit" name="Guardar_Frase" value="Guardar Frase" class="button-submit"/></td>

						<td><input type="submit" name="Cancelar" value="Cancelar" class="button-submit"/></td>

					</tr></table></center>



<script language="Javascript" type="text/javascript">

		

	function cargarPersonajes(combo) {

		cod=combo.options[combo.selectedIndex].value;

		

		str = cod;

		//alert(str);

		$.ajax({

			output_string:'',

			url: '<?php echo base_url().'index.php/gestion_frases/cargarPersonajes';?>',

			type:'POST',

			data: 'id_secc='+str,

			dataType: 'json',

			success: function(output_string){

					$("#mostrarContenido").empty();

					$('#mostrarContenido').append(output_string);

					return false;

				} // End of success function of ajax form

        });  // End of ajax call 

	}

	
	function cargarProcesos(combo) {

		cod=combo.options[combo.selectedIndex].value;

		

		str = cod;

		alert(str);

		$.ajax({

			output_string1:'',

			url: '<?php echo base_url().'index.php/gestion_frases/cargarProcesos';?>',

			type:'POST',

			data: 'id_fac='+str,

			dataType: 'json',

			success: function(output_string1){

					$("#mostrarContenidoProc").empty();

					$('#mostrarContenidoProc').append(output_string1);

					return false;

				} // End of success function of ajax form

        });  // End of ajax call 

	}	

	function cargarPasos(combo, combo1) {

		cod=combo.options[combo.selectedIndex].value;

		cod1=combo1.options[combo1.selectedIndex].value;

		str = cod;

		str1 = cod1;

		string_comp=str+','+str1;
		//alert(cod1);

		$.ajax({

			output_string2:'',

			url: '<?php echo base_url().'index.php/gestion_frases/cargarPasos';?>',

			type:'POST',

			data: 'todoIds='+string_comp,

			dataType: 'json',

			success: function(output_string2){

					$("#mostrarContenidoPas").empty();

					$('#mostrarContenidoPas').append(output_string2);

					return false;

				} // End of success function of ajax form

        });  // End of ajax call 

	}

	// function cargarOtro(combo){

		// cod=combo.options[combo.selectedIndex].value;

			

			// str = cod;

			// alert(str);

	// }	

</script>		

					

<script language="Javascript" type="text/javascript">

	

	

		

	function letras(e)

	{

		tecla = e.keyCode || e.which;

		if(tecla!=9){

			if(tecla!=37){

				if(tecla!=39){

					if((tecla>40 && tecla<64))

						{

						alert("Debe ingresar solo letras");

						return false;

						}

					else{

						if((tecla>90 && tecla<97)){

							alert("Debe ingresar solo letras");

							return false;

							}

						else{

							if((tecla>122 && tecla<126)){

								alert("Debe ingresar solo letras");

								return false;

								}

							else{

								patron=/[°!"#$%&\/\()=?¡]/;

								codigo_letra=String.fromCharCode(tecla);

								if(patron.test(codigo_letra)){

									alert("Debe ingresar solo letras");

									return false;

									}

								}

							}

						}

					return true;	

					}	

				return true;

				}	

			return true;

			}

	}	

	

	function numeros(e,div1)

	{

		tecla = e.keyCode || e.which;

		//alert(div1);

		var iddiv=div1;

		patron=/[0-9-+()\b]/;

		if(tecla!=9){

			if(tecla!=37){

				if(tecla!=39){

					codigo_letra=String.fromCharCode(tecla);

					if(!patron.test(codigo_letra)){

						var iDiv = document.createElement('div');

						iDiv.id = 'errorId'+div1;

						

						var s = document.getElementById('row'+div1);

						s.appendChild(iDiv);

						document.getElementById ("errorId"+div1).innerHTML="Debe ingresar solo n&uacute;meros";

						document.getElementById ("errorId"+div1).style.display = 'block';

						document.getElementById ("errorId"+div1).style.color = 'red';

						//alert("Debe ingresar solo numeros");

						return false;

					}

					document.getElementById ("errorId"+div1).style.display = 'none';

					return true;

				}

			document.getElementById ("errorId"+div1).style.display = 'none';

			return true;	

			}		

		document.getElementById ("errorId"+div1).style.display = 'none';

		return true;	

		}	

		document.getElementById ("errorId"+div1).style.display = 'none';

		return true;	

	}

	

	

	

</script>				

<?php echo form_close();?>		