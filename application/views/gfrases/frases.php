<?php 
	$url = base_url();
	$form=array('accept-charset'=>'iso-8859-1');
	echo form_open('gestion_frases/mostrar_datos',$form);
	$frases=$this->frase_model->frases();
?>	
<center>
	<div id="title-page">
		Frases
	</div>
	<br>
	<p>Seleccione la Frase que desea Editar.</p>
	<input type="hidden" name="idfrase" id="idfrase" readonly="readonly" value="prueba">
	<table id="lista_frases" height="100px">
		<thead>
			<tr>
				<th>Id</th>
				<th>Secci&oacute;n</th>
				<th>Personaje</th>
				<th>Paso</th>
				<th>Frase</th>

				<th></th>								
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=1;$i<count($frases);$i++){
					echo $frases[$i];
				}
			?>
		</tbody>
	</table>
</center>
<script type="text/javascript">
	$(document).ready(function(){
		$('#lista_frases').dataTable({
		"sPaginationType":"full_numbers",
		"oLanguage": {
		"oPaginate": {
        "sFirst": "Primera pagina",
		"sLast": "Ultima pagina",
		"sNext": "Siguiente",
		"sPrevious": "Anterior"},
		"sInfo": "Mostrando _END_ de _TOTAL_ frases",
		"sInfoEmpty": "No se encontraron frases para mostrar",
		"sLengthMenu": "Mostrar _MENU_  Frases por  p&aacute;gina",
		"sProcessing": "Se estan buscando todos las frases registrados",
		"sZeroRecords":    "No se encontraron frases",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sSearch": "Buscar:"
		}	
		});
		
		}
	)
</script>
<?php echo form_close();?>