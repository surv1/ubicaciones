<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_conexiones extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('conexion_model');
    }
	
	function gConexiones(){
		if($this->session->userdata('eslogeado')) {
			$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
			$datos['workspace'] = "</br>";
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
	}
	
	function nueva_conexion(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gconexiones/nueva_conexion',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
 	
	function consultar_conexion(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gconexiones/conexiones',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
	
	function validar_registro(){
		extract($_POST);
		if(array_key_exists('Cancelar', $_POST)){
			$this->gConexiones();
		}
		extract($_POST);
		$config = array( 
						// array(
						 // 'field'   => 'idConexion', 
						 // 'label'   => 'Id', 
						 // 'rules'   => 'required'
					  // ),
					  array(
						 'field'   => 'idSeccO', 
						 'label'   => 'Secci&oacute;n origen', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'idSeccD', 
						 'label'   => 'Secci&oacute;n destino', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'entrada', 
						 'label'   => 'Entrada', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'salida', 
						 'label'   => 'Salida', 
						 'rules'   => 'required'
					  ) );

			$this->form_validation->set_rules($config);
			$this->form_validation->set_message('required', '%s, se requiere');
			$this->form_validation->set_error_delimiters('&diams; &nbsp;', '<br/>');
		if ($this->form_validation->run()==FALSE ){
			if(array_key_exists('Guardar_Conexion', $_POST)){
				$this->getValores($idConexion,$idSeccO,'',$entrada,$salida);
				$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gconexiones/nueva_conexion','',true);	
				$this->load->view('plantilla/plantilla',$datos);
				
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
									<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
				$msg['type'] = "error_message";
				//$msg['view'] = "gsecciones/nueva_seccion";
				$this->load->view('plantilla/message',$msg);
				
			}
			else if(array_key_exists('Guardar_Cambios', $_POST)){
					$this->getValores($idConexion,$idSeccO,$idSeccD,$entrada,$salida);
					$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gconexiones/editar_seccion',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
										<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuario/editar_usuario";
					$this->load->view('plantilla/message',$msg);
				}
		}	
		else{
				if(array_key_exists('Guardar_Conexion', $_POST))
					$this->ingresar();
				else if(array_key_exists('Guardar_Cambios', $_POST)){
							$this->actualizar_datos();
						}				
			}
	}
	
	function ingresar(){
		extract($_POST);
		$existe=$this->conexion_model->validarConexion($idConexion);
		if(!$existe){
			if($idSeccO=="-1"){
				$this->getValores($idConexion,$idSeccO,$idSeccD,$entrada,$salida);
				$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gconexiones/nueva_conexion',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Campo requerido. 
											<br></b><br><br> 
											&diams; Debe seleccionar la secci&oacute;n de origen.";					
				$msg['type'] = "error_message";
				//$msg['view'] = "gusuarios/nuevo_usuario";
				$this->load->view('plantilla/message',$msg);
			}
			else{
				if($idSeccD=='-1'){
					$this->getValores($idConexion,$idSeccO,$idSeccD,$entrada,$salida);
					$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gconexiones/nueva_conexion',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Campo requerido. 
												<br></b><br><br> 
												&diams; Debe seleccionar la secci&oacute;n destino.";					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuarios/nuevo_usuario";
					$this->load->view('plantilla/message',$msg);
				}
				else{										$verNum=$this->conexion_model->getVerNum();					
					$datos = array(
					'CONEX_ID' => $idConexion,
					'SECC_O_ID' => $idSeccO,
					'SECC_D_ID' => $idSeccD,
					'ENTRADA' => $entrada,
					'SALIDA' => $salida,
					'INSTI_ID' => 1,										
					'VER_NUM' => $verNum
					);
					$this->conexion_model->saveConexion($datos);
					$nombSec=$this->conexion_model->nombSeccion($idSeccO);
					$idSeccO=$nombSec->row('SECC_NOMBRE');
					$nombSec=$this->conexion_model->nombSeccion($idSeccD);
					$idSeccD=$nombSec->row('SECC_NOMBRE');
					$this->nueva_conexion();
					$msg['title'] = "Registro Finalizado";
					$msg['mesage'] = 	"Los datos de la conexi&oacute;n han sido almacenados con &eacute;xito
										
										<p> Secci&oacute;n origen: ".$idSeccO."</p>
										<p> Secci&oacute;n destino: ".$idSeccD."</p>	
										<p> Entrada: ".$entrada."</p>
										<p> Salida: ".$salida."</p> ";					
					$msg['type'] = "information_message";
					$this->load->view('plantilla/message',$msg);
				}
			}
		}
		else{
			$this->getValores($idConexion,$idSeccO,$idSeccD,$entrada,$salida);
			$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gconexiones/nueva_conexion',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Conexi&oacute;n ya existente";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}	
	}
	
	function mostrar_datos() 
    {
		extract($_POST);
		if(array_key_exists('Editar', $_POST))
			{
			$this->limpia();
			$edicion_s=$this->conexion_model->obtener($idconexion);
			$this->session->set_userdata('idConexion',$edicion_s->row('CONEX_ID'));
			$this->session->set_userdata('idSeccO',$edicion_s->row('SECC_O_ID'));
			$des[$edicion_s->row('SECC_D_ID')]=$edicion_s->row('DESTINO');
			$this->session->set_userdata('idSeccD',$des);
			$this->session->set_userdata('entrada',$edicion_s->row('ENTRADA'));
			$this->session->set_userdata('salida',$edicion_s->row('SALIDA'));
			$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gconexiones/editar_conexion',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
		}     
		if(array_key_exists('Eliminar', $_POST)){
			$edicion_s=$this->conexion_model->obtener($idconexion);
			$eliminar=$this->conexion_model->deleteConexion($idconexion);
			$msg['title'] = "Eliminaci&oacute;n";
			if($eliminar){				
				
				$msg['mesage'] = 	"<b>Resultado</b>
											<p>La conexi&oacute;  \"".$edicion_s->row('CONEX_ID')."\" ha sido eliminada con &eacute;xito.</p>
											";					
				
			}
			
			// else{
				// $msg['mesage'] = 	"<b>Resultado</b>
											// <p><b>El segmento \"".$edicion_s->row('SEG_NOMBRE')."\" no se puede eliminar.</b></p>
											// <p>Tiene dependencias.</p>";					
			// }
			
			
			$this->consultar_conexion();
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		} 
	}
	
	function actualizar_datos()     
   {         	
		extract($_POST);
		if($idSeccO=="-1"){
				$this->getValores($idConexion,$idSeccO,$idSeccD,$entrada,$salida);
				$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gconexiones/nueva_conexion',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Campo requerido. 
											<br></b><br><br> 
											&diams; Debe seleccionar la secci&oacute;n de origen.";					
				$msg['type'] = "error_message";
				//$msg['view'] = "gusuarios/nuevo_usuario";
				$this->load->view('plantilla/message',$msg);
			}
			else{
				if($idSeccD=='-1'){
					$this->getValores($idConexion,$idSeccO,$idSeccD,$entrada,$salida);
					$datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gconexiones/nueva_conexion',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Campo requerido. 
												<br></b><br><br> 
												&diams; Debe seleccionar la secci&oacute;n destino.";					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuarios/nuevo_usuario";
					$this->load->view('plantilla/message',$msg);
				}
				else{										$verNum=$this->conexion_model->getVerNum();
					$datos = array(
					'CONEX_ID' => $idConexion,
					'SECC_O_ID' => $idSeccO,
					'SECC_D_ID' => $idSeccD,
					'ENTRADA' => $entrada,
					'SALIDA' => $salida,
					'INSTI_ID' => 1,										'VER_NUM' => $verNum
					);
					$this->conexion_model->updateConexion($idConexion,$datos);
					$nombSec=$this->conexion_model->nombSeccion($idSeccO);
					$idSeccO=$nombSec->row('SECC_NOMBRE');
					$nombSec=$this->conexion_model->nombSeccion($idSeccD);
					$idSeccD=$nombSec->row('SECC_NOMBRE');
					$this->consultar_conexion();
					$msg['title'] = "Registro Finalizado";
					$msg['mesage'] = 	"Los datos de la conexi&oacute;n han sido almacenados con &eacute;xito
										<p> Id: ".$idConexion."</p>		
										<p> Secci&oacute;n origen: ".$idSeccO."</p>
										<p> Secci&oacute;n destino: ".$idSeccD."</p>	
										<p> Entrada: ".$entrada."</p>
										<p> Salida: ".$salida."</p> ";					
					$msg['type'] = "information_message";
					$this->load->view('plantilla/message',$msg);
				}
			}
	}	
	
	function limpia(){
		$this->session->set_userdata('idConexion','');
		$this->session->set_userdata('idSeccO','Seleccione...');
		$this->session->set_userdata('idSeccD','');
		$this->session->set_userdata('entrada','');
		$this->session->set_userdata('salida','');
	}
	
	function getValores($id,$idsecco,$idseccd,$entrada,$salida){
		$this->session->set_userdata('idConexion',$id);
		$this->session->set_userdata('idSeccO',$idsecco);
		$this->session->set_userdata('idSeccD',$idseccd);
		$this->session->set_userdata('entrada',$entrada);
		$this->session->set_userdata('salida',$salida);
	}
	
}

 ?>