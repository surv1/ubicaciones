<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_tipopersonaje extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('tipopersonaje_model');
    }
	
	function gTipoPer(){
		if($this->session->userdata('eslogeado')) {
			$datos['sidebar'] = $this->load->view('gtipopersonajes/sidebar',"",true);
			$datos['workspace'] = "</br>";
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
	}
	
	function nuevo_tipoper(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gtipopersonajes/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gtipopersonajes/nuevo_tipoperson',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
 	
	function consultar_tipoper(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gtipopersonajes/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gtipopersonajes/tipoperson',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
	
	function validar_registro(){
		extract($_POST);
		if(array_key_exists('Cancelar', $_POST)){
			$this->gTipoPer();
		}
		extract($_POST);
		$config = array( 												// array(
						 // 'field'   => 'idTipoPer', 
						 // 'label'   => 'Id', 
						 // 'rules'   => 'required' ),					  					  					  					  	
						 array(
						 'field'   => 'nombTipoPer', 
						 'label'   => 'Nombre del tipo de personaje', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'descripTipoPer', 
						 'label'   => 'Descripci&oacute;n', 
						 'rules'   => 'required'
					  ) );

			$this->form_validation->set_rules($config);
			$this->form_validation->set_message('required', '%s, se requiere');
			$this->form_validation->set_error_delimiters('&diams; &nbsp;', '<br/>');
		if ($this->form_validation->run()==FALSE ){
			if(array_key_exists('Guardar_TipoPerson', $_POST)){
				$this->getValores($idTipoPer,$nombTipoPer,$descripTipoPer);
				$datos['sidebar'] = $this->load->view('gtipopersonajes/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gtipopersonajes/nuevo_tipoperson','',true);	
				$this->load->view('plantilla/plantilla',$datos);
				
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
									<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
				$msg['type'] = "error_message";
				//$msg['view'] = "gsecciones/nueva_seccion";
				$this->load->view('plantilla/message',$msg);
				
			}
			else if(array_key_exists('Actualizar_TipoPer', $_POST)){
					$this->getValores($idTipoPer,$nombTipoPer,$descripTipoPer);
					$datos['sidebar'] = $this->load->view('gtipopersonajes/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gtipopersonajes/editar_proceso',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
										<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuario/editar_usuario";
					$this->load->view('plantilla/message',$msg);
				}
		}	
		else{
				if(array_key_exists('Guardar_TipoPerson', $_POST))
					$this->ingresar();
				else if(array_key_exists('Actualizar_TipoPer', $_POST)){
							$this->actualizar_datos();
						}				
			}
	}
	
	function ingresar(){
		extract($_POST);
		$existe=$this->tipopersonaje_model->validarTipoPer($nombTipoPer);
		if(!$existe){
			$verNum=$this->tipopersonaje_model->getVerNum();

			$datos = array(

			//'TIPER_ID' => $idTipoPer,

			'TIPER_NOMBRE' => $nombTipoPer,

			'TIPER_DESC' => $descripTipoPer,
			'VER_NUM' => $verNum 

			);
			$this->tipopersonaje_model->saveTipoPer($datos);
			
			$this->nuevo_tipoper();
			$msg['title'] = "Registro Finalizado";
			$msg['mesage'] = 	"Los datos del tipo de personaje han sido almacenados con &eacute;xito
								<p> Tipo de personaje: ".$nombTipoPer."</p>
								<p> Descripci&oacute;n: ".$descripTipoPer."</p> ";					
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
		else{
			$this->getValores($idTipoPer,$nombTipoPer,$descripTipoPer);
			$datos['sidebar'] = $this->load->view('gtipopersonajes/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gtipopersonajes/nuevo_tipoperson',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Tipo de personaje ya existente";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}	
	}
	
	function mostrar_datos() 
    {
		extract($_POST);
		if(array_key_exists('Editar', $_POST))
			{
			$this->limpia();
			$edicion_s=$this->tipopersonaje_model->obtener($idtipoper);
			$this->session->set_userdata('idTipoPer',$edicion_s->row('TIPER_ID'));
			$this->session->set_userdata('nombTipoPer',$edicion_s->row('TIPER_NOMBRE'));
			//$des[$edicion_s->row('SECC_D_ID')]=$edicion_s->row('DESTINO');
			$this->session->set_userdata('descripTipoPer',$edicion_s->row('TIPER_DESC'));
			?>
            <?= form_open(base_url() . 'index.php/gestion_tipopersonaje/validar_registro','') ?>
			<?php
			$datos['sidebar'] = $this->load->view('gtipopersonajes/sidebar',"",true);
			$datos['workspace'] = "<center>
			<div id='title-page'>
				Editar Tipo de Personaje
			</div>
				<p></p>
				<input type='hidden' style='height:25px' name='id' value=".$this->session->userdata('idTipoPer')."><center></center>
				<table>
					<tr>
						<td align='right'><label>Id: *</label></td>
						<td ><input type='text' readonly='readonly' style='height:25px;width:150px' name='idTipoPer' value='".$this->session->userdata('idTipoPer')."'><center></center>
						</td>
					</tr>

					<tr>
						<td align='right'><label>Tipo personaje: *</label></td>
						<td ><input type='text' readonly='readonly' style='height:25px;width:230px' maxlength='50' name='nombTipoPer' value='".$this->session->userdata('nombTipoPer')."'><center></center>
						</td>
					</tr>
					<tr>
						<td align='right'><label>Descripci&oacute;n: * </label></td>
						<td><textarea  style='height:70px;width:230px' name='descripTipoPer' >".$this->session->userdata('descripTipoPer')."</textarea><center></center>
						</td>
					</tr>
				</table></center>
				<center><table>
					<tr>
						<td><input type='submit' name='Actualizar_TipoPer' value='Actualizar' class='button-submit'/></td>
						<td><input type='submit' name='Cancelar' value='Cancelar' class='button-submit'/></td>
					</tr></table></center>";	
			$this->load->view('plantilla/plantilla',$datos);
		}     
		
		if(array_key_exists('Eliminar', $_POST)){
			$edicion_s=$this->tipopersonaje_model->obtener($idtipoper);
			$eliminar=$this->tipopersonaje_model->deleteTipPersonaje($idtipoper);
			$msg['title'] = "Eliminaci&oacute;n";
			if($eliminar){				
				
				$msg['mesage'] = 	"<b>Resultado</b>
											<p>El tipo de personaje  \"".$edicion_s->row('TIPER_NOMBRE')."\" ha sido eliminado con &eacute;xito.</p>
											";					
				
			}
			else{
				$msg['mesage'] = 	"<b>Resultado</b>
											<p><b>El tipo de personaje \"".$edicion_s->row('TIPER_NOMBRE')."\" no se puede eliminar.</b></p>
											<p>Tiene dependencias.</p>";					
			}
			$this->consultar_tipoper();
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
	}
	
	function actualizar_datos()     
   {         	
		extract($_POST);
		$verNum=$this->tipopersonaje_model->getVerNum();
		$datos = array(

			'TIPER_DESC' => $descripTipoPer,
			'VER_NUM' => $verNum 

			);
			$this->tipopersonaje_model->updateTipoPer($idTipoPer,$datos);
			
			$this->consultar_tipoper();
			$msg['title'] = "Registro Finalizado";
			$msg['mesage'] = 	"Los datos del tipo de personaje han sido almacenados con &eacute;xito
								<p> Id: ".$idTipoPer."</p>	
								<p> Tipo de personaje: ".$nombTipoPer."</p>
								<p> Descripci&oacute;n: ".$descripTipoPer."</p> ";					
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
	}	
	

	function limpia(){
		$this->session->set_userdata('idTipoPer','');
		$this->session->set_userdata('nombTipoPer','');
		$this->session->set_userdata('descripTipoPer','');
	}
	
	function getValores($idTipoPer,$nombTipoPer,$descripTipoPer){
		$this->session->set_userdata('idTipoPer',$idTipoPer);
		$this->session->set_userdata('nombTipoPer',$nombTipoPer);
		$this->session->set_userdata('descripTipoPer',$descripTipoPer);
	}
	
}

 ?>