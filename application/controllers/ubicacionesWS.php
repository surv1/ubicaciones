<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ubicacionesWS extends CI_Controller
{	 
     function __construct()
     {
        parent::__construct();
        $ns = 'http://'.$_SERVER['SERVER_NAME'].'/GestorUbicacionES/index.php/ubicacionesWS/';
		$this->load->model('segmento_model');
		$this->load->model('actualizacion_model');
        $this->load->library("Nusoap_library"); // load nusoap toolkit library in controller
        $this->nusoap_server = new soap_server(); // create soap server object
        $this->nusoap_server->configureWSDL("portType", $ns); // wsdl cinfiguration
        $this->nusoap_server->wsdl->schemaTargetNamespace = $ns; // server namespace
		$input_array = array ('id_base' => "xsd:string"); 
		$return_array = array ("return" => "xsd:string");
		$input_array2 = array ('id_dispositivo' => "xsd:string"); 
		$input_array3 = array ('valor' => "xsd:string"); 
		$this->nusoap_server->register('traerScript', $input_array, $return_array, "urn:ubicacionesWSWSDL", "urn:".$ns."/traerScript", "rpc", "encoded", "Retorna el script de insercion de la base.");
		$this->nusoap_server->register('registrarDispositivo', $input_array2, $return_array, "urn:ubicacionesWSWSDL", "urn:".$ns."/registrarDispositivo", "rpc", "encoded", "Registra el dispositivo en la base.");
		$this->nusoap_server->register('existeActualizacion', $input_array3, $return_array, "urn:ubicacionesWSWSDL", "urn:".$ns."/existeActualizacion", "rpc", "encoded", "Valida si existe alguna version de la base mas nueva.");

     }
	 
	 function index()
	{
		function traerScript($id_base)
		{
			$soap = new ubicacionesWS();
			$c = $soap->segmento_model->traerScript($id_base);
			return $c;
		}
		
		function registrarDispositivo($id_dispositivo)
		{
			$soap = new ubicacionesWS();
			$datos = array(
				'DISP_ID' => $id_dispositivo,
				'DISP_FECHA' => date("y.m.d"),
				'DISP_PLATAFORMA' => $id_dispositivo
				);
			$soap->actualizacion_model->saveDispositivo($datos);
			return "Se inserto correctamente.";
		}
		
		function existeActualizacion($valor)
		{
			$soap = new ubicacionesWS();
			$pos = strpos($valor, ",");
			if ($pos === false) {
				return "No se envia informacion correcta";
			}
			else {
			
				$id_base = substr($valor, 0, $pos);
				$num_ver = substr($valor, $pos+1);
				if ($num_ver == $soap->actualizacion_model->getVerNum()) {
					return "0";
				}
				else {
					return "1";
				}
			}
		}
		$this->nusoap_server->service(file_get_contents("php://input")); // read raw data from request body
		

	}
}
?>