<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_personajes extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('personaje_model');
    }
	
	function gPersonaje(){
		if($this->session->userdata('eslogeado')) {
			$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
			$datos['workspace'] = "</br>";
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
	}
	
	function nuevo_personaje(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gpersonajes/nuevo_personaje',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
 	
	function consultar_personaje(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gpersonajes/personajes',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
	
	function validar_registro(){
		extract($_POST);
		if(array_key_exists('Cancelar', $_POST)){
			$this->gPersonaje();
		}
		extract($_POST);
		$config = array( 
						// array(
						 // 'field'   => 'idPerson', 
						 // 'label'   => 'Id', 
						 // 'rules'   => 'required'
					  // ),
					  array(
						 'field'   => 'numPerson', 
						 'label'   => 'N&uacute;mero', 
						 'rules'   => 'required'
					  ), array(
						 'field'   => 'tipPerson', 
						 'label'   => 'Tipo de personaje', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'idSeccion', 
						 'label'   => 'Secci&oacute:n', 
						 'rules'   => 'required'
					  ), array(
						 'field'   => 'nombre', 
						 'label'   => 'Nombre', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'apellido', 
						 'label'   => 'Apellido', 
						 'rules'   => 'required'
					  ) );

			$this->form_validation->set_rules($config);
			$this->form_validation->set_message('required', '%s, se requiere');
			$this->form_validation->set_error_delimiters('&diams; &nbsp;', '<br/>');
		if ($this->form_validation->run()==FALSE ){
			if(array_key_exists('Guardar_Per', $_POST)){
				$this->getValores($idPerson,$numPerson,$tipPerson,$idSeccion,$nombre,$apellido,$correo,$infoExtra,$telefono);
				$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gpersonajes/nuevo_personaje','',true);	
				$this->load->view('plantilla/plantilla',$datos);
				
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
									<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
				$msg['type'] = "error_message";
				//$msg['view'] = "gsecciones/nueva_seccion";
				$this->load->view('plantilla/message',$msg);
				
			}
			else if(array_key_exists('Guardar_Personaje', $_POST)){
					$this->getValores($idPerson,$numPerson,$tipPerson,$idSeccion,$nombre,$apellido,$correo,$infoExtra,$telefono);
					$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gpersonajes/editar_personaje',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
										<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuario/editar_usuario";
					$this->load->view('plantilla/message',$msg);
				}
		}	
		else{
				if(array_key_exists('Guardar_Per', $_POST))
					$this->ingresar();
				else if(array_key_exists('Guardar_Personaje', $_POST)){
							$this->actualizar_datos();
						}				
			}
	}
	
	function ingresar(){
		extract($_POST);
		$existe=$this->personaje_model->validarPersonaje($numPerson,$idSeccion);
		if(!$existe){
			if($tipPerson==0){
				$this->getValores($idPerson,$numPerson,$tipPerson,$idSeccion,$nombre,$apellido,$correo,$infoExtra,$telefono);
				$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gpersonajes/nuevo_personaje',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Campo requerido. 
											<br></b><br><br> 
											&diams; Debe seleccionar el tipo de personaje .";					
				$msg['type'] = "error_message";
				//$msg['view'] = "gusuarios/nuevo_usuario";
				$this->load->view('plantilla/message',$msg);
			}
			else{
				if($idSeccion==0){
					$this->getValores($idPerson,$numPerson,$tipPerson,$idSeccion,$nombre,$apellido,$correo,$infoExtra,$telefono);
					$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gpersonajes/nuevo_personaje',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Campo requerido. 
												<br></b><br><br> 
												&diams; Debe seleccionar la secci&oacute;n .";					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuarios/nuevo_usuario";
					$this->load->view('plantilla/message',$msg);
				}
				else{					
				
				$verNum=$this->personaje_model->getVerNum();
					$datos = array(
					//'PER_ID' => $idPerson,
					
					
					'PER_NUM' => $numPerson,
					'TIPER_ID' => $tipPerson,
					'SECC_ID' => $idSeccion,
					'PER_NOMBRE' => $nombre,
					'PER_APELLIDO' => $apellido,
					'PER_CORREO' => $correo,
					'PER_INFO_EXTRA' => $infoExtra,
					'PER_TELOFI_UES' => $telefono,
					'INSTI_ID' => 1,	
					'VER_NUM' => $verNum
					);
					$this->personaje_model->savePerson($datos);
					
					$tipo=$this->personaje_model->nombTipo($tipPerson);
					$tipPerson=$tipo->row('TIPER_NOMBRE');
					$seccion=$this->personaje_model->nombSecc($idSeccion);
					$idSeccion=$seccion->row('SECC_NOMBRE');

											
					$this->nuevo_personaje();
					$msg['title'] = "Registro Finalizado";
					$msg['mesage'] = 	"Los datos del personaje han sido almacenados con &eacute;xito
										
										<p> N&uacute;mero: ".$numPerson."</p>
										<p> Tipo de personaje: ".$tipPerson."</p>	
										<p> Secci&oacute;n: ".$idSeccion."</p> 
										<p> Nombre: ".$nombre."</p>	
										<p> Apellido: ".$apellido."</p>
										<p> Correo: ".$correo."</p>	
										<p> Informaci&oacute;n adicional: ".$infoExtra."</p>
										<p> T&eacute;lefono: ".$telefono."</p>	";					
					$msg['type'] = "information_message";
					$this->load->view('plantilla/message',$msg);
				}
			}
			
		}
		else{
			$this->getValores($idPerson,$numPerson,$tipPerson,$idSeccion,$nombre,$apellido,$correo,$infoExtra,$telefono);
			$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gpersonajes/nuevo_personaje',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Personaje ya existente";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}	
	}
	
	function mostrar_datos() 
    {
		extract($_POST);
		if(array_key_exists('Editar', $_POST))
			{
			$this->limpia();
			$edicion_s=$this->personaje_model->obtener($idper);
			$this->session->set_userdata('idPerson',$edicion_s->row('PER_ID'));
			$this->session->set_userdata('numPerson',$edicion_s->row('PER_NUM'));
			//$des[$edicion_s->row('SECC_D_ID')]=$edicion_s->row('DESTINO');
			$this->session->set_userdata('tipPerson',$edicion_s->row('TIPER_ID'));
			$this->session->set_userdata('idSeccion',$edicion_s->row('SECC_ID'));
			$this->session->set_userdata('nombre',$edicion_s->row('PER_NOMBRE'));
			$this->session->set_userdata('apellido',$edicion_s->row('PER_APELLIDO'));
			$this->session->set_userdata('correo',$edicion_s->row('PER_CORREO'));
			$this->session->set_userdata('infoExtra',$edicion_s->row('PER_INFO_EXTRA'));
			$this->session->set_userdata('telefono',$edicion_s->row('PER_TELOFI_UES'));
			$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gpersonajes/editar_personaje',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
		}     
		
		if(array_key_exists('Eliminar', $_POST)){
			$edicion_s=$this->personaje_model->obtener($idper);
			$eliminar=$this->personaje_model->deletePersonaje($idper);
			$msg['title'] = "Eliminaci&oacute;n";
			if($eliminar){				
				
				$msg['mesage'] = 	"<b>Resultado</b>
											<p>El personaje  \"".$edicion_s->row('PER_NOMBRE')." ".$edicion_s->row('PER_APELLIDO')."\" ha sido eliminado con &eacute;xito.</p>
											";					
				
			}
			else{
				$msg['mesage'] = 	"<b>Resultado</b>
											<p><b>El personaje \"".$edicion_s->row('PER_NOMBRE')." ".$edicion_s->row('PER_APELLIDO')."\" no se puede eliminar.</b></p>
											<p>Tiene dependencias.</p>";					
			}
			$this->consultar_personaje();
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
	}
	
	function actualizar_datos()     
   {         	
		extract($_POST);
		if($tipPerson==0){
				$this->getValores($idPerson,$numPerson,$tipPerson,$idSeccion,$nombre,$apellido,$correo,$infoExtra,$telefono);
				$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gpersonajes/editar_personaje',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Campo requerido. 
											<br></b><br><br> 
											&diams; Debe seleccionar el tipo de personaje .";					
				$msg['type'] = "error_message";
				//$msg['view'] = "gusuarios/nuevo_usuario";
				$this->load->view('plantilla/message',$msg);
			}
			else{
				if($idSeccion==0){ //Ya que la seccion cero no tiene personajes
					$this->getValores($idPerson,$numPerson,$tipPerson,$idSeccion,$nombre,$apellido,$correo,$infoExtra,$telefono);
					$datos['sidebar'] = $this->load->view('gpersonajes/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gpersonajes/editar_personaje',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Campo requerido. 
												<br></b><br><br> 
												&diams; Debe seleccionar la secci&oacute;n .";					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuarios/nuevo_usuario";
					$this->load->view('plantilla/message',$msg);
				}
				else{					
				
				$verNum=$this->personaje_model->getVerNum();
					$datos = array(
					//'PER_ID' => $idPerson,
					
					'PER_NUM' => $numPerson,
					'TIPER_ID' => $tipPerson,
					'SECC_ID' => $idSeccion,
					'PER_NOMBRE' => $nombre,
					'PER_APELLIDO' => $apellido,
					'PER_CORREO' => $correo,
					'PER_INFO_EXTRA' => $infoExtra,
					'PER_TELOFI_UES' => $telefono,
					'INSTI_ID' => 1,										
					'VER_NUM' => $verNum
					);
					$this->personaje_model->updatePersonaje($idPerson, $datos);
					
					$tipo=$this->personaje_model->nombTipo($tipPerson);
					$tipPerson=$tipo->row('TIPER_NOMBRE');
					$seccion=$this->personaje_model->nombSecc($idSeccion);
					$idSeccion=$seccion->row('SECC_NOMBRE');

											
					$this->consultar_personaje();
					$msg['title'] = "Registro Finalizado";
					$msg['mesage'] = 	"Los datos del personaje han sido almacenados con &eacute;xito
										<p> Id: ".$idPerson."</p>	
										<p> N&uacute;mero: ".$numPerson."</p>
										<p> Tipo de personaje: ".$tipPerson."</p>	
										<p> Secci&oacute;n: ".$idSeccion."</p> 
										<p> Nombre: ".$nombre."</p>	
										<p> Apellido: ".$apellido."</p>
										<p> Correo: ".$correo."</p>	
										<p> Informaci&oacute;n adicional: ".$infoExtra."</p>
										<p> T&eacute;lefono: ".$telefono."</p>	";					
					$msg['type'] = "information_message";
					$this->load->view('plantilla/message',$msg);
				}
			}
	}	
	
	function limpia(){
		$this->session->set_userdata('idPerson','');
		$this->session->set_userdata('numPerson','');
		$this->session->set_userdata('tipPerson','');
		$this->session->set_userdata('idSeccion','');
		$this->session->set_userdata('nombre','');
		$this->session->set_userdata('apellido','');
		$this->session->set_userdata('infoExtra','');
		$this->session->set_userdata('telefono','');
	}
	
	function getValores($idPerson,$numPerson,$tipPerson,$idSeccion,$nombre,$apellido,$correo,$infoExtra,$telefono){
		$this->session->set_userdata('idPerson',$idPerson);
		$this->session->set_userdata('numPerson',$numPerson);
		$this->session->set_userdata('tipPerson',$tipPerson);
		$this->session->set_userdata('idSeccion',$idSeccion);
		$this->session->set_userdata('nombre',$nombre);
		$this->session->set_userdata('apellido',$apellido);
		$this->session->set_userdata('correo',$correo);
		$this->session->set_userdata('infoExtra',$infoExtra);
		$this->session->set_userdata('telefono',$telefono);
	}
	
}

 ?>