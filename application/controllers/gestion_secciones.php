<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_secciones extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('seccion_model');
    }
	
	function gSecciones(){
		if($this->session->userdata('eslogeado')) {
			$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
			$datos['workspace'] = "</br>";
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
	}
	
	function nueva_seccion(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gsecciones/nueva_seccion',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
 	
	function consultar_seccion(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gsecciones/secciones',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
	
	function validar_registro(){
		extract($_POST);
		if(array_key_exists('Cancelar', $_POST)){
			$this->gSecciones();
		}
		extract($_POST);
		$config = array( 
						array(
						 'field'   => 'idSeccion', 
						 'label'   => 'Id', 
						 'rules'   => 'required'
					  ),
					  array(
						 'field'   => 'idSegmento', 
						 'label'   => 'Segmento', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'nombSeccion', 
						 'label'   => 'Nombre de la secci&oacute;n', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'descripSeccion', 
						 'label'   => 'Descripci&oacute;n', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'tipoInstalac', 
						 'label'   => 'Tipo de instalaci&oacute;n', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'brujula', 
						 'label'   => 'Br&uacute;jula', 
						 'rules'   => 'required'
					  ) );

			$this->form_validation->set_rules($config);
			$this->form_validation->set_message('required', '%s, se requiere');
			$this->form_validation->set_error_delimiters('&diams; &nbsp;', '<br/>');
		if ($this->form_validation->run()==FALSE ){
			if(array_key_exists('Guardar_Seccion', $_POST)){
				$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);
				$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gsecciones/nueva_seccion','',true);	
				$this->load->view('plantilla/plantilla',$datos);
				
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
									<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
				$msg['type'] = "error_message";
				//$msg['view'] = "gsecciones/nueva_seccion";
				$this->load->view('plantilla/message',$msg);
				
			}
			else if(array_key_exists('Guardar_Cambios', $_POST)){
					$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);
					$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gsecciones/editar_seccion',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
										<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuario/editar_usuario";
					$this->load->view('plantilla/message',$msg);
				}
		}	
		else{
				if(array_key_exists('Guardar_Seccion', $_POST))
					$this->ingresar();
				else if(array_key_exists('Guardar_Cambios', $_POST)){
							$this->actualizar_datos();
						}				
			}
	}
	
	function ingresar(){
		extract($_POST);
		$existe=$this->seccion_model->validarSeccion($idSeccion);
		if(!$existe){
			if($idSegmento=="0"){
				$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);
				$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gsecciones/nueva_seccion',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Campo requerido. 
											<br></b><br><br> 
											&diams; Debe seleccionar el segmento al que pertenece.";					
				$msg['type'] = "error_message";
				
				$this->load->view('plantilla/message',$msg);
			}
			else{
				if($tipoInstalac=='0'){
					$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);
					$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gsecciones/nueva_seccion',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Campo requerido. 
												<br></b><br><br> 
												&diams; Debe seleccionar que tipo de instalaci&oacute;n es.";					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuarios/nuevo_usuario";
					$this->load->view('plantilla/message',$msg);
				}
				else{										
					if($brujula=='0'){																		
						$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);
						
						$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);												
						
						$datos['workspace'] = $this->load->view('gsecciones/nueva_seccion',"",true);													
						
						$this->load->view('plantilla/plantilla',$datos);												
						
						$msg['title'] = "Existen campos vacios";												

						$msg['mesage'] = 	"<b>Campo requerido. 																										<br></b><br><br> 																										&diams; Debe seleccionar un valor para br&oacute;jula.";																								$msg['type'] = "error_message";																			
					//$msg['view'] = "gusuarios/nuevo_usuario";						
						$this->load->view('plantilla/message',$msg);
					}										
					else{																		
						
						$rangos=$this->seccion_model->validarRango($idSegmento);												
						
						$min=$rangos->row('SEG_MIN');												
						
						$max=$rangos->row('SEG_MAX');												
						
						if($idSeccion>=$min AND $idSeccion<=$max){														
							$verNum=$this->seccion_model->getVerNum();	
							if ($seccAula=="0") {
								$seccAula=null;
							}

							$datos = array(															
						
								'SECC_ID' => $idSeccion,															
								
								'SEG_ID' => $idSegmento,															
								
								'SECC_NOMBRE' => $nombSeccion,															
								
								'SECC_DESC' => $descripSeccion,															
								
								'SECCTIPO_INST' => $tipoInstalac,															
								
								'SECC_BRUJULA' => $brujula,															
								
								'INSTI_ID' => 1,																						
								
								'VER_NUM' => $verNum,																						
								
								'SECC_MAP' => $mapa,

								'idAula' => $seccAula														
							);														

							$this->seccion_model->saveSeccion($datos);	

							//Datos para puntos GPS
							//Punto 1
							$datos_ptogps1 = array(															
						
								'PGPS_POSI' => 1,															
								
								'SECC_ID' => $idSeccion,															
								
								'PGPS_LATI' => $ptoGrupLat1,															
								
								'PGPS_LONG' => $ptoGrupLong1,	

								'VER_NUM' => $verNum														
							);														
							$this->seccion_model->saveSeccionGPS($datos_ptogps1);	

							//Punto 2
							$datos_ptogps2 = array(															
						
								'PGPS_POSI' => 2,															
								
								'SECC_ID' => $idSeccion,															
								
								'PGPS_LATI' => $ptoGrupLat2,															
								
								'PGPS_LONG' => $ptoGrupLong2,		

								'VER_NUM' => $verNum													
							);														
							$this->seccion_model->saveSeccionGPS($datos_ptogps2);	

							//Punto 3
							$datos_ptogps3 = array(															
						
								'PGPS_POSI' => 3,															
								
								'SECC_ID' => $idSeccion,															
								
								'PGPS_LATI' => $ptoGrupLat3,															
								
								'PGPS_LONG' => $ptoGrupLong3,	

								'VER_NUM' => $verNum														
							);														
							$this->seccion_model->saveSeccionGPS($datos_ptogps3);	

							//Punto 4
							$datos_ptogps3 = array(															
						
								'PGPS_POSI' => 4,															
								
								'SECC_ID' => $idSeccion,															
								
								'PGPS_LATI' => $ptoGrupLat4,															
								
								'PGPS_LONG' => $ptoGrupLong4,	

								'VER_NUM' => $verNum														
							);														
							$this->seccion_model->saveSeccionGPS($datos_ptogps3);	


							$segmentos=$this->seccion_model->nombSegmento($idSegmento);														
							
							$idSegmento=$segmentos->row('SEG_NOMBRE');							
							
							if($tipoInstalac=="1")																
						
								$tipoInstalac="Externa";														
						
							else{																	
								$tipoInstalac="Interna";														
						
							}																					
							
							if($seccAula==null){
								$seccAula="Ninguna";
							}
							else{
								$aulas=$this->seccion_model->nombAula($seccAula);														
							
								$seccAula=$aulas->row('A_nombre');	
							}

							$this->nueva_seccion();	
						
							$msg['title'] = "Registro Finalizado";														
						
							$msg['mesage'] = 	"Los datos de la secci&oacute;n han sido almacenados con &eacute;xito																			

							<p> Id: ".$idSeccion."</p>																				
							
							<p> Segmento: ".$idSegmento."</p>																			
							
							<p> Secci&oacute;n: ".$nombSeccion."</p>																				
							
							<p> Descripci&oacute;n: ".$descripSeccion."</p>																			
							
							<p> Tipo de instalaci&oacute;n: ".$tipoInstalac."</p> 																			
							
							<p> Aula: ".$seccAula."</p> 																			

							<p> Br&uacute;jula: ".$brujula."</p> 																															
							
							<p> Mapa: ".$mapa."</p>	";																			
							
							$msg['type'] = "information_message";														
							
							$this->load->view('plantilla/message',$msg);												
						}												
						else{														
							
							$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);
							
							$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);														
							
							$datos['workspace'] = $this->load->view('gsecciones/nueva_seccion',"",true);	
							
							$this->load->view('plantilla/plantilla',$datos);														
							
							$msg['title'] = "Existen campos erroneos";														
							
							$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 		
							
												<br>Por favor revise y verifique que todo este correcto</b><br><br>																			
							
												&diams; El id de la secci&oacute;n debe estar en el rango establecido para el segmento seleccionado";																			
							
												$msg['type'] = "error_message";							

							//$msg['view'] = "gusuarios/nuevo_usuario";							
							$this->load->view('plantilla/message',$msg);												
						}															
					}
					
				}
					
			}
		}
		else{
			$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);
			$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gsecciones/nueva_seccion',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Secci&oacute;n ya existente";					
			$msg['type'] = "error_message";			
			//$msg['view'] = "gusuarios/nuevo_usuario";						
			$this->load->view('plantilla/message',$msg);
		}	
	}
	
	function mostrar_datos() 
    {
		extract($_POST);
		if(array_key_exists('Editar', $_POST))
			{
			$this->limpia();
			$edicion_s=$this->seccion_model->obtener($idseccion);
			
			$this->session->set_userdata('idSeccion',$edicion_s->row('SECC_ID'));
			$this->session->set_userdata('idSegmento',$edicion_s->row('SEG_ID'));
			$this->session->set_userdata('nombSeccion',$edicion_s->row('SECC_NOMBRE'));
			$this->session->set_userdata('descripSeccion',$edicion_s->row('SECC_DESC'));
			$this->session->set_userdata('tipoInstalac',$edicion_s->row('SECCTIPO_INST'));
			$this->session->set_userdata('seccAula',$edicion_s->row('idAula'));
			$this->session->set_userdata('brujula',$edicion_s->row('SECC_BRUJULA'));									
			$this->session->set_userdata('mapa',$edicion_s->row('SECC_MAP'));

			
			$edicion_ptos_gps=$this->seccion_model->obtenerGPS($idseccion,1);
			if ($edicion_ptos_gps!=null) {
				$this->session->set_userdata('ptoGrupLong1',$edicion_ptos_gps->row('PGPS_LONG'));
				$this->session->set_userdata('ptoGrupLat1',$edicion_ptos_gps->row('PGPS_LATI'));
			}
			

			$edicion_ptos_gps=$this->seccion_model->obtenerGPS($idseccion,2);
			if ($edicion_ptos_gps!=null) {
				$this->session->set_userdata('ptoGrupLat2',$edicion_ptos_gps->row('PGPS_LATI'));
				$this->session->set_userdata('ptoGrupLong2',$edicion_ptos_gps->row('PGPS_LONG'));
			}
			

			$edicion_ptos_gps=$this->seccion_model->obtenerGPS($idseccion,3);
			if ($edicion_ptos_gps!=null) {
				$this->session->set_userdata('ptoGrupLong3',$edicion_ptos_gps->row('PGPS_LONG'));
				$this->session->set_userdata('ptoGrupLat3',$edicion_ptos_gps->row('PGPS_LATI'));
			}
			

			$edicion_ptos_gps=$this->seccion_model->obtenerGPS($idseccion,4);
			if ($edicion_ptos_gps!=null) {
				$this->session->set_userdata('ptoGrupLat4',$edicion_ptos_gps->row('PGPS_LATI'));
			$this->session->set_userdata('ptoGrupLong4',$edicion_ptos_gps->row('PGPS_LONG'));
			}
			
			$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gsecciones/editar_seccion',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
		}   

		if(array_key_exists('Eliminar', $_POST)){
			$edicion_s=$this->seccion_model->obtener($idseccion);
			$eliminar=$this->seccion_model->deleteSeccion($idseccion);
			$msg['title'] = "Eliminaci&oacute;n";
			if($eliminar){				
				
				$msg['mesage'] = 	"<b>Resultado</b>
											<p>La secci&oacute;n  \"".$edicion_s->row('SECC_NOMBRE')."\" ha sido eliminada con &eacute;xito.</p>
											";					
				
			}
			else{
				$msg['mesage'] = 	"<b>Resultado</b>
											<p><b>La secci&oacute;n \"".$edicion_s->row('SECC_NOMBRE')."\" no se puede eliminar.</b></p>
											<p>Tiene dependencias.</p>";					
			}
			$this->consultar_seccion();
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		} 
		
	}
	
	function actualizar_datos()     
   {         	
		extract($_POST);
		if($idSegmento=="0"){
			$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);
			$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gsecciones/nueva_seccion',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos vacios";
			$msg['mesage'] = 	"<b>Campo requerido. 
										<br></b><br><br> 
										&diams; Debe seleccionar el segmento al que pertenece.";					
			$msg['type'] = "error_message";
			
			$this->load->view('plantilla/message',$msg);
		}
		else{
			if($tipoInstalac=='0'){
				$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);
				$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gsecciones/nueva_seccion',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Campo requerido. 
											<br></b><br><br> 
											&diams; Debe seleccionar que tipo de instalación es.";					
				$msg['type'] = "error_message";
				//$msg['view'] = "gusuarios/nuevo_usuario";
				$this->load->view('plantilla/message',$msg);
			}
			else{												
				if($brujula=='0'){	

					$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);

					
					$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);											
					
					$datos['workspace'] = $this->load->view('gsecciones/nueva_seccion',"",true);												
					
					$this->load->view('plantilla/plantilla',$datos);											
					
					$msg['title'] = "Existen campos vacios";											
					
					$msg['mesage'] = 	"<b>Campo requerido. 																		
					
										<br></b><br><br> 																		
										
										&diams; Debe seleccionar un valor para br&oacute;jula.";																
					$msg['type'] = "error_message";		

					$this->load->view('plantilla/message',$msg);

				}									
				else{																
					
					$rangos=$this->seccion_model->validarRango($idSegmento);											
					$min=$rangos->row('SEG_MIN');						//echo($min);						
					$max=$rangos->row('SEG_MAX');											
					
					if($idSeccion>=$min AND $idSeccion<=$max){													
					
					
						$verNum=$this->seccion_model->getVerNum();	

						if ($seccAula=="0") {
							$seccAula=null;
						}																			
						
						$datos = array(													
						
						'SEG_ID' => $idSegmento,													
						
						'SECC_NOMBRE' => $nombSeccion,													
						
						'SECC_DESC' => $descripSeccion,													
						
						'SECCTIPO_INST' => $tipoInstalac,													
						
						'SECC_BRUJULA' => $brujula,													
						
						'INSTI_ID' => 1,																				
						
						'VER_NUM' => $verNum,																			
						
						'SECC_MAP' => $mapa,

						'idAula' => $seccAula							
						
						);													

						$this->seccion_model->updateSeccion($idSeccion,$datos);	

						//Datos para puntos GPS
						//Punto 1
						$datos_ptogps1 = array(															
					
							'PGPS_POSI' => 1,															
							
							'SECC_ID' => $idSeccion,															
							
							'PGPS_LATI' => $ptoGrupLat1,															
							
							'PGPS_LONG' => $ptoGrupLong1,	

							'VER_NUM' => $verNum														
						);														
						$this->seccion_model->updateSeccionGPS($idSeccion,1,$datos_ptogps1);	

						//Punto 2
						$datos_ptogps2 = array(															
					
							'PGPS_POSI' => 2,															
							
							'SECC_ID' => $idSeccion,															
							
							'PGPS_LATI' => $ptoGrupLat2,															
							
							'PGPS_LONG' => $ptoGrupLong2,		

							'VER_NUM' => $verNum													
						);														
						$this->seccion_model->updateSeccionGPS($idSeccion,2,$datos_ptogps2);	

						//Punto 3
						$datos_ptogps3 = array(															
					
							'PGPS_POSI' => 3,															
							
							'SECC_ID' => $idSeccion,															
							
							'PGPS_LATI' => $ptoGrupLat3,															
							
							'PGPS_LONG' => $ptoGrupLong3,	

							'VER_NUM' => $verNum														
						);														
						$this->seccion_model->updateSeccionGPS($idSeccion,3,$datos_ptogps3);	

						//Punto 4
						$datos_ptogps4 = array(															
					
							'PGPS_POSI' => 4,															
							
							'SECC_ID' => $idSeccion,															
							
							'PGPS_LATI' => $ptoGrupLat4,															
							
							'PGPS_LONG' => $ptoGrupLong4,	

							'VER_NUM' => $verNum														
						);														
						$this->seccion_model->updateSeccionGPS($idSeccion,4,$datos_ptogps4);



						$segmentos=$this->seccion_model->nombSegmento($idSegmento);														

						$idSegmento=$segmentos->row('SEG_NOMBRE');																									

						if($tipoInstalac=="1")															
					
							$tipoInstalac="Externa";													
					
						else{							

							$tipoInstalac="Interna";													
						}													

						if($seccAula==null){
								$seccAula="Ninguna";
						}
						else{
							$aulas=$this->seccion_model->nombAula($seccAula);														
						
							$seccAula=$aulas->row('A_nombre');	
						}

						
						$this->consultar_seccion();													

						
						$msg['title'] = "Registro Finalizado";													

						$msg['mesage'] = 	"Los datos de la secci&oacute;n han sido almacenados con &eacute;xito																			

											<p> Id: ".$idSeccion."</p>																				

											<p> Segmento: ".$idSegmento."</p>																			

											<p> Secci&oacute;n: ".$nombSeccion."</p>																				

											<p> Descripci&oacute;n: ".$descripSeccion."</p>																			

											<p> Tipo de instalaci&oacute;n: ".$tipoInstalac."</p> 																			

											<p> Aula: ".$seccAula."</p> 																			
											
											<p> Br&uacute;jula: ".$brujula."</p>																																

											<p> Mapa: ".$mapa."</p> ";																		
											
						$msg['type'] = "information_message";													
						
						$this->load->view('plantilla/message',$msg);												
					}	

					else{														
					
						$this->getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4);
						
						$datos['sidebar'] = $this->load->view('gsecciones/sidebar',"",true);														
						
						$datos['workspace'] = $this->load->view('gsecciones/editar_seccion',"",true);															
						
						$this->load->view('plantilla/plantilla',$datos);														

						$msg['title'] = "Existen campos erroneos";														

						$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 	
											
											<br>Por favor revise y verifique que todo este correcto</b><br><br>		

											&diams; El id de la secci&oacute;n debe estar en el rango establecido para el segmento seleccionado";																			

						$msg['type'] = "error_message";														

						$this->load->view('plantilla/message',$msg);											

					}														
				}
			}
		}
	}	
	
	function limpia(){
		$this->session->set_userdata('idSeccion','');
		$this->session->set_userdata('idSegmento','Seleccione...');
		$this->session->set_userdata('nombSeccion','');
		$this->session->set_userdata('descripSeccion','');
		$this->session->set_userdata('tipoInstalac','Seleccione...');
		$this->session->set_userdata('seccAula','Ninguna');
		$this->session->set_userdata('brujula','');				
		$this->session->set_userdata('mapa','');
		$this->session->set_userdata('ptoGrupLong1','');
		$this->session->set_userdata('ptoGrupLat1','');
		$this->session->set_userdata('ptoGrupLat2','');
		$this->session->set_userdata('ptoGrupLong2','');
		$this->session->set_userdata('ptoGrupLong3','');
		$this->session->set_userdata('ptoGrupLat3','');
		$this->session->set_userdata('ptoGrupLat4','');
		$this->session->set_userdata('ptoGrupLong4','');
	}
	
	function getValores($idSeccion,$idSegmento,$nombSeccion,$descripSeccion,$tipoInstalac,$seccAula,$brujula,$mapa,$ptoGrupLat1,$ptoGrupLong1,$ptoGrupLat2,$ptoGrupLong2,$ptoGrupLat3,$ptoGrupLong3,$ptoGrupLat4,$ptoGrupLong4){

		$this->session->set_userdata('idSeccion',$idSeccion);
		$this->session->set_userdata('idSegmento',$idSegmento);
		$this->session->set_userdata('nombSeccion',$nombSeccion);
		$this->session->set_userdata('descripSeccion',$descripSeccion);
		$this->session->set_userdata('tipoInstalac',$tipoInstalac);
		$this->session->set_userdata('seccAula',$seccAula);
		$this->session->set_userdata('brujula',$brujula);				
		$this->session->set_userdata('mapa',$mapa);

		$this->session->set_userdata('ptoGrupLong1',$ptoGrupLong1);
		$this->session->set_userdata('ptoGrupLat1',$ptoGrupLat1);
		$this->session->set_userdata('ptoGrupLat2',$ptoGrupLat2);
		$this->session->set_userdata('ptoGrupLong2',$ptoGrupLong2);
		$this->session->set_userdata('ptoGrupLong3',$ptoGrupLong3);
		$this->session->set_userdata('ptoGrupLat3',$ptoGrupLat3);
		$this->session->set_userdata('ptoGrupLat4',$ptoGrupLat4);
		$this->session->set_userdata('ptoGrupLong4',$ptoGrupLong4);
	}
	
}

 ?>