<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_info extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('info_model');
    }
	
	function gInfo(){
		if($this->session->userdata('eslogeado')) {
			$datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
			$datos['workspace'] = "</br>";
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
	}
	
	function nueva_info(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('ginformaciones/nueva_info',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
 	
	function consultar_info(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('ginformaciones/informaciones',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
	
	function validar_registro(){
		extract($_POST);
		if(array_key_exists('Cancelar', $_POST)){
			$this->gInfo();
		}
		extract($_POST);
		$config = array( 						 // array(
						 // 'field'   => 'idInfo', 
						 // 'label'   => 'Id', 
						 // 'rules'   => 'required'
					  // ),					  					  
					  	array(
						 'field'   => 'infonum', 
						 'label'   => 'N&uacute;mero de informaci&oacute;n', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'idSecc', 
						 'label'   => 'Secci&oacute;n', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'contenido', 
						 'label'   => 'Contenido', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'tema', 
						 'label'   => 'Tema', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'infotipo', 
						 'label'   => 'Tipo de informaci&oacute;n', 
						 'rules'   => 'required'
					  ) );

			$this->form_validation->set_rules($config);
			$this->form_validation->set_message('required', '%s, se requiere');
			$this->form_validation->set_error_delimiters('&diams; &nbsp;', '<br/>');
		if ($this->form_validation->run()==FALSE ){
			if(array_key_exists('Guardar_Info', $_POST)){
				$this->getValores($idInfo,$infonum,$idSecc,$contenido,$tema,$infotipo);
				$datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
				$datos['workspace'] = $this->load->view('ginformaciones/nueva_info','',true);	
				$this->load->view('plantilla/plantilla',$datos);
				
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
									<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
				$msg['type'] = "error_message";
				//$msg['view'] = "gsecciones/nueva_seccion";
				$this->load->view('plantilla/message',$msg);
				
			}
			else if(array_key_exists('Guardar_Cambios', $_POST)){
					$this->getValores($idInfo,$infonum,$idSecc,$contenido,$tema,$infotipo);
					$datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
					$datos['workspace'] = $this->load->view('ginformaciones/editar_info',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
										<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuario/editar_usuario";
					$this->load->view('plantilla/message',$msg);
				}
		}	
		else{
				if(array_key_exists('Guardar_Info', $_POST))
					$this->ingresar();
				else if(array_key_exists('Guardar_Cambios', $_POST)){
							$this->actualizar_datos();
						}				
			}
	}
	
	function ingresar(){
		extract($_POST);
		$existe=$this->info_model->validarInfo($infonum,$idSecc,$infotipo);
		if(!$existe){
			if($infonum=="0"){
				$this->getValores($idInfo,$infonum,$idSecc,$contenido,$tema,$infotipo);
				$datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
				$datos['workspace'] = $this->load->view('ginformaciones/nueva_info',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Campo requerido. 
											<br></b><br><br> 
											&diams; Debe seleccionar  el n&umero dentro de la secci&oacute;n.";					
				$msg['type'] = "error_message";
				//$msg['view'] = "gusuarios/nuevo_usuario";
				$this->load->view('plantilla/message',$msg);
			}
			else{
				// if($idSecc=='0'){
					// $this->getValores($idInfo,$infonum,$idSecc,$contenido,$tema,$infotipo);
					// $datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
					// $datos['workspace'] = $this->load->view('ginformaciones/nueva_info',"",true);	
					// $this->load->view('plantilla/plantilla',$datos);
					// $msg['title'] = "Existen campos vacios";
					// $msg['mesage'] = 	"<b>Campo requerido. 
												// <br></b><br><br> 
												// &diams; Debe seleccionar la secci&oacute;n .";					
					// $msg['type'] = "error_message";
					
					// $this->load->view('plantilla/message',$msg);
				// }
				// else{
					// if($infotipo=="0"){
						// $this->getValores($idInfo,$infonum,$idSecc,$contenido,$tema,$infotipo);
						// $datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
						// $datos['workspace'] = $this->load->view('ginformaciones/nueva_info',"",true);	
						// $this->load->view('plantilla/plantilla',$datos);
						// $msg['title'] = "Existen campos vacios";
						// $msg['mesage'] = 	"<b>Campo requerido. 
													// <br></b><br><br> 
													// &diams; Debe seleccionar el tipo de informaci&oacute;n .";					
						// $msg['type'] = "error_message";
						
						// $this->load->view('plantilla/message',$msg);
					// }
					// else{						
					
					
						$verNum=$this->info_model->getVerNum();
						$datos = array(
						//'INFO_ID' => $idInfo,
						'INFO_NUM' => $infonum,
						'SECC_ID' => $idSecc,
						'INFO_CONTENIDO' => $contenido,
						'INFO_TEMA' => $tema,
						'INFO_TIPO' => $infotipo,												

						'VER_NUM' => $verNum  //ESTO SE DEBE DE CAMBIAR
						);
						$this->info_model->saveInfo($datos);
						$seccion=$this->info_model->nombSeccion($idSecc);
						$idSecc=$seccion->row('SECC_NOMBRE');
						if($infotipo==0)
							$infotipo="Mapa";
						else{
							if($infotipo==1)
							$infotipo="Rotulo";
							else{
								if($infotipo==2)
									$infotipo="Procesos administrativos";
								else{
									if($infotipo==3)
										$infotipo="Procesos academicos";
									else{
										if($infotipo==4)
											$infotipo="Informaci&oacute;n de instalaci&oacute;n";
										else
											$infotipo="Autoridades";
									}
								}							
							}
						}
												
						$this->nueva_info();
						$msg['title'] = "Registro Finalizado";
						$msg['mesage'] = 	"Los datos de la informaci&oacute;n han sido almacenados con &eacute;xito
											
											<p> N&uacute;mero: ".$infonum."</p>
											<p> Secci&oacute;n: ".$idSecc."</p>	
											<p> Informaci&oacute;n: ".$contenido."</p>
											<p> Tema: ".$tema."</p>
											<p> Tipo de informaci&oacute;n: ".$infotipo."</p> ";					
						$msg['type'] = "information_message";
						$this->load->view('plantilla/message',$msg);
					// }
				// }
			}
		}
		else{
			$this->getValores($idInfo,$infonum,$idSecc,$contenido,$tema,$infotipo);
			$datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('ginformaciones/nueva_info',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Informaci&oacute;n ya existente";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}	
	}
	
	function mostrar_datos() 
    {
		extract($_POST);
		if(array_key_exists('Editar', $_POST))
			{
			$this->limpia();
			$edicion_s=$this->info_model->obtener($idinfo);
			$this->session->set_userdata('idInfo',$edicion_s->row('INFO_ID'));
			$this->session->set_userdata('infonum',$edicion_s->row('INFO_NUM'));
			//$des[$edicion_s->row('SECC_D_ID')]=$edicion_s->row('DESTINO');
			$this->session->set_userdata('idSecc',$edicion_s->row('SECC_ID'));
			
			$this->session->set_userdata('contenido',$edicion_s->row('INFO_CONTENIDO'));
			//echo $this->session->userdata('contenido');
			$this->session->set_userdata('tema',$edicion_s->row('INFO_TEMA'));
			$this->session->set_userdata('infotipo',$edicion_s->row('INFO_TIPO'));
			$datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
			$datos['workspace'] = $this->load->view('ginformaciones/editar_info',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
		}     
		if(array_key_exists('Eliminar', $_POST)){
			$edicion_s=$this->info_model->obtener($idinfo);
			$eliminar=$this->info_model->deleteInfo($idinfo);
			$msg['title'] = "Eliminaci&oacute;n";
			if($eliminar){				
				
				$msg['mesage'] = 	"<b>Resultado</b>
											<p>La informaci&oacute;n  \"".$edicion_s->row('INFO_ID')."\" ha sido eliminada con &eacute;xito.</p>
											";					
				
			}
			
			// else{
				// $msg['mesage'] = 	"<b>Resultado</b>
											// <p><b>El segmento \"".$edicion_s->row('SEG_NOMBRE')."\" no se puede eliminar.</b></p>
											// <p>Tiene dependencias.</p>";					
			// }
			
			
			$this->consultar_info();
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
	}
	
	function actualizar_datos()     
   {         	
		extract($_POST);
		if($infonum=="0"){
				$this->getValores($idInfo,$infonum,$idSecc,$contenido,$tema,$infotipo);
				$datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
				$datos['workspace'] = $this->load->view('ginformaciones/editar_info',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Campo requerido. 
											<br></b><br><br> 
											&diams; Debe seleccionar  el n&umero dentro de la secci&oacute;n.";					
				$msg['type'] = "error_message";
				//$msg['view'] = "gusuarios/nuevo_usuario";
				$this->load->view('plantilla/message',$msg);
			}
			else{
				// if($idSecc=='0'){
					// $this->getValores($idInfo,$infonum,$idSecc,$contenido,$tema,$infotipo);
					// $datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
					// $datos['workspace'] = $this->load->view('ginformaciones/editar_info',"",true);	
					// $this->load->view('plantilla/plantilla',$datos);
					// $msg['title'] = "Existen campos vacios";
					// $msg['mesage'] = 	"<b>Campo requerido. 
												// <br></b><br><br> 
												// &diams; Debe seleccionar la secci&oacute;n .";					
					// $msg['type'] = "error_message";
					// $this->load->view('plantilla/message',$msg);
				// }
				// else{
					// if($infotipo=="0"){
						// $this->getValores($idInfo,$infonum,$idSecc,$contenido,$tema,$infotipo);
						// $datos['sidebar'] = $this->load->view('ginformaciones/sidebar',"",true);
						// $datos['workspace'] = $this->load->view('ginformaciones/editar_info',"",true);	
						// $this->load->view('plantilla/plantilla',$datos);
						// $msg['title'] = "Existen campos vacios";
						// $msg['mesage'] = 	"<b>Campo requerido. 
													// <br></b><br><br> 
													// &diams; Debe seleccionar el tipo de informaci&oacute;n .";					
						// $msg['type'] = "error_message";
						
						// $this->load->view('plantilla/message',$msg);
					// }
					// else{						
						$verNum=$this->info_model->getVerNum();
						$datos = array(
						'INFO_NUM' => $infonum,
						'SECC_ID' => $idSecc,
						'INFO_CONTENIDO' => $contenido,
						'INFO_TEMA' => $tema,
						'INFO_TIPO' => $infotipo,											

						'VER_NUM' => $verNum
						);
						$this->info_model->updateInfo($idInfo,$datos);
						$seccion=$this->info_model->nombSeccion($idSecc);
						$idSecc=$seccion->row('SECC_NOMBRE');
						if($infotipo==0)
							$infotipo="Mapa";
						else{
							if($infotipo==1)
							$infotipo="Rotulo";
							else{
								if($infotipo==2)
									$infotipo="Procesos administrativos";
								else{
									if($infotipo==3)
										$infotipo="Procesos academicos";
									else{
										if($infotipo==4)
											$infotipo="Informaci&oacute;n de instalaci&oacute;n";
										else
											$infotipo="Autoridades";
									}
								}							
							}
						}					
						$this->consultar_info();
						$msg['title'] = "Registro Finalizado";
						$msg['mesage'] = 	"Los datos de la informaci&oacute;n han sido almacenados con &eacute;xito
											<p> Id: ".$idInfo."</p>	
											<p> N&uacute;mero: ".$infonum."</p>
											<p> Secci&oacute;n: ".$idSecc."</p>	
											<p> Informaci&oacute;n: ".$contenido."</p>
											<p> Tema: ".$tema."</p>
											<p> Tipo de informaci&oacute;n: ".$infotipo."</p> ";					
						$msg['type'] = "information_message";
						$this->load->view('plantilla/message',$msg);
					// }
				// }
			}
	}	
	
	function limpia(){
		$this->session->set_userdata('idInfo','');
		$this->session->set_userdata('infonum','');
		$this->session->set_userdata('idSecc','Seleccione...');
		$this->session->set_userdata('contenido','');
		$this->session->set_userdata('tema','');
		$this->session->set_userdata('infotipo','');
	}
	
	function getValores($idInfo,$infonum,$idSecc,$contenido,$tema,$infotipo){
		$this->session->set_userdata('idInfo',$idInfo);
		$this->session->set_userdata('infonum',$infonum);
		$this->session->set_userdata('idSecc',$idSecc);
		$this->session->set_userdata('contenido',$contenido);
		$this->session->set_userdata('tema',$tema);
		$this->session->set_userdata('infotipo',$infotipo);
	}
	
}

 ?>