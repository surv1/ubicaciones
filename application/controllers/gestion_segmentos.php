<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_segmentos extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('segmento_model');
    }
 
    function index() 
    {
	if($this->session->userdata('eslogeado')){
            $this->load->view('pagina_inicio');
        }
        else{
            redirect('inicio');
        }	
        // $data['titulo'] = 'Update con codeIgniter';
        // $data['mensajes'] = $this->datos_model->mensajes();
        // $this->load->view('datos_view', $data);
    }
 
	function gSegmentos(){
		if($this->session->userdata('eslogeado')) {
			$datos['sidebar'] = $this->load->view('gsegmentos/sidebar',"",true);
			$datos['workspace'] = "</br>";
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
    }
	
	function nuevo_segmento(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gsegmentos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gsegmentos/nuevo_segmento',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
	
	function editar_segmento(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gsegmentos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gsegmentos/segmentos',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
	
	function validar_registro(){
		extract($_POST);
		if(array_key_exists('Cancelar', $_POST)){
			$this->gSegmentos();
		}
		extract($_POST);
		$config = array( 
						// array(
						 // 'field'   => 'idseg', 
						 // 'label'   => 'Id', 
						 // 'rules'   => 'required'
					  // ),
					  array(
						 'field'   => 'segmento', 
						 'label'   => 'Segmento', 
						 'rules'   => 'required'
					  ),
					  array(
						 'field'   => 'minSeg', 
						 'label'   => 'M&iacute;nimo', 
						 'rules'   => 'required'
					  ),
					  array(
						 'field'   => 'maxSeg', 
						 'label'   => 'M&aacute;ximo', 
						 'rules'   => 'required'
					  )
					  // array(
						 // 'field'   => 'idBase', 
						 // 'label'   => 'Segmento', 
						 // 'rules'   => 'required'
					  // ) 
					  );

			$this->form_validation->set_rules($config);
			$this->form_validation->set_message('required', '%s, se requiere');
			$this->form_validation->set_error_delimiters('&diams; &nbsp;', '<br/>');
		if ($this->form_validation->run()==FALSE ){
			if(array_key_exists('Guardar_Segmento', $_POST)){
				$this->getValores($idseg,$segmento,$descripSegmento,$minSeg,$maxSeg,1);
				$datos['sidebar'] = $this->load->view('gsegmentos/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gsegmentos/nuevo_segmento',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
									<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
				$msg['type'] = "error_message";
				$this->load->view('plantilla/message',$msg);
			}
			else if(array_key_exists('Guardar_Cambios', $_POST)){
					$this->getValores($idseg,$segmento,$descripSegmento,$minSeg,$maxSeg,1);
					$datos['sidebar'] = $this->load->view('gsegmentos/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gsegmentos/editar_segmento',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
										<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuario/editar_usuario";
					$this->load->view('plantilla/message',$msg);
				}
		}	
		else{
				if(array_key_exists('Guardar_Segmento', $_POST))
					$this->ingresar();
				else if(array_key_exists('Guardar_Cambios', $_POST)){
							$this->actualizar_datos();
						}				
			}
	}
	
	function ingresar(){
		extract($_POST);
		$existe=$this->segmento_model->validarSegmento($segmento);
		if(!$existe){
			// if($idBase==0){
				// $this->getValores($idseg,$segmento,$descripSegmento,$minSeg,$maxSeg,$idBase);
				// $datos['sidebar'] = $this->load->view('gsegmentos/sidebar',"",true);
				// $datos['workspace'] = $this->load->view('gsegmentos/nuevo_segmento',"",true);	
				// $this->load->view('plantilla/plantilla',$datos);
				// $msg['title'] = "Existen campos vacios";
				// $msg['mesage'] = 	"<b>Campo requerido. 
											// <br></b><br><br> 
											// &diams; Debe seleccionar la base a la que pertenece.";					
				// $msg['type'] = "error_message";
				$msg['view'] = "gusuarios/nuevo_usuario";
				// $this->load->view('plantilla/message',$msg);
			// }
			// else{
				$verNum=$this->segmento_model->getVerNum();								
				$datos = array(
				
				//'SEG_ID'=>$idseg,
				
				'SEG_NOMBRE' => $segmento,
				'SEG_DESC' => $descripSegmento,
				'SEG_MIN' => $minSeg,
				'SEG_MAX' => $maxSeg,
				'INSTI_ID' => 1,								
				'VER_NUM' => $verNum
				);
				$this->segmento_model->saveSegmento($datos);
				// $nombBD=$this->segmento_model->nombBase($idBase);
				// $idBase=$nombBD->row('BASE_EMPRESA');
				$this->nuevo_segmento();
				$msg['title'] = "Registro Finalizado";
				$msg['mesage'] = 	"Los datos del segmento han sido almacenados con &eacute;xito
									
									<p> Segmento: ".$segmento."</p>	
									<p> Descripci&oacute;n: ".$descripSegmento."</p>
									<p> M&iacute;nimo: ".$minSeg."</p>
									<p> M&aacute;ximo: ".$maxSeg."</p>";					
				$msg['type'] = "information_message";
				$this->load->view('plantilla/message',$msg);
			// }
			
		}
		else{
			$this->getValores($idseg,$segmento,$descripSegmento,$minSeg,$maxSeg,1);
			$datos['sidebar'] = $this->load->view('gsegmentos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gsegmentos/nuevo_segmento',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Segmento ya existente";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}	
	}
    //funci�n encargada de mostrar los formularios por ajax
    //dependiendo el bot�n que hayamos pulsado
    function mostrar_datos() 
    {
		extract($_POST);
		if(array_key_exists('Editar', $_POST))
			{
			$this->limpia();
			//echo $idsegmento;
			$edicion_s=$this->segmento_model->obtener($idsegmento);
			$this->session->set_userdata('idseg',$edicion_s->row('SEG_ID'));
			$this->session->set_userdata('segmento',$edicion_s->row('SEG_NOMBRE'));
			$this->session->set_userdata('descripSegmento',$edicion_s->row('SEG_DESC'));
			$this->session->set_userdata('minSeg',$edicion_s->row('SEG_MIN'));
			$this->session->set_userdata('maxSeg',$edicion_s->row('SEG_MAX'));
			$this->session->set_userdata('idBase',$edicion_s->row('INSTI_ID'));
			$datos['sidebar'] = $this->load->view('gsegmentos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gsegmentos/editar_segmento',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			 // ?>
             <?//= form_open(base_url() . 'index.php/gestion_segmentos/actualizar_datos','') ?>
			 <?php //
			// $datos['sidebar'] = $this->load->view('gsegmentos/sidebar',"",true);
			// $datos['workspace'] = "<center>
			// <div id='title-page'>
				// Editar Segmento
			// </div>
				// <p></p>
				// <input type='hidden' style='height:25px' name='id' value=".$idsegmento."><center></center>
				// <table>
					// <tr>
						// <td align='right'><label>Segmento: *</label></td>
						// <td ><input type='text' readonly='readonly' style='height:25px;width:250px' name='segmento' value='".$edicion_s->row('SEG_ID')."'><center></center>
						// </td>
					// </tr>

					// <tr>
						// <td align='right'><label>Segmento: *</label></td>
						// <td ><input type='text' readonly='readonly' style='height:25px;width:250px' name='segmento' value='".$edicion_s->row('SEG_NOMBRE')."'><center></center>
						// </td>
					// </tr>
					// <tr>
						// <td align='right'><label>Descripci&oacute;n: * </label></td>
						// <td><input type='text' style='height:25px;width:250px' name='descripSegmento' value='".$edicion_s->row('SEG_DESC')."'><center></center>
						// </td>
					// </tr>
				// </table></center>
				// <center><table>
					// <tr>
						// <td><input type='submit' name='Actualizar_Segmento' value='Actualizar' class='button-submit'/></td>
						// <td><input type='submit' name='Cancelar' value='Cancelar' class='button-submit'/></td>
					// </tr></table></center>";
			//$this->load->view('plantilla/plantilla',$datos);
		}   
		if(array_key_exists('Eliminar', $_POST)){
			$edicion_s=$this->segmento_model->obtener($idsegmento);
			$eliminar=$this->segmento_model->deleteSegmento($idsegmento);
			$msg['title'] = "Eliminaci&oacute;n";
			if($eliminar){				
				
				$msg['mesage'] = 	"<b>Resultado</b>
											<p>El segmento  \"".$edicion_s->row('SEG_NOMBRE')."\" ha sido eliminado con &eacute;xito.</p>
											";					
				
			}
			else{
				$msg['mesage'] = 	"<b>Resultado</b>
											<p><b>El segmento \"".$edicion_s->row('SEG_NOMBRE')."\" no se puede eliminar.</b></p>
											<p>Tiene dependencias.</p>";					
			}
			$this->editar_segmento();
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}  
	}
	
   //funci�n encargada de actualizar los datos     
   function actualizar_datos()     
   {         
		extract($_POST);
			// if($idBase==0){
				// $this->getValores($idseg,$segmento,$descripSegmento,$minSeg,$maxSeg,$idBase);
				// $datos['sidebar'] = $this->load->view('gsegmentos/sidebar',"",true);
				// $datos['workspace'] = $this->load->view('gsegmentos/nuevo_segmento',"",true);	
				// $this->load->view('plantilla/plantilla',$datos);
				// $msg['title'] = "Existen campos vacios";
				// $msg['mesage'] = 	"<b>Campo requerido. 
											// <br></b><br><br> 
											// &diams; Debe seleccionar la base a la que pertenece.";					
				// $msg['type'] = "error_message";
				$msg['view'] = "gusuarios/nuevo_usuario";
				// $this->load->view('plantilla/message',$msg);
			// }
			// else{
			
				extract($_POST);
				$verNum=$this->segmento_model->getVerNum();								
				$datos = array(
					'SEG_DESC' => $descripSegmento,
					'SEG_MIN' => $minSeg,
					'SEG_MAX' => $maxSeg,
					'INSTI_ID' => 1,								
					'VER_NUM' => $verNum
				);
				$this->segmento_model->updateSegmento($idseg,$datos);
				// $nombBD=$this->segmento_model->nombBase($idBase);
				// $idBase=$nombBD->row('BASE_EMPRESA');
				$this->editar_segmento();
				$msg['title'] = "Registro Finalizado";
				$msg['mesage'] = 	"Los datos del segmento han sido almacenados con &eacute;xito
									<p> Segmento: ".$idseg."</p>	
									<p> Segmento: ".$segmento."</p>	
									<p> Descripci&oacute;n: ".$descripSegmento."</p>
									<p> M&iacute;nimo: ".$minSeg."</p>
									<p> M&aacute;ximo: ".$maxSeg."</p>";					
				$msg['type'] = "information_message";
				$this->load->view('plantilla/message',$msg);
			// }

		
		
		
	}	
	
	function deleteSeg(){
		echo $id_seg;
	}
	function limpia(){
		$this->session->set_userdata('idseg','');
		$this->session->set_userdata('segmento','');
		$this->session->set_userdata('descripSegmento','');
		$this->session->set_userdata('minSeg','');
		$this->session->set_userdata('maxSeg','');
		$this->session->set_userdata('idBase','');
		
	}
		
	function getValores($idseg,$segmento,$descripSegmento,$minSeg,$maxSeg,$idBase){
		$this->session->set_userdata('idseg',$idseg);
		$this->session->set_userdata('segmento',$segmento);
		$this->session->set_userdata('descripSegmento',$descripSegmento);
		$this->session->set_userdata('minSeg',$minSeg);
		$this->session->set_userdata('maxSeg',$maxSeg);
		$this->session->set_userdata('idBase',$idBase);
	}
}

/*application/controllers/datos.php
 * el controlador datos.php
 */
 ?>