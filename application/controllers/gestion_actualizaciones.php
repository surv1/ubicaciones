<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_actualizaciones extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('actualizacion_model');
    }
	
	function gUpdateVer(){

		$exito=$this->actualizacion_model->updateVersion();
		$this->load->view('pagina_inicio');
		if($exito==true){
			$msg['title'] = "Actualizaci&oacute;n Finalizada";
			$msg['mesage'] = 	"Se ha cambiado a una nueva versi&oacute;n";					
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
		else{
			$msg['title'] = "Error en la actualizaci&oacute;n";
			$msg['mesage'] = 	"Fallo la actualizaci&oacute;n";					
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
		
				
		// $datos['sidebar'] = $this->load->view('gconexiones/sidebar',"",true);
		// $datos['workspace'] = "</br>";
        // $this->load->view('plantilla/plantilla',$datos);
			
        
	}
	
	
	function gETL_FIA(){
		extract($_POST);
		$conf= $this->input->post('conf');
		if($conf=='1'){
			
		}

		$exito=$this->actualizacion_model->callETL_DBFIA();
		if($exito==true){
			$this->load->view('pagina_inicio');
			$msg['title'] = "Actualizaci&oacute;n Finalizada";
			$msg['mesage'] = 	"Se ha actualizado la base de datos.";					
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
	}
}

 ?>