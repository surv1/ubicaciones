<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_reportes extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('reporte_model');
    }
 
    function index() 
    {
	if($this->session->userdata('eslogeado')){
            $this->load->view('pagina_inicio');
        }
        else{
            redirect('inicio');
        }	
        // $data['titulo'] = 'Update con codeIgniter';
        // $data['mensajes'] = $this->datos_model->mensajes();
        // $this->load->view('datos_view', $data);
    }
 
	function gReporteUso(){
		if($this->session->userdata('eslogeado')) {
			$datos['sidebar'] = "";
			$datos['workspace'] = $this->load->view('greportes/reporteUso',"",true);
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
    }

    function gReporteFrecUso(){
        if($this->session->userdata('eslogeado')) {
            $datos['sidebar'] = "";
            $datos['workspace'] = $this->load->view('greportes/reporteFrecUso',"",true);
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
    }
	
}

/*application/controllers/datos.php
 * el controlador datos.php
 */
 ?>