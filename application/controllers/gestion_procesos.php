<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_procesos extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('proceso_model');
    }
	
	function gProceso(){
		if($this->session->userdata('eslogeado')) {
			$datos['sidebar'] = $this->load->view('gprocesos/sidebar',"",true);
			$datos['workspace'] = "</br>";
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
	}
	
	function nuevo_proceso(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gprocesos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gprocesos/nuevo_proceso',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
 	
	function consultar_proceso(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gprocesos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gprocesos/procesos',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
	
	function validar_registro(){
		extract($_POST);
		if(array_key_exists('Cancelar', $_POST)){
			$this->gProceso();
		}
		extract($_POST);
		$config = array( array(
						 'field'   => 'idProceso', 
						 'label'   => 'Id', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'nombProceso', 
						 'label'   => 'Nombre del proceso', 
						 'rules'   => 'required'
					  ) );

			$this->form_validation->set_rules($config);
			$this->form_validation->set_message('required', '%s, se requiere');
			$this->form_validation->set_error_delimiters('&diams; &nbsp;', '<br/>');
		if ($this->form_validation->run()==FALSE ){
			if(array_key_exists('Guardar_Proceso', $_POST)){
				$this->getValores($idProceso,$nombProceso,$descripProceso,$antecesor);
				$datos['sidebar'] = $this->load->view('gprocesos/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gprocesos/nuevo_proceso','',true);	
				$this->load->view('plantilla/plantilla',$datos);
				
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
									<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
				$msg['type'] = "error_message";
				//$msg['view'] = "gsecciones/nueva_seccion";
				$this->load->view('plantilla/message',$msg);
				
			}
			else if(array_key_exists('Guardar_Cambios', $_POST)){
					$this->getValores($idProceso,$nombProceso,$descripProceso,$antecesor);
					$datos['sidebar'] = $this->load->view('gprocesos/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gprocesos/editar_proceso',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
										<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuario/editar_usuario";
					$this->load->view('plantilla/message',$msg);
				}
		}	
		else{
				if(array_key_exists('Guardar_Proceso', $_POST))
					$this->ingresar();
				else if(array_key_exists('Guardar_Cambios', $_POST)){
							$this->actualizar_datos();
						}				
			}
	}
	
	function ingresar(){
		extract($_POST);
		$existe=$this->proceso_model->validarProceso($idProceso,$nombProceso);
		if(!$existe){
			if($antecesor==-1)
				$antecesor = null;
			$verNum=$this->proceso_model->getVerNum();						$datos = array(
			'PROC_ID' => $idProceso,
			'PROC_NOMBRE' => $nombProceso,
			'PROC_DESC' => $descripProceso,
			'PROC_ANTECESOR' => $antecesor,						'VER_NUM' => $verNum
			);
			$this->proceso_model->saveProceso($datos);
			if($antecesor!=null){
				$proceso=$this->proceso_model->nombProc($antecesor);
				$antecesor=$proceso->row('PROC_NOMBRE');
			}
			else
				$antecesor=-1;
			if($antecesor!=-1)
				$stringAntecesor="<p> Antecesor: ".$antecesor."</p> ";					
			else	
			$stringAntecesor="<p> Antecesor: Ninguno</p> ";						
			$this->nuevo_proceso();
			$msg['title'] = "Registro Finalizado";
			$msg['mesage'] = 	"Los datos del proceso han sido almacenados con &eacute;xito
								<p> Id: ".$idProceso."</p>	
								<p> Proceso: ".$nombProceso."</p>
								<p> Descripci&oacute;n: ".$descripProceso."</p>".$stringAntecesor;					
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
		else{
			$this->getValores($idProceso,$nombProceso,$descripProceso,$antecesor);
			$datos['sidebar'] = $this->load->view('gprocesos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gprocesos/nuevo_proceso',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Informaci&oacute;n ya existente";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}	
	}
	
	function mostrar_datos() 
    {
		extract($_POST);
		if(array_key_exists('Editar', $_POST))
			{
			$this->limpia();
			$edicion_s=$this->proceso_model->obtener($idproceso);
			$this->session->set_userdata('idProceso',$edicion_s->row('PROC_ID'));
			$this->session->set_userdata('nombProceso',$edicion_s->row('PROC_NOMBRE'));
			//$des[$edicion_s->row('SECC_D_ID')]=$edicion_s->row('DESTINO');
			$this->session->set_userdata('descripProceso',$edicion_s->row('PROC_DESC'));
			$this->session->set_userdata('antecesor',$edicion_s->row('PROC_ANTECESOR'));
			$datos['sidebar'] = $this->load->view('gprocesos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gprocesos/editar_proceso',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
		}
			
		if(array_key_exists('Eliminar', $_POST)){
			$edicion_s=$this->proceso_model->obtener($idproceso);
			$eliminar=$this->proceso_model->deleteProceso($idproceso);
			$msg['title'] = "Eliminaci&oacute;n";
			if($eliminar){				
				
				$msg['mesage'] = 	"<b>Resultado</b>
											<p>El proceso  \"".$edicion_s->row('PROC_NOMBRE')."\" ha sido eliminado con &eacute;xito.</p>
											";					
				
			}
			else{
				$msg['mesage'] = 	"<b>Resultado</b>
											<p><b>El proceso \"".$edicion_s->row('PROC_NOMBRE')."\" no se puede eliminar.</b></p>
											<p>Tiene dependencias.</p>";					
			}
			$this->consultar_proceso();
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
	}
	
	function actualizar_datos()     
   {         	
		extract($_POST);
		if($antecesor<$idProceso or $idProceso==0)		{
			if($antecesor==-1)
				$antecesor = null;
			$verNum=$this->proceso_model->getVerNum();						$datos = array(
			'PROC_NOMBRE' => $nombProceso,
			'PROC_DESC' => $descripProceso,
			'PROC_ANTECESOR' => $antecesor,						'VER_NUM' => $verNum
			);
			$this->proceso_model->updateProceso($idProceso,$datos);
			if($antecesor!=null){
				$proceso=$this->proceso_model->nombProc($antecesor);
				$antecesor=$proceso->row('PROC_NOMBRE');
			}
			else
				$antecesor=-1;
			if($antecesor!=-1)
				$stringAntecesor="<p> Antecesor: ".$antecesor."</p> ";					
			else	
			$stringAntecesor="<p> Antecesor: Ninguno</p> ";
			$this->consultar_proceso();
			$msg['title'] = "Registro Finalizado";
			$msg['mesage'] = 	"Los datos del proceso han sido almacenados con &eacute;xito
								<p> Id: ".$idProceso."</p>	
								<p> Nombre: ".$nombProceso."</p>
								<p> Descripci&oacute;n: ".$descripProceso."</p>".$stringAntecesor;	
													
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
		else{
			$this->getValores($idProceso,$nombProceso,$descripProceso,$antecesor);
			$datos['sidebar'] = $this->load->view('gprocesos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gprocesos/editar_proceso',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Antecesor no puede ser mayor";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}
		
	}	
	
	function limpia(){
		$this->session->set_userdata('idProceso','');
		$this->session->set_userdata('nombProceso','');
		$this->session->set_userdata('descripProceso','');
		$this->session->set_userdata('antecesor','');
	}
	
	function getValores($idProceso,$nombProceso,$descripProceso,$antecesor){
		$this->session->set_userdata('idProceso',$idProceso);
		$this->session->set_userdata('nombProceso',$nombProceso);
		$this->session->set_userdata('descripProceso',$descripProceso);
		$this->session->set_userdata('antecesor',$antecesor);
	}
	
}

 ?>