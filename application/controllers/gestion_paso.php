<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_paso extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('paso_model');
    }
	
	function gPaso(){
		if($this->session->userdata('eslogeado')) {
			$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
			$datos['workspace'] = "</br>";
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
	}
	
	function nuevo_paso(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gpasos/nuevo_paso',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
 	
	function consultar_paso(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gpasos/pasos',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
		}
	}
	
	function validar_registro(){
		extract($_POST);
		if(array_key_exists('Cancelar', $_POST)){
			$this->gPaso();
		}
		extract($_POST);
		$config = array( array(
						 'field'   => 'idpaso', 
						 'label'   => 'Id', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'idproc', 
						 'label'   => 'Proceso', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'nombPaso', 
						 'label'   => 'Nombre del paso', 
						 'rules'   => 'required'
					  ) );

			$this->form_validation->set_rules($config);
			$this->form_validation->set_message('required', '%s, se requiere');
			$this->form_validation->set_error_delimiters('&diams; &nbsp;', '<br/>');
		if ($this->form_validation->run()==FALSE ){
			if(array_key_exists('Guardar_Paso', $_POST)){
				if(isset($userfile)==false)
					$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,'',$antecesor);
				else
					$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,$userfile,$antecesor);
				$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gpasos/nuevo_paso','',true);	
				$this->load->view('plantilla/plantilla',$datos);
				
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
									<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
				$msg['type'] = "error_message";
				
				$this->load->view('plantilla/message',$msg);
				
			}
			else if(array_key_exists('Guardar_Cambios', $_POST)){
					if(isset($userfile)==false)
						$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,'',$antecesor);
					else
						$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,$userfile,$antecesor);
					$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gpasos/editar_paso',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
										<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
					$msg['type'] = "error_message";
					
					$this->load->view('plantilla/message',$msg);
				}
		}	
		else{
				if(array_key_exists('Guardar_Paso', $_POST))
					$this->ingresar();
				else if(array_key_exists('Guardar_Cambios', $_POST)){
							$this->actualizar_datos();
						}				
			}
	}
	
	function ingresar(){
		extract($_POST);
		$existe=$this->paso_model->validarPaso($idpaso,$nombPaso);
		if(!$existe){
			if($idproc==-1){
				if(isset($userfile)==false)
				$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,'',$antecesor);
				else
					$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,$userfile,$antecesor);			
				$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gpasos/nuevo_paso',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos erroneos";
				$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
									<br>Por favor revise y verifique que todo este correcto</b><br><br>
									&diams; Debe seleccionar un proceso";					
				$msg['type'] = "error_message";
				//$msg['view'] = "gusuarios/nuevo_usuario";
				$this->load->view('plantilla/message',$msg);
			}
			else{
				if($antecesor==-1)
					$antecesor = null;
				if($idfacultad==0)
					$idfacultad = null;
				// if(isset($userfile)==false)
				// 	$userfile=null;
				// else{
				// 	echo($userfile);
				// }
				$verNum=$this->paso_model->getVerNum();								
				

				//Obtiene el nombre y extension de la imagen
				if($_FILES['userfile']['name']!=''){
					if($_FILES['userfile']['type']=='image/gif')
					$tipoExt='gif';
					else{
						if($_FILES['userfile']['type']=='image/jpg' or $_FILES['userfile']['type']=='image/jpeg')
							$tipoExt='jpg';
						else{
							if($_FILES['userfile']['type']=='image/png')
							$tipoExt='png';
						}
					}
					$datos = array(
						'PASO_ID' => $idpaso,
						'PROC_ID' => $idproc,
						'PASO_NOMBRE' => $nombPaso,
						'PASO_DESC' => $descripPaso,
						
						'FAC_ID' => $idfacultad,
						'SECC_ID' => $idsecc,
						'PASO_NUM' => $numPosiPaso,
						'PASO_LOGRO' => $logroPaso,
						
						'PASO_DOC' => 'paso'.$idpaso.'.'.$tipoExt,

						'PASO_ANTECESOR' => $antecesor,								

						'VER_NUM' => $verNum
					);
				}	
				else{
					$datos = array(
						'PASO_ID' => $idpaso,
						'PROC_ID' => $idproc,
						'PASO_NOMBRE' => $nombPaso,
						'PASO_DESC' => $descripPaso,
						
						'FAC_ID' => $idfacultad,
						'SECC_ID' => $idsecc,
						'PASO_NUM' => $numPosiPaso,
						'PASO_LOGRO' => $logroPaso,
						
						//'PASO_DOC' => 'paso'.$idpaso.'.'.$tipoExt,

						'PASO_ANTECESOR' => $antecesor,								

						'VER_NUM' => $verNum
					);
				}

				$this->paso_model->savePaso($datos);

				//Guardar la imagen
				if($_FILES['userfile']['name']!=''){
					$url = base_url();
					$config['upload_path']="./recursos/pasos";
					$config['allowed_types']="gif|jpg|png|jpeg";
					$config['file_name']="paso".$idpaso;
					$config['overwrite']=TRUE;
					$config['max_width'] = '1024';
					$config['max_height'] = '768';
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('userfile')){
						
						$error = array('error' => $this->upload->display_errors());
						$error1=$this->upload->display_errors();
						$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
						$datos['workspace'] = $this->load->view('gpasos/editar_paso',"",true);	
						$this->load->view('plantilla/plantilla',$datos);
						$msg['title'] = "Existen campos erroneos";
						$msg['mesage'] = 	"<b>Se ha producido un error al subir la imagen. 
											<br>Por favor revise y verifique la imagen que desea subir, para que todo este correcto</b><br><br>
											&diams; Subir nuevamente la imagen ".$error1;					
						$msg['type'] = "error_message";
						//$msg['view'] = "gusuarios/nuevo_usuario";
						$this->load->view('plantilla/message',$msg);
					}
				}
				
				//Obtener los datos a mostrar del nuevo paso guardado
				if($antecesor!=null){
					$paso=$this->paso_model->nombPaso($antecesor);
					$antecesor=$paso->row('PASO_NOMBRE');
				}
				else
					$antecesor=-1;
				if($antecesor!=-1)
					$stringAntecesor="<p> Antecesor: ".$antecesor."</p> ";					
				else	
					$stringAntecesor="<p> Antecesor: Ninguno</p> ";
				
				$proceso=$this->paso_model->nombProceso($idproc);
				$idproc=$proceso->row('PROC_NOMBRE');

				if($idsecc=!-1){
					$seccion=$this->paso_model->nombSeccion($idsecc);
					$idsecc=$seccion->row('SECC_NOMBRE');
				}
				if($idfacultad=!0){
					$facultad=$this->paso_model->nombFacultad($idfacultad);
					$idfacultad=$facultad->row('FAC_NOMBRE');
				}
				
				$this->nuevo_paso();
				$msg['title'] = "Registro Finalizado";
				$msg['mesage'] = 	"Los datos del paso han sido almacenados con &eacute;xito
									<p> Id: ".$idpaso."</p>	
									<p> Proceso: ".$idproc."</p>
									<p> Paso: ".$nombPaso."</p>
									<p> Descripci&oacute;n: ".$descripPaso."
									</p>".$stringAntecesor."</p>";					
				$msg['type'] = "information_message";
				$this->load->view('plantilla/message',$msg);
			}
			
		}
		else{
			if(isset($userfile)==false)
				$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,'',$antecesor);
			else
				$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,$userfile,$antecesor);			
			$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gpasos/nuevo_paso',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Paso ya existente";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}	
	}
	
	function mostrar_datos() 
    {
		extract($_POST);
		if(array_key_exists('Editar', $_POST))
			{
			$this->limpia();
			$edicion_s=$this->paso_model->obtener($idpaso);
			$this->session->set_userdata('idpaso',$edicion_s->row('PASO_ID'));
			$this->session->set_userdata('idproc',$edicion_s->row('PROC_ID'));
			$this->session->set_userdata('nombPaso',$edicion_s->row('PASO_NOMBRE'));
			$this->session->set_userdata('descripPaso',$edicion_s->row('PASO_DESC'));
			//$this->session->set_userdata('userfile',$edicion_s->row('PASO_DOC'));

			$this->session->set_userdata('idfacultad',$edicion_s->row('FAC_ID'));
			$this->session->set_userdata('idsecc',$edicion_s->row('SECC_ID'));
			$this->session->set_userdata('numPosiPaso',$edicion_s->row('PASO_NUM'));
			$this->session->set_userdata('logroPaso',$edicion_s->row('PASO_LOGRO'));

			if($edicion_s->row('PASO_DOC')!=null){
				$this->session->set_userdata('nombfile',base_url().'recursos/pasos/'.$edicion_s->row('PASO_DOC'));
			}
			
			if($edicion_s->row('PASO_ANTECESOR')==null)
				$this->session->set_userdata('antecesor',-1);
			else
				$this->session->set_userdata('antecesor',$edicion_s->row('PASO_ANTECESOR'));
			$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gpasos/editar_paso',"",true);	
			//$datos['paso']=$edicion_s->row('PASO_DOC');
			$this->load->view('plantilla/plantilla',$datos);
		}

		if(array_key_exists('Eliminar', $_POST)){
			$edicion_s=$this->paso_model->obtener($idpaso);
			$eliminar=$this->paso_model->deletePaso($idpaso);
			$msg['title'] = "Eliminaci&oacute;n";
			if($eliminar){				
				
				$msg['mesage'] = 	"<b>Resultado</b>
											<p>El paso  \"".$edicion_s->row('PASO_NOMBRE')."\" ha sido eliminado con &eacute;xito.</p>
											";					
				
			}
			else{
				$msg['mesage'] = 	"<b>Resultado</b>
											<p><b>El paso \"".$edicion_s->row('PASO_NOMBRE')."\" no se puede eliminar.</b></p>
											<p>Tiene dependencias.</p>";					
			}
			$this->consultar_paso();
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		}
	}
	
	function actualizar_datos()     
   {         	
		extract($_POST);
		//echo $idsecc;
		if($antecesor<$idpaso or $idpaso==0)		{
			if($idproc==-1){
				if(isset($userfile)==false)
				$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,'',$antecesor);
				else
					$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,$userfile,$antecesor);			
					$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gpasos/nuevo_paso',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos erroneos";
					$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
										<br>Por favor revise y verifique que todo este correcto</b><br><br>
										&diams; Debe seleccionar un proceso";					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuarios/nuevo_usuario";
					$this->load->view('plantilla/message',$msg);
			}
			else{
				if($antecesor==-1)
					$antecesor = null;
				
				if($idfacultad==-1)
					$idfacultad = null;
				
				if($idsecc==-1)
					$idsecc = null;
				//echo $idsecc;
				$verNum=$this->paso_model->getVerNum();								

				// $image = imagecreatefromgif($userfile);
				// ob_start();
				// imagegif($image);
				// $jpg = ob_get_contents();
				// ob_end_clean();
				// $jpg = str_replace('##','##',mysql_real_escape_string($jpg));
				
				// $fp = fopen ($userfile, 'r');
				// if ($fp){
				// 	$datos = fread ($fp, filesize ($userfile)); // cargo la imagen
				// 	fclose($fp);
				// 	$datos = base64_encode ($datos);
					
					
				// }
				
				
				//Obtiene el nombre y extension de la imagen
				if($_FILES['userfile']['name']!=''){
					if($_FILES['userfile']['type']=='image/gif')
					$tipoExt='gif';
					else{
						if($_FILES['userfile']['type']=='image/jpg' or $_FILES['userfile']['type']=='image/jpeg')
							$tipoExt='jpg';
						else{
							if($_FILES['userfile']['type']=='image/png')
							$tipoExt='png';
						}
					}
					$datos = array(
					
						'PROC_ID' => $idproc,
						'PASO_NOMBRE' => $nombPaso,
						'PASO_DESC' => $descripPaso,
						
						'FAC_ID' => $idfacultad,
						'SECC_ID' => $idsecc,
						'PASO_NUM' => $numPosiPaso,
						'PASO_LOGRO' => $logroPaso,
						
						'PASO_DOC' => 'paso'.$idpaso.'.'.$tipoExt,

						'PASO_ANTECESOR' => $antecesor,								

						'VER_NUM' => $verNum
					);
				}	
				else{
					$datos = array(
					
						'PROC_ID' => $idproc,
						'PASO_NOMBRE' => $nombPaso,
						'PASO_DESC' => $descripPaso,
						
						'FAC_ID' => $idfacultad,
						'SECC_ID' => $idsecc,
						'PASO_NUM' => $numPosiPaso,
						'PASO_LOGRO' => $logroPaso,
						
						//'PASO_DOC' => 'paso'.$idpaso.'.'.$tipoExt,

						'PASO_ANTECESOR' => $antecesor,								

						'VER_NUM' => $verNum
					);
				}
				$this->paso_model->updatePaso($idpaso,$datos);


				//Guardar la imagen
				if($_FILES['userfile']['name']!=''){
					$url = base_url();
					$config['upload_path']="./recursos/pasos";
					$config['allowed_types']="gif|jpg|png|jpeg";
					$config['file_name']="paso".$idpaso;
					$config['overwrite']=TRUE;
					$config['max_width'] = '1024';
					$config['max_height'] = '768';
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('userfile')){
						
						$error = array('error' => $this->upload->display_errors());
						$error1=$this->upload->display_errors();
						$datos['sidebar'] = $this->load->view('gpasos/sidebar',"",true);
						$datos['workspace'] = $this->load->view('gpasos/editar_paso',"",true);	
						$this->load->view('plantilla/plantilla',$datos);
						$msg['title'] = "Existen campos erroneos";
						$msg['mesage'] = 	"<b>Se ha producido un error al subir la imagen. 
											<br>Por favor revise y verifique la imagen que desea subir, para que todo este correcto</b><br><br>
											&diams; Subir nuevamente la imagen ".$error1;					
						$msg['type'] = "error_message";
						//$msg['view'] = "gusuarios/nuevo_usuario";
						$this->load->view('plantilla/message',$msg);
					}
				}

				


				if($antecesor!=null){
					$paso=$this->paso_model->nombPaso($antecesor);
					$antecesor=$paso->row('PASO_NOMBRE');
				}
				else
					$antecesor=-1;
				if($antecesor!=-1)
					$stringAntecesor="<p> Antecesor: ".$antecesor."</p> ";					
				else	
					$stringAntecesor="<p> Antecesor: Ninguno</p> ";
				

				$proceso=$this->paso_model->nombProceso($idproc);
				$idproc=$proceso->row('PROC_NOMBRE');

				if($idsecc=!null){
					$seccion=$this->paso_model->nombSeccion($idsecc);
					$idsecc=$seccion->row('SECC_NOMBRE');
				}
				else
					$idsecc=-1;
				if($idsecc!=-1)
					$stringSecc="<p> Secci&oacute;n Fin: ".$idsecc."</p>";					
				else	
					$stringSecc="<p> Secci&oacute;n Fin: Ninguno</p> ";

				if($idfacultad=!0){
					$facultad=$this->paso_model->nombFacultad($idfacultad);
					$idfacultad=$facultad->row('FAC_NOMBRE');
				}
				
				$this->consultar_paso();
				$msg['title'] = "Registro Finalizado";
				$msg['mesage'] = 	"Los datos del paso han sido almacenados con &eacute;xito
									<p> Id: ".$idpaso."</p>	
									<p> Proceso: ".$idproc."</p>
									<p> Paso: ".$nombPaso."</p>
									<p> Descripci&oacute;n: ".$descripPaso."
									</p>".$stringAntecesor."</p>";					
				$msg['type'] = "information_message";
				$this->load->view('plantilla/message',$msg);
				
			}
		}
		else{
			if(isset($userfile)==false)
				$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,'',$antecesor);
			else
				$this->getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,$userfile,$antecesor);		
			$datos['sidebar'] = $this->load->view('gprocesos/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gprocesos/editar_paso',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Antecesor no puede ser mayor";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}
	}	
	
	function limpia(){
		$this->session->set_userdata('idpaso','');
		$this->session->set_userdata('idproc','Seleccione...');
		$this->session->set_userdata('nombPaso','');
		$this->session->set_userdata('descripPaso','');
		$this->session->set_userdata('idfacultad','');
		$this->session->set_userdata('idsecc','');
		$this->session->set_userdata('numPosiPaso','');
		$this->session->set_userdata('logroPaso','');
		$this->session->set_userdata('userfile','');
		$this->session->set_userdata('antecesor','Seleccione...');
	}
	
	function getValores($idpaso,$idproc,$nombPaso,$descripPaso,$idfacultad,$idsecc,$numPosiPaso,$logroPaso,$userfile,$antecesor){
		$this->session->set_userdata('idpaso',$idpaso);
		$this->session->set_userdata('idproc',$idproc);
		$this->session->set_userdata('nombPaso',$nombPaso);
		$this->session->set_userdata('descripPaso',$descripPaso);
		$this->session->set_userdata('idfacultad',$idfacultad);
		$this->session->set_userdata('idsecc',$idsecc);
		$this->session->set_userdata('numPosiPaso',$numPosiPaso);
		$this->session->set_userdata('logroPaso',$logroPaso);	
		$this->session->set_userdata('userfile',$userfile);
		$this->session->set_userdata('antecesor',$antecesor);

	}
	
}

 ?>