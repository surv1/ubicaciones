<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function index()
	{
		if($this->session->userdata('eslogeado')){
            $this->load->view('pagina_inicio');
        }
        else{
            $this->validador();
        }
	}

	Public function validador(){
	
	$config = array( array(
                     'field'   => 'nick', 
                     'label'   => 'Nombre de Usuario', 
                     'rules'   => 'required'
                  ),array(
                     'field'   => 'password', 
                     'label'   => 'La contrase&ntilde;a', 
                     'rules'   => 'required'
                  ),  );

	$this->form_validation->set_rules($config);
	$this->form_validation->set_message('required', '%s se requiere');
	$this->form_validation->set_error_delimiters('&diams; &nbsp;', '<br/>');
    if ($this->form_validation->run()==FALSE )
		{
			if(array_key_exists('iniciar_sesion', $_POST))
			{
				$this->load->view('login');
				$msg['title'] = "Error de Informacion";
				$msg['mesage'] = 	"<p>Datos de Usuario se Requieren.</p>".validation_errors();				
				$msg['type'] = "error_message";
				//$msg['view'] = "login";
				$this->load->view('plantilla/message',$msg);
			}
			else
				$this->load->view('login');
		}
		else
		{
                 $this->load->model('login_model');
                 extract($_POST);
                    $Usuario=$this->login_model->ValidarUsuario($nick);    //    comprobamos que el usuario exista en la base de datos y la password ingresada sea correcta
                    if($Usuario->row('usuario')==$nick){    // La variable $ExisteUsuarioyPassoword recibe valor TRUE si el usuario existe y FALSE en caso que no. Este valor lo determina el modelo.
						if($Usuario->row('password')==$password){ 
							$usuario=  $this->login_model->DatosUsr($nick);
							$this->session->set_userdata('usuario',$usuario->row('usuario'));
							$this->session->set_userdata('nomb_usuario',$usuario->row('usuario'));
							$this->session->set_userdata('tipo_usuario','1');
							$this->session->set_userdata('eslogeado',TRUE);
							$this->session->set_userdata('perfil','1');
							$this->load->view('pagina_inicio');
						}
						else {
							$this->load->view('login');
							$msg['title'] = "Error de Informacion";
							$msg['mesage'] = 	"<p> &diams; Password Incorrecto</p>";				
							$msg['type'] = "error_message";
							//$msg['view'] = "login";
							$this->load->view('plantilla/message',$msg);
							//redirect('inicio');
						}  
					} 
					else 
					{
						$this->load->view('login');
						$msg['title'] = "Error de Informacion";
						$msg['mesage'] = 	"<p> &diams; Nombre de Usuario Incorrecto</p>";				
						$msg['type'] = "error_message";
						//$msg['view'] = "login";
						$this->load->view('plantilla/message',$msg);
						//redirect('inicio');
					}  
        }
  	}

  	public function cerrarSesion(){
		if($this->session->userdata('eslogeado')){
            $this->session->set_userdata('eslogeado',FALSE);
			$this->load->model('login_model');
			$this->session->sess_destroy();
        }
		$this->index();
      }
}