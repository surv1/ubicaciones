<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gestion_frases extends CI_Controller 
{     
    function __construct() 
    {         
		parent::__construct();         
		$this->load->model('frase_model');
    }
	
	function gFrase(){
		if($this->session->userdata('eslogeado')) {
			$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
			$datos['workspace'] = "</br>";
            $this->load->view('plantilla/plantilla',$datos);
        }
        else{
            redirect('inicio');
        }
	}
	
	function nueva_frase(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gfrases/nueva_frase',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
 	
	function consultar_frase(){
		if($this->session->userdata('eslogeado')) {
			$this->limpia();
			$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gfrases/frases',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			
		}
	}
	
	function cargarPersonajes(){
		//print_r( 'entra');
		
			$secc = $this->input->post('id_secc');
			$info['personajes']=$this->frase_model->comboPersonajes($secc);
			$output_string=$this->load->view('gfrases/comboPer',$info,true);
			$r = new stdClass();
			header('content-type: application/json; charset=utf-8');
			$r = utf8_encode($output_string);
			echo json_encode($r);
		
		
	}
	
	function cargarProcesos(){
		//print_r( 'entra');
		
			$fac = $this->input->post('id_fac');
			$info['procesos']=$this->frase_model->comboProcesos($fac);
			$output_string1=$this->load->view('gfrases/comboProc',$info,true);
			$r = new stdClass();
			header('content-type: application/json; charset=utf-8');
			$r = utf8_encode($output_string1);
			echo json_encode($r);
		
		
	}

	function cargarPasos(){
		
			$val=$this->input->post('todoIds');
			$total = explode(",", $val);
			$proc = $total[0];
			$fac = $total[1];
			//print_r($proc);
			$info['pasosxProc']=$this->frase_model->comboPasos($proc,$fac);
			$output_string2=$this->load->view('gfrases/comboPas',$info,true);
			$r = new stdClass();
			header('content-type: application/json; charset=utf-8');
			$r = utf8_encode($output_string2);
			echo json_encode($r);
		
		
	}

	function validar_registro(){
		extract($_POST);
		if(array_key_exists('Cancelar', $_POST)){
			$this->gFrase();
		}
		extract($_POST);
		$config = array( 
						// array(
						 // 'field'   => 'idFrase', 
						 // 'label'   => 'Id', 
						 // 'rules'   => 'required'
					  // ), 
					  array(
						 'field'   => 'idPaso', 
						 'label'   => 'Paso', 
						 'rules'   => 'required'
					  ),array(
						 'field'   => 'descripFrase', 
						 'label'   => 'Frase', 
						 'rules'   => 'required'
					  ) );

			$this->form_validation->set_rules($config);
			$this->form_validation->set_message('required', '%s, se requiere');
			$this->form_validation->set_error_delimiters('&diams; &nbsp;', '<br/>');
		if ($this->form_validation->run()==FALSE ){
			if(array_key_exists('Guardar_Frase', $_POST)){
				if(isset($idPer)==false)
					$this->getValores($idFrase,$idSecc,'',$idPaso,$descripFrase);
				else	
					$this->getValores($idFrase,$idSecc,$idPer,$idPaso,$descripFrase);
				$datos['personajes']=$this->frase_model->comboPersonajes($idSecc);
				//$datos['mostrarContenido']=$this->load->view('gfrases/comboPer',$info,true);
				$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gfrases/nueva_frase','',true);
				
				$this->load->view('plantilla/plantilla',$datos);
				
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
									<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
				$msg['type'] = "error_message";
				//$msg['view'] = "gsecciones/nueva_seccion";
				$this->load->view('plantilla/message',$msg);
				
			}
			else if(array_key_exists('Guardar_Cambios', $_POST)){
					$this->getValores($idFrase,$idSecc,$idPer,$idPaso,$descripFrase);
					$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gfrases/editar_frase',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
										<br>Por favor revise y verifique que todo este correcto</b><br><br>".validation_errors();					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuario/editar_usuario";
					$this->load->view('plantilla/message',$msg);
				}
		}	
		else{
				if(array_key_exists('Guardar_Frase', $_POST))
					$this->ingresar();
				else if(array_key_exists('Guardar_Cambios', $_POST)){
							$this->actualizar_datos();
						}				
			}
	}
	
	function ingresar(){
		extract($_POST);
		$existe=$this->frase_model->validarFrase($idFrase);
		if(!$existe){
			if($idSecc==0){
				if(isset($idPer)==false)
					$this->getValores($idFrase,$idSecc,'',$idPaso,$descripFrase);
				else	
					$idPer=-1;
					$this->getValores($idFrase,$idSecc,$idPer,$idPaso,$descripFrase);
				$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gfrases/nueva_frase',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Campo requerido. 
											<br></b><br><br> 
											&diams; Debe seleccionar la secci&oacute;n .";					
				$msg['type'] = "error_message";
				//$msg['view'] = "gusuarios/nuevo_usuario";
				$this->load->view('plantilla/message',$msg);
			}
			else{
				if($idPer==0){
					$this->getValores($idFrase,$idSecc,$idPer,$idPaso,$descripFrase);
					$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gfrases/nueva_frase',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Campo requerido. 
												<br></b><br><br> 
												&diams; Debe seleccionar el personaje .";					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuarios/nuevo_usuario";
					$this->load->view('plantilla/message',$msg);
				}
				else{
					if($idPaso==-1){
						$this->getValores($idFrase,$idSecc,$idPer,$idPaso,$descripFrase);
						$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
						$datos['workspace'] = $this->load->view('gfrases/nueva_frase',"",true);	
						$this->load->view('plantilla/plantilla',$datos);
						$msg['title'] = "Existen campos vacios";
						$msg['mesage'] = 	"<b>Campo requerido. 
													<br></b><br><br> 
													&diams; Debe seleccionar el paso correspondiente.";					
						$msg['type'] = "error_message";
						//$msg['view'] = "gusuarios/nuevo_usuario";
						$this->load->view('plantilla/message',$msg);
					}
					else{												$verNum=$this->frase_model->getVerNum();						
						$datos = array(
						'FRASE_ID' => $idFrase,
						'SECC_ID' => $idSecc,
						'PER_ID' => $idPer,
						'PASO_ID' => $idPaso,
						'FRASE_VALOR' => $descripFrase,
						'INSTI_ID' => 1,												
						'VER_NUM' => $verNum
						);
						$this->frase_model->saveFrase($datos);
						
						$seccion=$this->frase_model->nombSecc($idSecc);
						$idSecc=$seccion->row('SECC_NOMBRE');
						$personaje=$this->frase_model->nombPerson($idPer);
						$idPer=$personaje->row('PER_NOMBRE');
						$paso=$this->frase_model->nombPaso($idPaso);
						$idPaso=$paso->row('PASO_NOMBRE');

						$this->nueva_frase();
						$msg['title'] = "Registro Finalizado";
						$msg['mesage'] = 	"Los datos de la frase han sido almacenados con &eacute;xito
											
											<p> Secci&oacute;n: ".$idSecc."</p>
											<p> Personaje: ".$idPer."</p>	
											<p> Paso: ".$idPaso."</p> 
											<p> Frase: ".$descripFrase."</p>	";					
						$msg['type'] = "information_message";
						$this->load->view('plantilla/message',$msg);
					}
				}
			}
		}
		else{
			$this->getValores($idFrase,$idSecc,$idPer,$idPaso,$descripFrase);
			$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gfrases/nueva_frase',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos erroneos";
			$msg['mesage'] = 	"<b>Se ha producido un error al procesar la informaci&oacute;n. 
								<br>Por favor revise y verifique que todo este correcto</b><br><br>
								&diams; Frase ya existente";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}	
	}
	
	function mostrar_datos() 
    {
		extract($_POST);
		if(array_key_exists('Editar', $_POST))
			{
			$this->limpia();
			$edicion_s=$this->frase_model->obtener($idfrase);
			$this->session->set_userdata('idFrase',$edicion_s->row('FRASE_ID'));
			$this->session->set_userdata('idSecc',$edicion_s->row('SECC_ID'));
			//$des[$edicion_s->row('SECC_D_ID')]=$edicion_s->row('DESTINO');
			$this->session->set_userdata('idPer',$edicion_s->row('PER_ID'));
			$this->session->set_userdata('idfac',$edicion_s->row('FAC_ID'));
			$this->session->set_userdata('idProces',$edicion_s->row('PROC_ID'));
			$this->session->set_userdata('idPaso',$edicion_s->row('PASO_ID'));
			$this->session->set_userdata('descripFrase',$edicion_s->row('FRASE_VALOR'));
			//$this->session->set_userdata('prioridad',$edicion_s->row('FRASE_PRIORIDAD'));
			$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gfrases/editar_frase',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
		}  

		if(array_key_exists('Eliminar', $_POST)){
			$edicion_s=$this->frase_model->obtener($idfrase);
			$eliminar=$this->frase_model->deleteFrase($idfrase);
			$msg['title'] = "Eliminaci&oacute;n";
			if($eliminar){				
				
				$msg['mesage'] = 	"<b>Resultado</b>
											<p>La frase  \"".$edicion_s->row('FRASE_ID')."\" ha sido eliminada con &eacute;xito.</p>
											";					
				
			}
			
			// else{
				// $msg['mesage'] = 	"<b>Resultado</b>
											// <p><b>El segmento \"".$edicion_s->row('SEG_NOMBRE')."\" no se puede eliminar.</b></p>
											// <p>Tiene dependencias.</p>";					
			// }
			
			
			$this->consultar_frase();
			$msg['type'] = "information_message";
			$this->load->view('plantilla/message',$msg);
		} 
	}
	
	function actualizar_datos()     
   {         	
		extract($_POST);
		if($idSecc==0){
			if(isset($idPer)==false)
				$this->getValores($idFrase,$idSecc,'',$idPaso,$descripFrase);
			else	
				$idPer=-1;
				$this->getValores($idFrase,$idSecc,$idPer,$idPaso,$descripFrase);
			$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
			$datos['workspace'] = $this->load->view('gfrases/nueva_frase',"",true);	
			$this->load->view('plantilla/plantilla',$datos);
			$msg['title'] = "Existen campos vacios";
			$msg['mesage'] = 	"<b>Campo requerido. 
										<br></b><br><br> 
										&diams; Debe seleccionar la secci&oacute;n .";					
			$msg['type'] = "error_message";
			//$msg['view'] = "gusuarios/nuevo_usuario";
			$this->load->view('plantilla/message',$msg);
		}
		else{
			if($idPer==0){
				$this->getValores($idFrase,$idSecc,$idPer,$idPaso,$descripFrase);
				$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
				$datos['workspace'] = $this->load->view('gfrases/nueva_frase',"",true);	
				$this->load->view('plantilla/plantilla',$datos);
				$msg['title'] = "Existen campos vacios";
				$msg['mesage'] = 	"<b>Campo requerido. 
											<br></b><br><br> 
											&diams; Debe seleccionar el personaje .";					
				$msg['type'] = "error_message";
				//$msg['view'] = "gusuarios/nuevo_usuario";
				$this->load->view('plantilla/message',$msg);
			}
			else{
				if($idPaso==-1){
					$this->getValores($idFrase,$idSecc,$idPer,$idPaso,$descripFrase);
					$datos['sidebar'] = $this->load->view('gfrases/sidebar',"",true);
					$datos['workspace'] = $this->load->view('gfrases/nueva_frase',"",true);	
					$this->load->view('plantilla/plantilla',$datos);
					$msg['title'] = "Existen campos vacios";
					$msg['mesage'] = 	"<b>Campo requerido. 
												<br></b><br><br> 
												&diams; Debe seleccionar el paso correspondiente.";					
					$msg['type'] = "error_message";
					//$msg['view'] = "gusuarios/nuevo_usuario";
					$this->load->view('plantilla/message',$msg);
				}
				else{										$verNum=$this->frase_model->getVerNum();					
					$datos = array(
					//'FRASE_ID' => $idFrase,
					'SECC_ID' => $idSecc,
					'PER_ID' => $idPer,
					'PASO_ID' => $idPaso,
					'FRASE_VALOR' => $descripFrase,
					'INSTI_ID' => 1,										
					'VER_NUM' => $verNum
					);
					$this->frase_model->updateFrase($idFrase,$datos);
					
					$seccion=$this->frase_model->nombSecc($idSecc);
					$idSecc=$seccion->row('SECC_NOMBRE');
					$personaje=$this->frase_model->nombPerson($idPer);
					$idPer=$personaje->row('PER_NOMBRE');
					$paso=$this->frase_model->nombPaso($idPaso);
					$idPaso=$paso->row('PASO_NOMBRE');

					$this->consultar_frase();
					$msg['title'] = "Registro Finalizado";
					$msg['mesage'] = 	"Los datos de la frase han sido almacenados con &eacute;xito
										<p> Id: ".$idFrase."</p>	
										<p> Secci&oacute;n: ".$idSecc."</p>
										<p> Personaje: ".$idPer."</p>	
										<p> Paso: ".$idPaso."</p> 
										<p> Frase: ".$descripFrase."</p> ";					
					$msg['type'] = "information_message";
					$this->load->view('plantilla/message',$msg);
				}
			}
		}
	}	
	
	function limpia(){
		$this->session->set_userdata('idFrase','');
		$this->session->set_userdata('idSecc',-1);
		$this->session->set_userdata('idPer','');
		$this->session->set_userdata('idPaso','');
		$this->session->set_userdata('descripFrase','');
		//$this->session->set_userdata('prioridad','');
	}
	
	function getValores($idFrase,$idSecc,$idPer,$idPaso,$descripFrase){
		$this->session->set_userdata('idFrase',$idFrase);
		$this->session->set_userdata('idSecc',$idSecc);
		$this->session->set_userdata('idPer',$idPer);
		$this->session->set_userdata('idPaso',$idPaso);
		$this->session->set_userdata('descripFrase',$descripFrase);
		//$this->session->set_userdata('prioridad',$prioridad);
	}
	
}

 ?>