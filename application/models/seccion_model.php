<?php class seccion_model extends CI_Model {     
 
	function __construct() 
	{        
		parent::__construct();    
	}     
	 /*************Datos ocupado dentro de los campos de los formularios************/
 	function  comboSegmentos(){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT SEG_ID,SEG_NOMBRE FROM segmento INNER JOIN institucionbd ON institucionbd.INSTI_ID=segmento.INSTI_ID WHERE segmento.INSTI_ID=1 AND VER_NUM='.$version);
		if($query->num_rows()>0){
				$segmento[0]='Seleccione...';
				foreach($query->result() as $row){
					$segmento[$row->SEG_ID]=$row->SEG_NOMBRE;
				}
			}
			else
				$segmento[0]='No hay segmentos';
		$query->free_result();
		return $segmento;
	 }

	function  comboAulas(){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT idAula,A_nombre FROM aula WHERE VER_NUM='.$version);
		$aula[0]='Ninguna';
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$aula[$row->idAula]=$row->A_nombre;
			}
		}
		$query->free_result();
		return $aula;
	}

	 /*******************************************************************/
	
	function validarSeccion($id){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('SECC_ID',$id);
		$query=$this->db->where('VER_NUM',$version);
		$query = $this->db->get('seccion');
	    return $query->row(); //    Devuelve al controlador la fila que coincide con la b�squeda. (FALSE en caso que no existir coincidencias)
	}
	
	function validarRango($idseg){
		//$version=$this->getVerNum();
		//if($version==null){
		//	$version=0;
		//}
		//$query=$this->db->where('VER_NUM',$version);
		
		$query=$this->db->where('INSTI_ID',1);
		$query=$this->db->where('SEG_ID',$idseg);
		$query = $this->db->get('segmento');
	    return $query;
	}		


	function getVerNum(){		

		$query=$this->db->query('SELECT VER_NUM FROM version WHERE VER_ESTADO=3 ORDER BY VER_NUM DESC');		

		if($query->num_rows()>0){			

			foreach($query->result() as $row){					

				$version=$row->VER_NUM;				

			}		

		}		

		else			

			$version=null;		

		return $version;	

	}	


	function saveSeccion($datos) {
		$this->db->insert('seccion',$datos);
	}
	
	function saveSeccionGPS($datos) {
		$this->db->insert('ptos_gps',$datos);
	}

	function nombSegmento($idseg){
		$query=$this->db->where('SEG_ID',$idseg);
		$query = $this->db->get('segmento');
	    return $query;
	}

	function nombAula($idAula){
		$query=$this->db->where('idAula',$idAula);
		$query = $this->db->get('aula');
	    return $query;
	}

	function secciones() 
	{         
		$lista_secciones[]=array();
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query = $this->db->query('SELECT DISTINCT SECC_ID,segmento.SEG_NOMBRE,SECC_NOMBRE,SECC_DESC, 
		CASE SECCTIPO_INST WHEN "1" THEN "Externa" ELSE "Interna" END AS TIPO, SECC_BRUJULA, SECC_MAP from seccion INNER JOIN segmento ON segmento.SEG_ID=seccion.SEG_ID 
		INNER JOIN institucionbd ON institucionbd.INSTI_ID=seccion.INSTI_ID 
		INNER JOIN version ON version.VER_NUM=seccion.VER_NUM
		WHERE seccion.INSTI_ID=1 AND  version.VER_NUM='. $version.'
		ORDER BY  SECC_ID ASC ');
		if($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$lista_secciones[]="<tr>
				<td><center>".$row['SECC_ID']."</center></td>
				<td><center>".$row['SEG_NOMBRE']."</center></td>
				<td><center>".$row['SECC_NOMBRE']."</center></td>
				<td><center>".$row['SECC_DESC']."</center></td>
				<td><center>".$row['TIPO']."</center></td>
				<td><center>".$row['SECC_BRUJULA']."</center></td>								<td><center>".$row['SECC_MAP']."</center></td>
				<td><input type='submit' class='button-submit' onclick='this.form.idseccion.value=".$row['SECC_ID']."' name='Editar' value='Editar'></td>								<td><input type='submit' class='button-submit' onclick='this.form.idseccion.value=".$row['SECC_ID']."' name='Eliminar' value='Eliminar'></td>
				</tr>";
			}
		}
		else
			$lista_secciones[]="";
		$query->free_result();
		return $lista_secciones;
	}
	
	function obtener($id) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$this->db->where('VER_NUM',$version);
    	$this->db->where('SECC_ID',$id);
        $query = $this->db->get('seccion');
    	return $query;
    }
	
	function obtenerGPS($id,$posi) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$this->db->where('VER_NUM',$version);
    	$this->db->where('SECC_ID',$id);
    	$this->db->where('PGPS_POSI',$posi);
        $query = $this->db->get('ptos_gps');
        if($query->num_rows()>0){
        	return $query;
        }
    	else{
    		return $query=null;
    	}
    }

	function updateSeccion($id, $data) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$this->db->where('VER_NUM',$version);
		$this->db->where('SECC_ID', $id);
		return $this->db->update('seccion', $data);
	}
	
	function updateSeccionGPS($id,$posi, $data) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$this->db->where('VER_NUM',$version);
		$this->db->where('PGPS_POSI', $posi);
		$this->db->where('SECC_ID', $id);
		$query = $this->db->get('ptos_gps');
		if($query->num_rows()>0){
			$this->db->where('VER_NUM',$version);
			$this->db->where('PGPS_POSI', $posi);
			$this->db->where('SECC_ID', $id);
			return $this->db->update('ptos_gps', $data);
		}
		else{
			return $this->db->insert('ptos_gps', $data);
		}
		
	}

	function deleteSeccion($id){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}

		$queryConex=$this->db->query('SELECT SECC_O_ID, SECC_D_ID FROM conexion WHERE SECC_O_ID='.$id.' OR 						SECC_D_ID='.$id.' AND VER_NUM='.$version);
		$queryInfo=$this->db->query('SELECT SECC_ID FROM info WHERE SECC_ID='.$id.' AND VER_NUM='.$version);
		$queryPer=$this->db->query('SELECT SECC_ID FROM personaje WHERE SECC_ID='.$id.' AND VER_NUM='.$version);		
		$queryFrase=$this->db->query('SELECT SECC_ID FROM frase WHERE SECC_ID='.$id.' AND VER_NUM='.$version);
		
			if($queryConex->num_rows()>0 OR $queryInfo->num_rows()>0 OR $queryPer->num_rows()>0 OR $queryFrase->num_rows()>0){
				return false;
			}
			else{
				$this->db->where('VER_NUM',$version);
				$this->db->where('SECC_ID',$id);
				$query = $this->db->delete('seccion');
				return true;
			}
	}
}

 ?>