<?php class paso_model extends CI_Model {     
 
	function __construct() 
	{        
		parent::__construct();    
	}     
	 /*************Datos ocupado dentro de los campos de los formularios************/
	 function  comboProcesos(){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT PROC_ID,PROC_NOMBRE FROM proceso WHERE VER_NUM='.$version);
		if($query->num_rows()>0){
					$proceso[-1]='Seleccione...';
				foreach($query->result() as $row){
					// if($row->PROC_ID!=0)
						$proceso[$row->PROC_ID]=$row->PROC_NOMBRE;
				}
			}
			else
				$proceso[0]='No hay procesos';
		// if(count($proceso)==1)
			// $proceso[0]='No hay procesos';
		$query->free_result();
		return $proceso;
	 }
	 
	 function comboPasos(){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT PASO_ID,PASO_NOMBRE FROM paso WHERE VER_NUM='.$version);
		if($query->num_rows()>0){
					$paso[-1]='Seleccione...';
				foreach($query->result() as $row){
					// if($row->PROC_ID!=0)
						$paso[$row->PASO_ID]=$row->PASO_NOMBRE;
				}
			}
			else
				$paso[-1]='No hay pasos';
		// if(count($paso)==1)
			// $paso[0]='No hay pasos';
		$query->free_result();
		return $paso;
	 }

	 function  comboSecciones(){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT SECC_ID,SECC_NOMBRE FROM seccion INNER JOIN institucionbd ON institucionbd.INSTI_ID=seccion.INSTI_ID WHERE seccion.INSTI_ID=1 AND VER_NUM='.$version);
		if($query->num_rows()>0){
				$seccion[-1]='Seleccione...';
				foreach($query->result() as $row){
					if($row->SECC_ID!=0)
						$seccion[$row->SECC_ID]=$row->SECC_NOMBRE;
				}
			}
			else
				$seccion[-1]='No hay secciones';
		$query->free_result();
		return $seccion;
	 }
	 
	 function  comboFacultades(){
		// $version=$this->getVerNum();
		// if($version==null){
		// 	$version=0;
		// }
		$query=$this->db->query('SELECT FAC_ID,FAC_NOMBRE FROM facultad ');
		if($query->num_rows()>0){
					$facultades[-1]='Seleccione...';
				foreach($query->result() as $row){
					// if($row->PROC_ID!=0)
						$facultades[$row->FAC_ID]=$row->FAC_NOMBRE;
				}
			}
			else
				$facultades[0]='No hay facultades';
		// if(count($proceso)==1)
			// $proceso[0]='No hay procesos';
		$query->free_result();
		return $facultades;
	 }

	 function nombImg($id){
		$query=$this->db->query('SELECT PASO_ID,PASO_DOC FROM paso WHERE PASO_ID='.$id);
		if($query->num_rows()>0){
				foreach($query->result() as $row){
					// if($row->PROC_ID!=0)
						$nombImg=$row->PASO_DOC;
				}
			}
			else
				$nombImg='No hay pasos';
		// if(count($paso)==1)
			// $paso[0]='No hay pasos';
		$query->free_result();
		return $nombImg;
	 }

	 /*******************************************************************/
	
	function validarPaso($id,$nomb){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('PASO_ID',$id);
		$query=$this->db->where('PASO_NOMBRE',$id);
		$query = $this->db->get('paso');
	    return $query->row(); //    Devuelve al controlador la fila que coincide con la busqueda. (FALSE en caso que no existir coincidencias)
	}
	
	function getVerNum(){		
	
	$query=$this->db->query('SELECT VER_NUM FROM version WHERE VER_ESTADO=3 ORDER BY VER_NUM DESC');		
	
	if($query->num_rows()>0){			
	
		foreach($query->result() as $row){					
	
			$version=$row->VER_NUM;				
		}		
	}		
	
	else			
	$version=null;
	
	return $version;	
	}
	
	
	function savePaso($datos) {
		$this->db->insert('paso',$datos);
	}
	
	function  nombPaso($idpaso){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('PASO_ID', $idpaso);
		$query= $this->db->get('paso');
		return $query;
	 }
	 
	 function  nombProceso($idproc){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('PROC_ID', $idproc);
		$query= $this->db->get('proceso');
		return $query;
	 }
	 
	 function  nombSeccion($idsecc){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('SECC_ID', $idsecc);
		$query= $this->db->get('seccion');
		return $query;
	 }

	 function  nombFacultad($idfac){
	 // 	$version=$this->getVerNum();
		// if($version==null){
		// 	$version=0;
		// }
		// $query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('FAC_ID', $idfac);
		$query= $this->db->get('facultad');
		return $query;
	 }

	 
	function pasos() 
	{         
		$lista_pasos[]=array();
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query = $this->db->query('SELECT DISTINCT paso1.PASO_ID,PROC_NOMBRE,paso1.PASO_NOMBRE,paso1.PASO_DESC,FAC_NOMBRE,(SELECT DISTINCT paso2.PASO_NOMBRE 
		FROM paso paso2 WHERE paso2.PASO_ID=paso1.PASO_ANTECESOR ) AS ANTECESOR, paso1.VER_NUM FROM paso paso1
		INNER JOIN proceso ON proceso.PROC_ID=paso1.PROC_ID 
		INNER JOIN version ON version.VER_NUM=paso1.VER_NUM
		LEFT JOIN facultad ON facultad.FAC_ID=paso1.FAC_ID
		WHERE  version.VER_NUM='. $version);
		if($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$lista_pasos[]="<tr>
				<td><center>".$row['PASO_ID']."</center></td>
				<td><center>".$row['PROC_NOMBRE']."</center></td>
				<td><center>".$row['PASO_NOMBRE']."</center></td>
				<td><center>".$row['PASO_DESC']."</center></td>
				<td><center>".$row['ANTECESOR']."</center></td>
				<td><center>".$row['FAC_NOMBRE']."</center></td>
				<td><input type='submit' class='button-submit' onclick='this.form.idpaso.value=".$row['PASO_ID']."' name='Editar' value='Editar'></td>								
				
				<td><input type='submit' class='button-submit' onclick='this.form.idpaso.value=".$row['PASO_ID']."' name='Eliminar' value='Eliminar'></td>
				</tr>";
			}
		}
		else
			$lista_pasos[]="";
		$query->free_result();
		return $lista_pasos;
	}
	
	function obtener($id) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query = $this->db->where('PASO_ID',$id);
		$query = $this->db->get('paso');
        return $query;
    }
	
	function updatePaso($id, $data) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$this->db->where('VER_NUM',$version);
		$this->db->where('PASO_ID', $id);
		return $this->db->update('paso', $data);
	}
	
	function deletePaso($id){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		
		$query=$this->db->query('SELECT PASO_ID FROM frase WHERE PASO_ID='.$id.' AND VER_NUM='.$version);			
		$queryPaso=$this->db->query('SELECT PASO_ANTECESOR FROM paso WHERE PASO_ANTECESOR='.$id.' AND VER_NUM='.$version);			
			if($query->num_rows()>0 OR $queryPaso->num_rows()>0){
				return false;
			}
			else{
				$this->db->where('VER_NUM',$version);
				$this->db->where('PASO_ID',$id);
				$query = $this->db->delete('paso');
				return true;
			}
	}
}

 ?>