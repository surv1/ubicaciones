<?php class frase_model extends CI_Model {     
 
	function __construct() 
	{        
		parent::__construct();    
	}     
	 /*************Datos ocupado dentro de los campos de los formularios************/
	 function  comboPersonajes($idsecc){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT PER_ID,CONCAT(PER_NOMBRE," ",PER_APELLIDO) AS NOMBRE FROM personaje 
		INNER JOIN seccion ON seccion.SECC_ID=personaje.SECC_ID 
		INNER JOIN institucionbd ON institucionbd.INSTI_ID=personaje.INSTI_ID WHERE personaje .INSTI_ID=1
		AND personaje.SECC_ID='.$idsecc.' AND personaje.VER_NUM='.$version);
		if($query->num_rows()>0){
					$pesonajes[0]='Seleccione...';
				foreach($query->result() as $row){
						$pesonajes[$row->PER_ID]=$row->NOMBRE;
				}
			}
			else
				$pesonajes[0]='No hay personajes en esta secci&oacute;n';
		$query->free_result();
		return $pesonajes;
		// $pesonajes=$this->db->query('SELECT PER_ID,CONCAT(PER_NOMBRE," ",PER_APELLIDO) AS NOMBRE FROM personaje 
		// INNER JOIN seccion ON seccion.SECC_ID=personaje.SECC_ID WHERE personaje.SECC_ID='.$isecc);
        // if($pesonajes->num_rows()>0)
        // {
            // return $pesonajes->result();
        // }
	 }
	 
	 function  comboSecciones(){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT SECC_ID,SECC_NOMBRE FROM seccion INNER JOIN institucionbd ON institucionbd.INSTI_ID=seccion.INSTI_ID WHERE seccion.INSTI_ID=1 AND VER_NUM='.$version);
		if($query->num_rows()>0){
				$seccion[0]='Seleccione...';
				foreach($query->result() as $row){
					if($row->SECC_ID!=0)
						$seccion[$row->SECC_ID]=$row->SECC_NOMBRE;
				}
			}
			else
				$seccion[0]='No hay secciones';
		$query->free_result();
		return $seccion;
	 }
	 
	 

	 function  comboFacultades(){
		$version=$this->getVerNum();
		// if($version==null){
		// 	$version=0;
		// }
		$query=$this->db->query('SELECT FAC_ID,FAC_NOMBRE FROM facultad');
		if($query->num_rows()>0){
				$facultad[0]='Seleccione...';
				foreach($query->result() as $row){
					$facultad[$row->FAC_ID]=$row->FAC_NOMBRE;
				}
			}
			else
				$facultad[0]='No hay facultades';
		$query->free_result();
		return $facultad;
	 }

	 function  comboProcesos($idfac){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT PROC_ID,PROC_NOMBRE FROM proceso WHERE proceso.VER_NUM='.$version);
		if($query->num_rows()>0){
					$procesos[-1]='Seleccione...';
				foreach($query->result() as $row){
					if($row->PROC_ID!=0)
						$procesos[$row->PROC_ID]=$row->PROC_NOMBRE;
				}
			}
			else
				$procesos[-1]='No hay procesos para esta facultad';
		$query->free_result();
		return $procesos;
		// $pesonajes=$this->db->query('SELECT PER_ID,CONCAT(PER_NOMBRE," ",PER_APELLIDO) AS NOMBRE FROM personaje 
		// INNER JOIN seccion ON seccion.SECC_ID=personaje.SECC_ID WHERE personaje.SECC_ID='.$isecc);
        // if($pesonajes->num_rows()>0)
        // {
            // return $pesonajes->result();
        // }
	 }

	 function  comboPasos($idproc,$idfac){
	  
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		if($idproc!='' AND $idfac!='' ){

			$query=$this->db->query('SELECT PASO_ID,PASO_NOMBRE FROM paso 
				WHERE VER_NUM='.$version.' AND PROC_ID='.$idproc.' AND FAC_ID='.$idfac);
			if($query->num_rows()>0){
					$pasos[-1]='Seleccione...';
					foreach($query->result() as $row){
							$pasos[$row->PASO_ID]=$row->PASO_NOMBRE;
					}
				}
				else
					$pasos[-1]='No hay pasos';
			$query->free_result();
			return $pasos;
		}
		else{
			$query=$this->db->query('SELECT PASO_ID,PASO_NOMBRE FROM paso 
				WHERE VER_NUM='.$version.' AND PROC_ID=0 AND FAC_ID IS NULL');
			if($query->num_rows()>0){
					$pasos[-1]='Seleccione...';
					foreach($query->result() as $row){
							$pasos[$row->PASO_ID]=$row->PASO_NOMBRE;
					}
				}
			return $pasos;
		}
	 }


	 
	 /*******************************************************************/
	
	
	function validarFrase($id){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('INSTI_ID',1);
		$query=$this->db->where('FRASE_ID',$id);
		$query = $this->db->get('frase');
	    return $query->row(); //    Devuelve al controlador la fila que coincide con la búsqueda. (FALSE en caso que no existir coincidencias)
	}
	
	function getVerNum(){		
		
		$query=$this->db->query('SELECT VER_NUM FROM version WHERE VER_ESTADO=3 ORDER BY VER_NUM DESC');		
		
		if($query->num_rows()>0){			
		
			foreach($query->result() as $row){					
		
				$version=$row->VER_NUM;				
			}		
		}		
		
		else			
			$version=null;		
		return $version;	
	}


	function saveFrase($datos) {
		$this->db->insert('frase',$datos);
	}
		
	function  nombSecc($idsec){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('INSTI_ID',1);
		$query=$this->db->where('SECC_ID', $idsec);
		$query= $this->db->get('seccion');
		return $query;
	 }
	 
	function  nombPerson($idper){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('INSTI_ID',1);
		$query=$this->db->where('PER_ID', $idper);
		$query= $this->db->get('personaje');
		return $query;
	 }
	 
	 function nombPaso($idpaso){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		
		//$query=$this->db->where('INSTI_ID',1);
		
		$query=$this->db->where('PASO_ID', $idpaso);
		$query= $this->db->get('paso');
		return $query;
	 }
	 
	 
	function frases() 
	{         
		$lista_frases[]=array();
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query = $this->db->query('SELECT  DISTINCT frase.FRASE_ID,SECC_NOMBRE,PER_NOMBRE,PER_APELLIDO,PASO_NOMBRE,FRASE_VALOR,FRASE_PRIORIDAD FROM frase
		LEFT JOIN seccion ON seccion.SECC_ID=frase.SECC_ID
		INNER JOIN personaje ON 	personaje.PER_ID=frase.PER_ID
		INNER JOIN paso ON paso.PASO_ID=frase.PASO_ID
		INNER JOIN institucionbd ON institucionbd.INSTI_ID=frase.INSTI_ID 
		INNER JOIN version ON version.VER_NUM=frase.VER_NUM
		WHERE frase.INSTI_ID=1 AND  version.VER_NUM='. $version);
		if($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$lista_frases[]="<tr>
				<td><center>".$row['FRASE_ID']."</center></td>
				<td><center>".$row['SECC_NOMBRE']."</center></td>
				<td><center>".$row['PER_NOMBRE']." ".$row['PER_APELLIDO']."</center></td>
				<td><center>".$row['PASO_NOMBRE']."</center></td>
				<td><center>".$row['FRASE_VALOR']."</center></td>
				<td><input type='submit' class='button-submit' onclick='this.form.idfrase.value=".$row['FRASE_ID']."' name='Editar' value='Editar'></td>								<td><input type='submit' class='button-submit' onclick='this.form.idfrase.value=".$row['FRASE_ID']."' name='Eliminar' value='Eliminar'></td>
				</tr>";
			}
		}
		else
			$lista_frases[]="";
		$query->free_result();
		return $lista_frases;
	}
	
	function obtener($id) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query = $this->db->query('SELECT  DISTINCT frase.FRASE_ID,frase.SECC_ID,frase.PER_ID,frase.PASO_ID,paso.FAC_ID,paso.PROC_ID,FRASE_VALOR,FRASE_PRIORIDAD,frase.VER_NUM FROM frase
		INNER JOIN seccion ON seccion.SECC_ID=frase.SECC_ID
		INNER JOIN personaje ON personaje.PER_ID=frase.PER_ID
		INNER JOIN paso ON paso.PASO_ID=frase.PASO_ID
		LEFT JOIN proceso ON proceso.PROC_ID=paso.PROC_ID
		LEFT JOIN facultad ON facultad.FAC_ID=paso.FAC_ID
		INNER JOIN institucionbd ON institucionbd.INSTI_ID=frase.INSTI_ID 
		INNER JOIN version ON version.VER_NUM=frase.VER_NUM
		WHERE frase.INSTI_ID=1 AND  version.VER_NUM='.$version.' AND frase.FRASE_ID='.$id);
        return $query;
    }
	
	function updateFrase($id, $data) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$this->db->where('VER_NUM',$version);
		$this->db->where('INSTI_ID',1);
		$this->db->where('FRASE_ID', $id);
		return $this->db->update('frase', $data);
	}
	
	function deleteFrase($id){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT FRASE_ID FROM frase WHERE FRASE_ID='.$id.' AND VER_NUM='.$version);			
			if($query->num_rows()>0){
				
				$this->db->where('VER_NUM',$version);
				$this->db->where('INSTI_ID',1);
				$this->db->where('FRASE_ID',$id);
				$query = $this->db->delete('frase');
				return true;
			}
	}
}

 ?>