<?php class info_model extends CI_Model {     
 
	function __construct() 
	{        
		parent::__construct();    
	}     
	 /*************Datos ocupado dentro de los campos de los formularios************/
	 function  comboSecciones(){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT SECC_ID,SECC_NOMBRE FROM seccion  WHERE VER_NUM='.$version);
		if($query->num_rows()>0){
				$seccion[0]='Seleccione...';
				foreach($query->result() as $row){
					$seccion[$row->SECC_ID]=$row->SECC_NOMBRE;
				}
			}
			else
				$seccion[]='No hay secciones';
		$query->free_result();
		return $seccion;
	 }
	 
	 
	 /*******************************************************************/
	
	function validarInfo($infonum,$idsecc,$infotipo){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('INFO_NUM',$infonum);	
		$query=$this->db->where('SECC_ID',$idsecc);
		$query=$this->db->where('INFO_TIPO',$infotipo);
		$query = $this->db->get('info');
	    return $query->row(); //    Devuelve al controlador la fila que coincide con la búsqueda. (FALSE en caso que no existir coincidencias)
	}
	
	
	function getVerNum(){		
	
	$query=$this->db->query('SELECT VER_NUM FROM version WHERE VER_ESTADO=3 ORDER BY VER_NUM DESC');		
	if($query->num_rows()>0){			
	foreach($query->result() as $row){					
		$version=$row->VER_NUM;				
		}		
	}		
	else			
		$version=null;		
	return $version;	
	}
	
	
	
	function saveInfo($datos) {
		$this->db->insert('info',$datos);
	}
	
	function  nombSeccion($idsecc){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('SECC_ID', $idsecc);
		$query= $this->db->get('seccion');
		return $query;
	 }
	 
	function informaciones() 
	{         
		$lista_conexiones[]=array();
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		
		$query = $this->db->query('SELECT DISTINCT INFO_ID,INFO_NUM,SECC_NOMBRE,INFO_CONTENIDO,INFO_TEMA,CASE INFO_TIPO WHEN "0" THEN "Mapa" WHEN "1" THEN "Rotulo" 
		WHEN "2" THEN "Procesos administrativos" WHEN "3" THEN "Procesos acad&eacute;micos" WHEN "4" THEN "Informaci&oacute;n de instalaci&oacute;n"  ELSE "Autoridades" END AS TIPOINFO FROM info 
		INNER JOIN seccion ON seccion.SECC_ID=info.SECC_ID
		INNER JOIN version ON (version.VER_NUM=info.VER_NUM AND version.VER_NUM=seccion.VER_NUM)
		WHERE version.VER_NUM='. $version);
		
		if($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$lista_conexiones[]="<tr>
				<td><center>".$row['INFO_ID']."</center></td>
				<td><center>".$row['INFO_NUM']."</center></td>
				<td><center>".$row['SECC_NOMBRE']."</center></td>
				<td><center>".$row['INFO_CONTENIDO']."</center></td>
				<td><center>".$row['INFO_TEMA']."</center></td>
				<td><center>".$row['TIPOINFO']."</center></td>
				<td><input type='submit' class='button-submit' onclick='this.form.idinfo.value=".$row['INFO_ID']."' name='Editar' value='Editar'></td>			
				
				<td><input type='submit' class='button-submit' onclick='this.form.idinfo.value=".$row['INFO_ID']."' name='Eliminar' value='Eliminar'></td>
				</tr>";
			}
		}
		else
			$lista_conexiones[]="";
		$query->free_result();
		return $lista_conexiones;
	}
	
	function obtener($id) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query = $this->db->where('INFO_ID',$id);
		$query = $this->db->get('info');
        return $query;
    }
	
	function updateInfo($id, $data) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$this->db->where('VER_NUM',$version);
		$this->db->where('INFO_ID', $id);
		return $this->db->update('info', $data);
	}
	
	function deleteInfo($id){
	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}

		$query=$this->db->query('SELECT INFO_ID FROM info WHERE INFO_ID='.$id.' AND VER_NUM='.$version);			
			if($query->num_rows()>0){
				$this->db->where('VER_NUM',$version);
				$this->db->where('INFO_ID',$id);
				$query = $this->db->delete('info');
				return true;
			}
	}
}

 ?>