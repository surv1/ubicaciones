<?php class reporte_model extends CI_Model {     
 
	function __construct() 
	{        
		parent::__construct();    
	}     
	 
		

	function uso() 
	{         
		// $query = $this->db->query("SELECT  CONCAT(username,'-',CAST(idcostumer AS CHAR)) AS u_id,username,status,name,id  from usersurvey INNER JOIN costumer ON costumer.id=usersurvey.idcostumer");
			// foreach ($query->result() as $fila) 
			// {
				// $data[] = $fila;
			// }
		// return $data;
		$lista_uso[]=array();
		
		$query = $this->db->query('SELECT DISTINCT DISP_PLATAFORMA,CASE DISP_PLATAFORMA WHEN "1" THEN "IOS" ELSE "Android" END AS TIPO_P, COUNT(DISP_PLATAFORMA) AS CANTIDAD from control_dispositivo WHERE DISP_PLATAFORMA IN(1,2) GROUP BY DISP_PLATAFORMA ORDER BY DISP_PLATAFORMA ');
		if($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$lista_uso[]="<tr>
				<td><center>".$row['TIPO_P']."</center></td>
				<td><center>".$row['CANTIDAD']."</center></td>
				</tr>";
				$total=$total+$row['CANTIDAD'];
			}
			$lista_uso[]="<tr>
				<td><center>TOTAL</center></td>
				<td><center>".$total."</center></td>
				</tr>";
		}
		else
			$lista_uso[]="";
		$query->free_result();
		return $lista_uso;
	}
	 
		
	function frecuenciaUso() 
	{         
		$lista_frec[]=array();
		
		$query = $this->db->query('SELECT DISTINCT DISP_PLATAFORMA,CASE DISP_PLATAFORMA WHEN "3" THEN "Navegaci&oacute;n Libre" WHEN "4" THEN "Tutorial Nuevo Ingreso" WHEN "5" THEN "Mapa General" END AS TIPO_P, COUNT(DISP_PLATAFORMA) AS CANTIDAD from control_dispositivo WHERE DISP_PLATAFORMA IN(3,4,5)  GROUP BY DISP_PLATAFORMA ORDER BY DISP_PLATAFORMA ');
		if($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$lista_frec[]="<tr>
				<td><center>".$row['TIPO_P']."</center></td>
				<td><center>".$row['CANTIDAD']."</center></td>
				</tr>";
				$total=$total+$row['CANTIDAD'];
			}
			$lista_frec[]="<tr>
				<td><center>TOTAL</center></td>
				<td><center>".$total."</center></td>
				</tr>";
		}
		else
			$lista_frec[]="";
		$query->free_result();
		return $lista_frec;
	}

		
		
	}
	
 ?>