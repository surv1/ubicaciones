<?php class proceso_model extends CI_Model {     
 
	function __construct() 
	{        
		parent::__construct();    
	}     
	 /*************Datos ocupado dentro de los campos de los formularios************/
	 function  comboProcesos(){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT PROC_ID,PROC_NOMBRE FROM proceso WHERE VER_NUM='.$version);
		if($query->num_rows()>0){
					$proceso[-1]='Seleccione...';
				foreach($query->result() as $row){
					//if($row->PROC_ID!=0)
						$proceso[$row->PROC_ID]=$row->PROC_NOMBRE;
				}
			}
			else
				$proceso[-1]='No hay procesos';
		// if(count($proceso)==1)
			// $proceso[0]='No hay procesos';
		$query->free_result();
		return $proceso;
	 }
	 
	 
	 /*******************************************************************/
	
	function validarProceso($id,$nomb){
		$version=$this->getVerNum();
			if($version==null){
				$version=0;
			}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('PROC_ID',$id);
		$query=$this->db->where('PROC_NOMBRE',$nomb);
		$query = $this->db->get('proceso');
	    return $query->row(); //    Devuelve al controlador la fila que coincide con la b�squeda. (FALSE en caso que no existir coincidencias)
	}
	
	
	function getVerNum(){		
	
	$query=$this->db->query('SELECT VER_NUM FROM version WHERE VER_ESTADO=3 ORDER BY VER_NUM DESC');		
	
	if($query->num_rows()>0){			
		
		foreach($query->result() as $row){					
			
			$version=$row->VER_NUM;				
		
		}		
	
	}		
	
	else			
		
		$version=null;		
	return $version;	
	}
	
	
	function saveProceso($datos) {
		$this->db->insert('proceso',$datos);
	}
	
	function  nombProc($idproc){
		$version=$this->getVerNum();
			if($version==null){
				$version=0;
			}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('PROC_ID', $idproc);
		$query= $this->db->get('proceso');
		return $query;
	 }
	 
	function procesos() 
	{         
		$lista_procesos[]=array();
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query = $this->db->query('SELECT proc.PROC_ID,proc.PROC_NOMBRE,proc.PROC_DESC,(SELECT proc2.PROC_NOMBRE 
		FROM proceso proc2 WHERE proc2.PROC_ID=proc.PROC_ANTECESOR AND proc2.VER_NUM=version.VER_NUM) AS ANTECESOR FROM proceso proc
		INNER JOIN version ON version.VER_NUM=proc.VER_NUM 
		WHERE  version.VER_NUM='. $version);
		if($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$lista_procesos[]="<tr>
				<td><center>".$row['PROC_ID']."</center></td>
				<td><center>".$row['PROC_NOMBRE']."</center></td>
				<td><center>".$row['PROC_DESC']."</center></td>
				<td><center>".$row['ANTECESOR']."</center></td>
				<td><input type='submit' class='button-submit' onclick='this.form.idproceso.value=".$row['PROC_ID']."' name='Editar' value='Editar'></td>								
				
				<td><input type='submit' class='button-submit' onclick='this.form.idproceso.value=".$row['PROC_ID']."' name='Eliminar' value='Eliminar'></td>
				</tr>";
			}
		}
		else
			$lista_procesos[]="";
		$query->free_result();
		return $lista_procesos;
	}
	
	function obtener($id) {
		$version=$this->getVerNum();
			if($version==null){
				$version=0;
			}
		$query=$this->db->where('VER_NUM',$version);
		$query = $this->db->where('PROC_ID',$id);
		$query = $this->db->get('proceso');
        return $query;
    }
	
	function updateProceso($id, $data) {
		$version=$this->getVerNum();
			if($version==null){
				$version=0;
			}
		$this->db->where('VER_NUM',$version);
		$this->db->where('PROC_ID', $id);
		return $this->db->update('proceso', $data);
	}
	
	function deleteProceso($id){
		$version=$this->getVerNum();
			if($version==null){
				$version=0;
			}

		$queryPaso=$this->db->query('SELECT PROC_ID FROM paso WHERE PROC_ID='.$id.' AND VER_NUM='.$version);
		$queryProceso=$this->db->query('SELECT PROC_ANTECESOR FROM proceso WHERE PROC_ANTECESOR='.$id.' AND VER_NUM='.$version);
		if($queryPaso->num_rows()>0 or $queryProceso->num_rows()>0){
			return false;
		}
		else{
			$this->db->where('VER_NUM',$version);
			$this->db->where('PROC_ID',$id);
			$query = $this->db->delete('proceso');
			return true;
		}
	}
}

 ?>