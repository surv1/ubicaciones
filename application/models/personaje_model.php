<?php class personaje_model extends CI_Model {     
 
	function __construct() 
	{        
		parent::__construct();    
	}     
	 /*************Datos ocupado dentro de los campos de los formularios************/
	 function  comboTipoPer(){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT TIPER_ID,TIPER_NOMBRE FROM tipo_personaje WHERE VER_NUM='.$version);
		if($query->num_rows()>0){
					$tipoper[0]='Seleccione...';
				foreach($query->result() as $row){
						$tipoper[$row->TIPER_ID]=$row->TIPER_NOMBRE;
				}
			}
			else
				$tipoper[0]='No hay tipos de personajes';
		$query->free_result();
		return $tipoper;
	 }
	 
	 function  comboSecciones(){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT SECC_ID,SECC_NOMBRE FROM seccion 
			INNER JOIN institucionbd ON institucionbd.INSTI_ID=seccion.INSTI_ID 
			WHERE seccion.INSTI_ID=1 AND seccion.VER_NUM='.$version);
		if($query->num_rows()>0){
				$seccion[0]='Seleccione...';
				foreach($query->result() as $row){
					if($row->SECC_ID!=0)
						$seccion[$row->SECC_ID]=$row->SECC_NOMBRE;
				}
			}
			else
				$seccion[0]='No hay secciones';
		$query->free_result();
		return $seccion;
	 }
	 /*******************************************************************/
	
	function validarPersonaje($numPerson,$idSeccion){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}

		$query=$this->db->where('PER_NUM',$numPerson);	
		$query=$this->db->where('VER_NUM',$version);	
		$query=$this->db->where('SECC_ID',$idSeccion);
		$query = $this->db->get('personaje');
	    return $query->row(); //    Devuelve al controlador la fila que coincide con la b�squeda. (FALSE en caso que no existir coincidencias)
	}
	
	function getVerNum(){		
	
	$query=$this->db->query('SELECT VER_NUM FROM version WHERE VER_ESTADO=3 ORDER BY VER_NUM DESC');		
	
	if($query->num_rows()>0){			
		foreach($query->result() as $row){					
	
			$version=$row->VER_NUM;				
	
		}		
	}		
	
	else			
		$version=null;		
	
	return $version;	
	}
	
	
	function savePerson($datos) {
		$this->db->insert('personaje',$datos);
	}
	
	function  nombTipo($idtipo){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);	
		$query=$this->db->where('TIPER_ID', $idtipo);
		$query= $this->db->get('tipo_personaje');
		return $query;
	 }
	 
	 function  nombSecc($idsec){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('SECC_ID', $idsec);
		$query= $this->db->get('seccion');
		return $query;
	 }
	 
	function personajes() 
	{         
		$lista_personajes[]=array();
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		
		$query = $this->db->query('SELECT DISTINCT personaje.PER_ID,PER_NUM,TIPER_NOMBRE,SECC_NOMBRE,PER_NOMBRE,PER_APELLIDO FROM personaje
		INNER JOIN tipo_personaje ON tipo_personaje.TIPER_ID=personaje.TIPER_ID
		INNER JOIN seccion ON seccion.SECC_ID=personaje.SECC_ID
		INNER JOIN institucionbd ON institucionbd.INSTI_ID=personaje.INSTI_ID
		INNER JOIN version ON version.VER_NUM=personaje.VER_NUM
		WHERE  version.VER_NUM='. $version);
		if($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$lista_personajes[]="<tr>
				<td><center>".$row['PER_ID']."</center></td>
				<td><center>".$row['PER_NUM']."</center></td>
				<td><center>".$row['TIPER_NOMBRE']."</center></td>
				<td><center>".$row['SECC_NOMBRE']."</center></td>
				<td><center>".$row['PER_NOMBRE']."</center></td>
				<td><center>".$row['PER_APELLIDO']."</center></td>
				<td><input type='submit' class='button-submit' onclick='this.form.idper.value=".$row['PER_ID']."' name='Editar' value='Editar'></td>								
				
				<td><input type='submit' class='button-submit' onclick='this.form.idper.value=".$row['PER_ID']."' name='Eliminar' value='Eliminar'></td>
				</tr>";
			}
		}
		else
			$lista_personajes[]="";
		$query->free_result();
		return $lista_personajes;
	}
	
	function obtener($id) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query = $this->db->where('VER_NUM',$version);
		$query = $this->db->where('PER_ID',$id);
		$query = $this->db->get('personaje');
        return $query;
    }
	
	function updatePersonaje($id, $data) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$this->db->where('VER_NUM',$version);
		$this->db->where('PER_ID', $id);
		return $this->db->update('personaje', $data);
	}
	
	function deletePersonaje($id){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT PER_ID FROM frase WHERE PER_ID='.$id.' AND VER_NUM='.$version);			
		if($query->num_rows()>0){
			return false;
		}
		else{
			$version=$this->getVerNum();
			if($version==null){
				$version=0;
			}
			$this->db->where('VER_NUM',$version);
			$this->db->where('PER_ID',$id);
			$query = $this->db->delete('personaje');
			return true;
		}
	}
}

 ?>