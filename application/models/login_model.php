<?php


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Class login_model extends CI_Model{
     
    function ValidarUsuario($Id){            //    Consulta Mysql para buscar en la tabla Usuario aquellos usuarios que coincidan con el mail y password ingresados en pantalla de login
        $query = $this->db->where('usuario',$Id);    //    La consulta se efectúa mediante Active Record. Una manera alternativa, y en lenguaje más sencillo, de generar las consultas Sql.
        $dato = $this->db->get('usuario');
        return $dato;     //    Devolvemos al controlador la fila que coincide con la búsqueda. (FALSE en caso que no existir coincidencias)
    }
    
	function ValidarPass($nick){            //    Consulta Mysql para buscar en la tabla Usuario aquellos usuarios que coincidan con el mail y password ingresados en pantalla de login
        $query = $this->db->where('usuario',$nick);
        $query = $this->db->get('usuario');
        return $query->row();     //    Devolvemos al controlador la fila que coincide con la búsqueda. (FALSE en caso que no existir coincidencias)
    }
	
    function DatosUsr($id){
    	$this->db->where('usuario',$id);
    	$dato = $this->db->get('usuario');
	    return $dato;
    }
	
	function bitacora($datos)
	{
		$this->db->insert('bitacora', $datos);
	}
}
?>
