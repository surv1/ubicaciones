<?php class segmento_model extends CI_Model {     
 
	function __construct() 
	{        
		parent::__construct();    
	}     
	 
		/*************Datos ocupado dentro de los campos de los formularios************/
		 function  comboBases(){
			$query=$this->db->query('SELECT INSTI_ID,INSTI_NOMB FROM institucionbd WHERE INSTI_ID=1');
			if($query->num_rows()>0){
					$bd[0]='Seleccione...';
					foreach($query->result() as $row){
						$bd[$row->INSTI_ID]=$row->INSTI_NOMB ;
					}
				}
				else
					$bd[]='No hay empresas';
			$query->free_result();
			return $bd;
		 }
		 
		 function traerScript($idbase){
			$query=$this->db->query('SELECT tabla1('.$idbase.')');
			if($query->num_rows()>0){
					foreach($query->result_array() as $row){
						$bd=$row['tabla1('.$idbase.')'];
					}
				}
				else
					$bd[]='No hay segmentos';
			$query->free_result();
			return $bd;
		 }
		 
		 function nombBase($id){
			$query=$this->db->where('INSTI_ID	', $id);
			$query= $this->db->get('institucionbd');
			return $query;
		 }
		 /********************************************************************/
	 
	 function validarSegmento($segmento){
		//$query=$this->db->where('SEG_ID',$id);
		
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('INSTI_ID	',1);
		$query=$this->db->where('SEG_NOMBRE',$segmento);
		$query = $this->db->get('segmento');
			return $query->row(); //    Devuelve al controlador la fila que coincide con la b�squeda. (FALSE en caso que no existir coincidencias)
	}
	
	
	function getVerNum(){			
		$query=$this->db->query('SELECT VER_NUM FROM version WHERE VER_ESTADO=3 ORDER BY VER_NUM DESC');			
		if($query->num_rows()>0){				
			foreach($query->result() as $row){						
				$version=$row->VER_NUM;					
			}			
		}			
		else				
		$version=null;			
		return $version;		
	}
	
	
	function saveSegmento($datos) {
		$this->db->insert('segmento',$datos);
	}

	function segmentos() 
	{         
		// $query = $this->db->query("SELECT  CONCAT(username,'-',CAST(idcostumer AS CHAR)) AS u_id,username,status,name,id  from usersurvey INNER JOIN costumer ON costumer.id=usersurvey.idcostumer");
			// foreach ($query->result() as $fila) 
			// {
				// $data[] = $fila;
			// }
		// return $data;
		$lista_segmentos[]=array();
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query = $this->db->query('SELECT SEG_ID,SEG_NOMBRE,SEG_DESC,SEG_MIN,SEG_MAX,(SELECT INSTI_NOMB FROM institucionbd AS base WHERE base.INSTI_ID=seg.INSTI_ID) AS BASE_ID  from segmento as seg INNER JOIN version ON version.VER_NUM=seg.VER_NUM WHERE seg.INSTI_ID=1 AND version.VER_NUM='. $version);
		if($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$lista_segmentos[]="<tr>
				<td><center>".$row['SEG_ID']."</center></td>
				<td><center>".$row['SEG_NOMBRE']."</center></td>
				<td><center>".$row['SEG_DESC']."</center></td>
				<td><center>".$row['SEG_MIN']."</center></td>
				<td><center>".$row['SEG_MAX']."</center></td>
				<td><center>".$row['BASE_ID']."</center></td>
				<td><input type='submit' class='button-submit' onclick='this.form.idsegmento.value=".$row['SEG_ID']." ' name='Editar' value='Editar'></td>								
				<td><input type='submit' class='button-submit' onclick='this.form.idsegmento.value=".$row['SEG_ID']." ' name='Eliminar' value='Eliminar'></td>
				</tr>";
			}
		}
		else
			$lista_segmentos[]="";
		$query->free_result();
		return $lista_segmentos;
	}
	 
		//obtenemos la fila completa del mensaje a editar
		//vemos que como solo queremos una fila utilizamos
		//la funci�n row que s�lo nos devuelve una fila,
		//as� la consulta ser� m�s r�pida
		function obtener($id) {
			$version=$this->getVerNum();
			if($version==null){
				$version=0;
			}
			$query=$this->db->where('VER_NUM',$version);
			$this->db->where('SEG_ID',$id);
			$query = $this->db->get('segmento');
			return $query;
		}
	 
		
		//actualizamos los datos en la base de datos con el patr�n
		//active record de codeIginiter, recordar que no hace falta
		//escapar las consultas ya que lo hace �l automatic�mente
		function updateSegmento($id, $data) {
			$version=$this->getVerNum();
			if($version==null){
				$version=0;
			}
			$query=$this->db->where('VER_NUM',$version);
			$this->db->where('SEG_ID', $id);
			return $this->db->update('segmento', $data);
		}
		
		
		function deleteSegmento($id){
			$version=$this->getVerNum();
			if($version==null){
				$version=0;
			}
			//$query=$this->db->where('VER_NUM',$version);
			$query=$this->db->query('SELECT SEG_ID FROM seccion WHERE SEG_ID='.$id.' AND VER_NUM='.$version);			
			if($query->num_rows()>0){
				return false;
			}
			else{
				$this->db->where('SEG_ID',$id);
				$query = $this->db->delete('segmento');
				return true;
			}
		}
	}
	/*application/models/datos_model.php
	 * el modelo datos_model.php
	 */
 ?>