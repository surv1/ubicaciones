<?php class conexion_model extends CI_Model {     
 
	function __construct() 
	{        
		parent::__construct();    
	}     
	 /*************Datos ocupado dentro de los campos de los formularios************/
	 function  comboSecciones(){
	 	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT SECC_ID,SECC_NOMBRE FROM seccion INNER JOIN institucionbd ON institucionbd.INSTI_ID=seccion.INSTI_ID WHERE seccion.INSTI_ID=1 AND VER_NUM='.$version);
		if($query->num_rows()>0){
				$seccion[-1]='Seleccione...';
				foreach($query->result() as $row){
					$seccion[$row->SECC_ID]=$row->SECC_NOMBRE;
				}
			}
			else
				$seccion[]='No hay secciones';
		$query->free_result();
		return $seccion;
	 }
	 
	 
	 /*******************************************************************/
	
	function validarConexion($id){
	$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
	$query=$this->db->where('VER_NUM',$version);
	$query=$this->db->where('CONEX_ID',$id);
	$query = $this->db->get('conexion');
	    return $query->row(); //    Devuelve al controlador la fila que coincide con la b�squeda. (FALSE en caso que no existir coincidencias)
	}
	
	
	function getVerNum(){	
	
	$query=$this->db->query('SELECT VER_NUM FROM version WHERE VER_ESTADO=3 ORDER BY VER_NUM DESC');	
	
	if($query->num_rows()>0){			
		foreach($query->result() as $row){					
			$version=$row->VER_NUM;				
		}		
	}		
	
	else			
		$version=null;		
	return $version;	
	}
	
	
	function saveConexion($datos) {
		$this->db->insert('conexion',$datos);
	}
	
	function  nombSeccion($idsecc){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->where('VER_NUM',$version);
		$query=$this->db->where('SECC_ID', $idsecc);
		$query= $this->db->get('seccion');
		return $query;
	 }
	 
	function conexiones() 
	{         
		$lista_conexiones[]=array();
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query = $this->db->query('SELECT CONEX_ID,(SELECT SECC_NOMBRE FROM seccion WHERE SECC_ID=SECC_O_ID AND VER_NUM='.$version.') AS ORIGEN, 
		(SELECT SECC_NOMBRE FROM seccion WHERE SECC_ID=SECC_D_ID AND VER_NUM='.$version.') AS DESTINO,ENTRADA,SALIDA from conexion 
		INNER JOIN institucionbd ON institucionbd.INSTI_ID=conexion.INSTI_ID 
		INNER JOIN version ON version.VER_NUM=conexion.VER_NUM
		WHERE conexion.INSTI_ID=1  AND  version.VER_NUM='. $version);
		if($query->num_rows()>0){
			foreach($query->result_array() as $row){
				$lista_conexiones[]="<tr>
				<td><center>".$row['CONEX_ID']."</center></td>
				<td><center>".$row['ORIGEN']."</center></td>
				<td><center>".$row['DESTINO']."</center></td>
				<td><center>".$row['ENTRADA']."</center></td>
				<td><center>".$row['SALIDA']."</center></td>
				<td><input type='submit' class='button-submit' onclick='this.form.idconexion.value=".$row['CONEX_ID']."' name='Editar' value='Editar'></td>								<td><input type='submit' class='button-submit' onclick='this.form.idconexion.value=".$row['CONEX_ID']."' name='Eliminar' value='Eliminar'></td>
				</tr>";
			}
		}
		else
			$lista_conexiones[]="";
		$query->free_result();
		return $lista_conexiones;
	}
	
	function obtener($id) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query = $this->db->query('SELECT CONEX_ID,SECC_O_ID,
		(SELECT SECC_NOMBRE FROM seccion WHERE SECC_ID=SECC_D_ID AND VER_NUM='.$version.') AS DESTINO,SECC_D_ID,ENTRADA,SALIDA from conexion WHERE CONEX_ID='.$id.' AND VER_NUM='.$version);
        return $query;
    }
	
	function updateConexion($id, $data) {
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$this->db->where('VER_NUM',$version);
		$this->db->where('CONEX_ID', $id);
		return $this->db->update('conexion', $data);
	}
	
	function deleteConexion($id){
		$version=$this->getVerNum();
		if($version==null){
			$version=0;
		}
		$query=$this->db->query('SELECT CONEX_ID FROM conexion WHERE CONEX_ID='.$id);			
			if($query->num_rows()>0){
				$this->db->where('VER_NUM',$version);
				$this->db->where('CONEX_ID',$id);
				$query = $this->db->delete('conexion');
				return true;
			}
			
	}
}

 ?>